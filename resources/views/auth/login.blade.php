@extends('layouts.auth-app')
@section('title', 'Amazing Ecom - Log In')

@section('content')
<div class="card">
  <div class="card-body login-card-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form id="login-form" action="javascript:void(0)" method="post">
      @csrf
      <div class="form-group mb-3">
        <div class="input-group">
          <input type="text" name="email" id="email" class="form-control" placeholder="Email Address">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <span class="error" id="email-error"></span>
      </div>
      <div class="form-group mb-3">
        <div class="input-group">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <span class="error" id="password-error"></span>
      </div>
      <div class="row">
        <div class="col-8"></div>
        <!-- /.col -->
        <div class="col-4">
          <button type="submit" id="login-btn" class="btn btn-primary btn-block" style="min-width: 38px">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="{{route('forgot_password')}}">I forgot my password</a>
  </div>
  <!-- /.login-card-body -->
</div>
@endsection

@push('footer-script')
<script>

  let loginFormValidator = $('#login-form').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6,
        maxlength: 20
      }
    },
    messages: {
      email: {
        required: "Email is required.",
      },
      password: {
        required: "Password is required.",
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element).parent())
    },
    submitHandler: function(form) {
      const payload = {
        email: document.querySelector('#email').value,
        password: document.querySelector('#password').value,
        _token: "{{csrf_token()}}"
      }
      const param = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      }
      login(param)
    }
  })

  async function login(param) {
    const submitButton = document.querySelector('#login-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetLoginForm()

      let response = await fetch("{{route('attempt')}}", param)

      if(response.ok) {
        response = await response.json()
        if(response.status) {
          location.href = "{{route('dashboard')}}"
        } else {
          toastr.error(response.message)
        }
        
      } else {
        if(response.status !== 422) return toastr.error("Something went wrong, Please try again!")
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.email) document.querySelector('#email-error').textContent = response.message.email[0]
          if(response?.message?.password) document.querySelector('#password-error').textContent = response.message.password[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Sign In'
    }
  }

  function resetLoginForm() {
    const form = document.querySelector('#login-form')
    form.reset()
    loginFormValidator.resetForm()
    document.querySelector('#email-error').textContent = ""
    document.querySelector('#password-error').textContent = ""
  }

</script>
@endpush