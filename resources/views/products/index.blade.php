@extends('layouts.app')
@section('title', 'Amazing Ecom - Products')

@push('head-script')
<link href="https://unpkg.com/dropzone@6.0.0-beta.1/dist/dropzone.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Products</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Products</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of products.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('products.export')}}" class="btn btn-info mr-2" title="Export Products"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Products" onclick="createProduct()"><i class="fas fa-plus"></i> Create Product</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="product-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Product Name</th>
                  <th>Vendor Name</th>
                  <th>Price</th>
                  <th>Quanity</th>
                  <th>Categories</th>
                  <th>Sub Categories</th>
                  <th>Colors</th>
                  <th>Brand</th>
                  <th>Sizes</th>
                  <th>Tags</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

{{-- Create Product Modal --}}
<div class="modal fade" id="create-product-modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form id="create-product-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Product</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="create-product-name">Product Name</label>
              <input type="text" class="form-control" name="name" id="create-product-name" placeholder="Product Name">
              <span class="error" id="create-product-name-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-price">Price</label>
              <input type="text" class="form-control" name="price" id="create-product-price" placeholder="Price">
              <span class="error" id="create-product-price-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-quantity">Quantity</label>
              <input type="number" class="form-control" name="quantity" id="create-product-quantity" placeholder="quantity" min="0">
              <span class="error" id="create-product-quantity-error"></span>
            </div>
          </div>
          <hr>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="create-product-name">Select Vendor</label>
              <select name="vendor" id="create-product-vendor" class="form-control select2">
                <option value="">Select Vendor</option>
                @foreach ($vendors as $vendor)
                  <option value="{{$vendor->id}}">{{$vendor->fname." ".$vendor->lname}}</option>
                @endforeach
              </select>
              <span class="error" id="create-product-vendor-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-categories">Select Categories</label>
              <select name="categories[]" id="create-product-categories" class="form-control select2" multiple onchange="getSubCategories(event, 'create')">
                <option value="">Select Categories</option>
                @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
              <span class="error" id="create-product-category-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-sub-categories">Select Sub Categories</label>
              <select name="sub_categories[]" id="create-product-sub-categories" class="form-control select2" multiple>
                <option value="">Select Sub Categories</option>
              </select>
              <span class="error" id="create-product-sub-category-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="create-product-colors">Select Colors</label>
              <select name="colors[]" id="create-product-colors" class="form-control select2" multiple>
                <option value="">Select Colors</option>
                @foreach ($colors as $color)
                  <option value="{{$color->id}}">{{$color->name}}</option>
                @endforeach
              </select>
              <span class="error" id="create-product-color-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-brand">Select Brand</label>
              <select name="brand" id="create-product-brand" class="form-control select2">
                <option value="">Select Brand</option>
                @foreach ($brands as $brand)
                  <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
              </select>
              <span class="error" id="create-product-brand-error"></span>
            </div>
            <div class="col-md-4">
              <label for="create-product-sizes">Select Sizes</label>
              <select name="sizes[]" id="create-product-sizes" class="form-control select2" multiple>
                <option value="">Select Sizes</option>
                @foreach ($sizes as $size)
                  <option value="{{$size->id}}">{{$size->size}}</option>
                @endforeach
              </select>
              <span class="error" id="create-product-size-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="create-product-tags">Add Tags</label>
              <select name="tags[]" id="create-product-tags" class="form-control select2 tags" multiple></select>
              <span class="error" id="create-product-tag-error"></span>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <label for="create-product-summary">Summary</label>
              <textarea name="summary" class="form-control" id="create-product-summary" rows="3"></textarea>
              <span class="error" id="create-product-summary-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-product-description">Description</label>
              <textarea name="description" class="form-control" id="create-product-description" rows="3"></textarea>
              <span class="error" id="create-product-description-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-product-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

{{-- update product modal --}}
<div class="modal fade" id="update-product-modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form id="update-product-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Product</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="update-product-name">Product Name</label>
              <input type="hidden" class="form-control" name="id" id="update-product-id">
              <input type="text" class="form-control" name="name" id="update-product-name" placeholder="Product Name">
              <span class="error" id="update-product-name-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-price">Price</label>
              <input type="text" class="form-control" name="price" id="update-product-price" placeholder="Price">
              <span class="error" id="update-product-price-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-quantity">Quantity</label>
              <input type="number" class="form-control" name="quantity" id="update-product-quantity" placeholder="quantity" min="0">
              <span class="error" id="update-product-quantity-error"></span>
            </div>
          </div>
          <hr>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="update-product-name">Select Vendor</label>
              <select name="vendor" id="update-product-vendor" class="form-control select2" disabled>
                <option value="">Select Vendor</option>
                @foreach ($vendors as $vendor)
                  <option value="{{$vendor->id}}">{{$vendor->fname." ".$vendor->lname}}</option>
                @endforeach
              </select>
              <span class="error" id="update-product-vendor-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-categories">Select Categories</label>
              <select name="categories[]" id="update-product-categories" class="form-control select2" multiple onchange="getSubCategories(event, 'update')">
                <option value="">Select Categories</option>
                @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
              <span class="error" id="update-product-category-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-sub-categories">Select Sub Categories</label>
              <select name="sub_categories[]" id="update-product-sub-categories" class="form-control select2" multiple>
                <option value="">Select Sub Categories</option>
              </select>
              <span class="error" id="update-product-sub-category-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="update-product-colors">Select Colors</label>
              <select name="colors[]" id="update-product-colors" class="form-control select2" multiple>
                <option value="">Select Colors</option>
                @foreach ($colors as $color)
                  <option value="{{$color->id}}">{{$color->name}}</option>
                @endforeach
              </select>
              <span class="error" id="update-product-color-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-brand">Select Brand</label>
              <select name="brand" id="update-product-brand" class="form-control select2">
                <option value="">Select Brand</option>
                @foreach ($brands as $brand)
                  <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
              </select>
              <span class="error" id="update-product-brand-error"></span>
            </div>
            <div class="col-md-4">
              <label for="update-product-sizes">Select Sizes</label>
              <select name="sizes[]" id="update-product-sizes" class="form-control select2" multiple>
                <option value="">Select Sizes</option>
                @foreach ($sizes as $size)
                  <option value="{{$size->id}}">{{$size->size}}</option>
                @endforeach
              </select>
              <span class="error" id="update-product-size-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-4">
              <label for="update-product-tags">Add Tags</label>
              <select name="tags[]" id="update-product-tags" class="form-control select2 tags" multiple></select>
              <span class="error" id="update-product-tag-error"></span>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <label for="update-product-summary">Summary</label>
              <textarea name="summary" class="form-control" id="update-product-summary" rows="3"></textarea>
              <span class="error" id="update-product-summary-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-product-description">Description</label>
              <textarea name="description" class="form-control" id="update-product-description" rows="3"></textarea>
              <span class="error" id="update-product-description-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-product-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

{{-- view product review modal --}}
<div class="modal fade" id="view-rate-review-modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">View Product Rates & Reviews</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="view-rate-review-content">
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

{{-- upload product modal --}}
<div class="modal fade" id="upload-product-modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Product Image</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="upload-product-content">
        <div>
          <input type="hidden" class="" id="upload-product-id" name="product_id">
          <div class="dropzone"></div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script src="https://unpkg.com/dropzone@6.0.0-beta.1/dist/dropzone-min.js"></script>
<script>

  $(".tags").select2({
    tags: true
  });

  let table = $('#product-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("products.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'name', name: 'name'},
      {data: 'vendor', name: 'vendor'},
      {data: 'price', name: 'price'},
      {data: 'quantity', name: 'quantity'},
      {data: 'categories', name: 'categories'},
      {data: 'sub_categories', name: 'sub_categories'},
      {data: 'colors', name: 'colors'},
      {data: 'brand', name: 'brand'},
      {data: 'sizes', name: 'sizes'},
      {data: 'tags', name: 'tags'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true }
    ]
  })

  // create product
  function createProduct() {
    $('#create-product-modal').modal('show');
  }

  let createProductFormValidator = $('#create-product-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      price: {
        required: true,
        number: true
      },
      quantity: {
        required: true,
        number: true
      },
      vendor: {
        required: true
      },
      "categories[]": {
        required: true
      },
      "sub_categories[]": {
        required: true
      },
      "colors[]": {
        required: true
      },
      "brand": {
        required: true
      },
      "sizes[]": {
        required: true
      },
      "tags[]": {
        required: true
      },
      summary: {
        maxlength: 250,
      }
    },
    messages: {
      name: {
        required: "Product name is required."
      },
      price: {
        required: "Price is required.",
      },
      quantity: {
        required: "Quantity is required."
      },
      vendor: {
        required: "Vendor is required."
      },
      "categories[]": {
        required: "Category is required."
      },
      "sub_categories[]": {
        required: "Sub category is required."
      },
      "colors[]": {
        required: "Color is required."
      },
      "brand": {
        required: "Brand is required."
      },
      "sizes[]": {
        required: "Size is required."
      },
      "tags[]": {
        required: "Tag is required."
      },
      summary: {
        required: true,
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "vendor") {
        error.appendTo('#create-product-vendor-error')
      } else if(element.attr('name') == "categories[]") {
        error.appendTo('#create-product-category-error')
      } else if(element.attr('name') == "sub_categories[]") {
        error.appendTo('#create-product-sub-category-error')
      } else if(element.attr('name') == "colors[]") {
        error.appendTo('#create-product-color-error')
      } else if(element.attr('name') == "brand") {
        error.appendTo('#create-product-brand-error')
      } else if(element.attr('name') == "sizes[]") {
        error.appendTo('#create-product-size-error')
      } else if(element.attr('name') == "tags[]") {
        error.appendTo('#create-product-tag-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      // categories
      const categoryInput = document.querySelector(`#create-product-categories`)
      const selectedCategoryOptions = categoryInput.querySelectorAll('option:checked')
      const selectedCategories = [...selectedCategoryOptions].map(e => e.value)

      // sub categories
      const subCategoryInput = document.querySelector(`#create-product-sub-categories`)
      const selectedSubCategoryOptions = subCategoryInput.querySelectorAll('option:checked')
      const selectedSubCategories = [...selectedSubCategoryOptions].map(e => e.value)

      // colors
      const colorInput = document.querySelector(`#create-product-colors`)
      const selectedColorOptions = colorInput.querySelectorAll('option:checked')
      const selectedColors = [...selectedColorOptions].map(e => e.value)

      // brand
      const selectedBrand = document.querySelector(`#create-product-brand`).value

      // sizes
      const sizeInput = document.querySelector(`#create-product-sizes`)
      const selectedSizeOptions = sizeInput.querySelectorAll('option:checked')
      const selectedSizes = [...selectedSizeOptions].map(e => e.value)

      // tags
      const tagInput = document.querySelector(`#create-product-tags`)
      const selectedTagOptions = tagInput.querySelectorAll('option:checked')
      const selectedTags = [...selectedTagOptions].map(e => e.value)

      const formData = new FormData()
      formData.append('name', document.querySelector('#create-product-name').value)
      formData.append('price', document.querySelector('#create-product-price').value)
      formData.append('quantity', document.querySelector('#create-product-quantity').value)
      formData.append('vendor', document.querySelector('#create-product-vendor').value)
      formData.append('categories', selectedCategories)
      formData.append('sub_categories', selectedSubCategories)
      formData.append('colors', selectedColors)
      formData.append('brand', selectedBrand)
      formData.append('sizes', selectedSizes)
      formData.append('tags', selectedTags)
      formData.append('summary', document.querySelector('#create-product-summary').value)
      formData.append('description', document.querySelector('#create-product-description').value)
      formData.append('_token', "{{csrf_token()}}")

      const param = prepareFetchParamWithImage(formData)
      storeProduct(param)
    }
  })

  async function storeProduct(param) {
    const submitButton = document.querySelector('#create-product-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateProductForm()

      let response = await fetch("{{route('products.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-product-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#create-product-name-error').textContent = response.message.name[0]
          if(response?.message?.price) document.querySelector('#create-product-price-error').textContent = response.message.price[0]
          if(response?.message?.quantity) document.querySelector('#create-product-quantity-error').textContent = response.message.quantity[0]
          if(response?.message?.vendor) document.querySelector('#create-product-vendor-error').textContent = response.message.vendor[0]
          if(response?.message?.["categories[]"]) document.querySelector('#create-product-category-error').textContent = response.message["categories[]"][0]
          if(response?.message?.["sub_categories[]"]) document.querySelector('#create-product-sub-category-error').textContent = response.message["sub_categories[]"][0]
          if(response?.message?.["colors[]"]) document.querySelector('#create-product-color-error').textContent = response.message["colors[]"][0]
          if(response?.message?.["brand"]) document.querySelector('#create-product-brand-error').textContent = response.message["brand"][0]
          if(response?.message?.["sizes[]"]) document.querySelector('#create-product-size-error').textContent = response.message["sizes[]"][0]
          if(response?.message?.["tags[]"]) document.querySelector('#create-product-tag-error').textContent = response.message["tags[]"][0]
          if(response?.message?.summary) document.querySelector('#create-product-summary-error').textContent = response.message.summary[0]
          if(response?.message?.description) document.querySelector('#create-product-description-error').textContent = response.message.description[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-product-modal').on('hidden.bs.modal', event => {
    resetCreateProductForm();
  })

  function resetCreateProductForm() {
    const form = document.querySelector('#create-product-form')
    form.reset()

    createProductFormValidator.resetForm()

    document.querySelector('#create-product-name-error').textContent = ""
    document.querySelector('#create-product-price-error').textContent = ""
    document.querySelector('#create-product-quantity-error').textContent = ""
    document.querySelector('#create-product-summary-error').textContent = ""
    document.querySelector('#create-product-description-error').textContent = ""
  }


  // update product
  async function editProduct(product) {
    $('#update-product-modal').modal('show');

    try {
      let response = await fetch(`{{url('/products/edit')}}/${product}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-product-id').value = response.data.id;
        document.querySelector('#update-product-name').value = response.data.name;
        document.querySelector('#update-product-price').value = response.data.price;
        document.querySelector('#update-product-quantity').value = response.data.quantity;
        document.querySelector('#update-product-summary').value = response.data.summary;
        document.querySelector('#update-product-description').value = response.data.description;
      
        // select vendor
        const vendor = document.querySelector('#update-product-vendor')
        vendor.value = response.data.vendor_id
        vendor.dispatchEvent(new Event("change"));

        // select category
        const oldCategories = response.data.categories;
        const selectedCategories = oldCategories.split(',')
        $('#update-product-categories').val(selectedCategories).change();

        await getSubCategoriesByCategory(selectedCategories, 'update')

        // select sub category
        const oldSubCategories = response.data.sub_categories;
        const selectedSubCategories = oldSubCategories.split(',')
        $('#update-product-sub-categories').val(selectedSubCategories).change();

        // select color
        const oldColors = response.data.colors;
        const selectedColors = oldColors.split(',')
        $('#update-product-colors').val(selectedColors).change();

        // select brand
        const selectedBrand = response.data.brand
        $('#update-product-brand').val(selectedBrand).change();

        // select size
        const oldSizes = response.data.sizes;
        const selectedSizes = oldSizes.split(',')
        $('#update-product-sizes').val(selectedSizes).change();
        
        // select tag
        const oldTags = response.data.tags;
        const selectedTags = oldTags.split(',')
        selectedTags.forEach(tag => {
          const option = new Option(tag, tag, true, true);
          $('#update-product-tags').append(option).trigger('change');
        })
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateProductFormValidator = $('#update-product-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      price: {
        required: true,
        number: true
      },
      quantity: {
        required: true,
        number: true
      },
      vendor: {
        required: true
      },
      "categories[]": {
        required: true
      },
      "sub_categories[]": {
        required: true
      },
      "colors[]": {
        required: true
      },
      "brand": {
        required: true
      },
      "sizes[]": {
        required: true
      },
      "tags[]": {
        required: true
      },
      summary: {
        maxlength: 250,
      }
    },
    messages: {
      name: {
        required: "Product name is required."
      },
      price: {
        required: "Price is required.",
      },
      quantity: {
        required: "Quantity is required."
      },
      vendor: {
        required: "Vendor is required."
      },
      "categories[]": {
        required: "Category is required."
      },
      "sub_categories[]": {
        required: "Sub category is required."
      },
      "colors[]": {
        required: "Color is required."
      },
      "brand": {
        required: "Brand is required."
      },
      "sizes[]": {
        required: "Size is required."
      },
      "tags[]": {
        required: "Tag is required."
      },
      summary: {
        required: true,
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "vendor") {
        error.appendTo('#update-product-vendor-error')
      } else if(element.attr('name') == "categories[]") {
        error.appendTo('#update-product-category-error')
      } else if(element.attr('name') == "sub_categories[]") {
        error.appendTo('#update-product-sub-category-error')
      } else if(element.attr('name') == "colors[]") {
        error.appendTo('#update-product-color-error')
      } else if(element.attr('name') == "brand") {
        error.appendTo('#update-product-brand-error')
      } else if(element.attr('name') == "sizes[]") {
        error.appendTo('#update-product-size-error')
      } else if(element.attr('name') == "tags[]") {
        error.appendTo('#update-product-tag-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const productID = document.querySelector('#update-product-id').value

      // categories
      const categoryInput = document.querySelector(`#update-product-categories`)
      const selectedCategoryOptions = categoryInput.querySelectorAll('option:checked')
      const selectedCategories = [...selectedCategoryOptions].map(e => e.value)

      // sub categories
      const subCategoryInput = document.querySelector(`#update-product-sub-categories`)
      const selectedSubCategoryOptions = subCategoryInput.querySelectorAll('option:checked')
      const selectedSubCategories = [...selectedSubCategoryOptions].map(e => e.value)

      // colors
      const colorInput = document.querySelector(`#update-product-colors`)
      const selectedColorOptions = colorInput.querySelectorAll('option:checked')
      const selectedColors = [...selectedColorOptions].map(e => e.value)

      // brand
      const selectedBrand = document.querySelector(`#update-product-brand`).value

      // sizes
      const sizeInput = document.querySelector(`#update-product-sizes`)
      const selectedSizeOptions = sizeInput.querySelectorAll('option:checked')
      const selectedSizes = [...selectedSizeOptions].map(e => e.value)

      // tags
      const tagInput = document.querySelector(`#update-product-tags`)
      const selectedTagOptions = tagInput.querySelectorAll('option:checked')
      const selectedTags = [...selectedTagOptions].map(e => e.value)

      const formData = new FormData()
      formData.append('name', document.querySelector('#update-product-name').value)
      formData.append('price', document.querySelector('#update-product-price').value)
      formData.append('quantity', document.querySelector('#update-product-quantity').value)
      formData.append('categories', selectedCategories)
      formData.append('sub_categories', selectedSubCategories)
      formData.append('colors', selectedColors)
      formData.append('brand', selectedBrand)
      formData.append('sizes', selectedSizes)
      formData.append('tags', selectedTags)
      formData.append('summary', document.querySelector('#update-product-summary').value)
      formData.append('description', document.querySelector('#update-product-description').value)
      formData.append('_token', "{{csrf_token()}}")
      const param = prepareFetchParamWithImage(formData)
      updateProduct(param, productID)
    }
  })

  async function updateProduct(param, product) {
    const submitButton = document.querySelector('#update-product-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateProductForm()

      let response = await fetch(`{{url('/products/update')}}/${product}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-product-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#update-product-name-error').textContent = response.message.name[0]
          if(response?.message?.price) document.querySelector('#update-product-price-error').textContent = response.message.price[0]
          if(response?.message?.quantity) document.querySelector('#update-product-quantity-error').textContent = response.message.quantity[0]
          if(response?.message?.["categories[]"]) document.querySelector('#update-product-category-error').textContent = response.message["categories[]"][0]
          if(response?.message?.["sub_categories[]"]) document.querySelector('#update-product-sub-category-error').textContent = response.message["sub_categories[]"][0]
          if(response?.message?.["colors[]"]) document.querySelector('#update-product-color-error').textContent = response.message["colors[]"][0]
          if(response?.message?.["brand"]) document.querySelector('#update-product-brand-error').textContent = response.message["brand"][0]
          if(response?.message?.["sizes[]"]) document.querySelector('#update-product-size-error').textContent = response.message["sizes[]"][0]
          if(response?.message?.["tags[]"]) document.querySelector('#update-product-tag-error').textContent = response.message["tags[]"][0]
          if(response?.message?.summary) document.querySelector('#update-product-summary-error').textContent = response.message.summary[0]
          if(response?.message?.description) document.querySelector('#update-product-description-error').textContent = response.message.description[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-product-modal').on('hidden.bs.modal', event => {
    resetUpdateProductForm();
  })

  function resetUpdateProductForm() {
    updateProductFormValidator.resetForm()

    document.querySelector('#update-product-name-error').textContent = ""
    document.querySelector('#update-product-price-error').textContent = ""
    document.querySelector('#update-product-quantity-error').textContent = ""
    document.querySelector('#update-product-summary-error').textContent = ""
    document.querySelector('#update-product-description-error').textContent = ""
  }


  // view rates & reviews
  async function viewRateReview(product) {
    $('#view-rate-review-modal').modal('show');

    try {
      let response = await fetch(`{{url('/products')}}/${product}/rate-review`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.text();
      $('#view-rate-review-content').html(response)

      $('#review-list').DataTable()

    } catch (err) {
      console.error(err)
    }
  }

  // delete product reviews
  function deleteReview(product, review) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this review!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/products')}}/rate-review/delete/${review}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            await viewRateReview(product)
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }


  // delete product
  function deleteProduct(product) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/products/delete')}}/${product}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }


  // update status of product
  function changeProductStatus(product) {
    swal({
      title: "Are you sure?",
      text: "You want to change product status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/products/change-status')}}/${product}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }


  // get sub categories
  async function getSubCategories(event, type) {
    const SubcategoryInput = document.querySelector(`#create-product-sub-categories`)

    const categoryInput = document.querySelector(`#create-product-categories`)
    const selectedCategoryOptions = categoryInput.querySelectorAll('option:checked')
    const selectedCategories = [...selectedCategoryOptions].map(e => e.value)

    if(selectedCategories.length == 0) return SubcategoryInput.innerHTML = `<option value="">Select Sub Categories</option>`

    await getSubCategoriesByCategory(selectedCategories.join(','), type)
  }

  async function getSubCategoriesByCategory(categories, type) {
    let subCategoryInput = document.querySelector(`#${type}-product-sub-categories`)

    try {
      let response = await fetch(`{{url('/products/get-sub-categories')}}?categories=${categories}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        const subCategories = response.data
        let options = `<option value="">Select Sub Categories</option>`
        subCategories.forEach(subCategory => options += `<option value="${subCategory.id}">${subCategory.name}</option>`);
        subCategoryInput.innerHTML = options
      }

    } catch (err) {
      console.error(err)
    }
  }


  // upload product
  async function uploadProducts(product) {
    document.querySelector('#upload-product-id').value = product
    $('#upload-product-modal').modal('show');
    
    const template = document.querySelector(`#old-images-${product}`)

    await setImageInDropzone(template)

    // const images1 = template.content.querySelectorAll('img')
  //   console.log(images1[0])

  //   const reader = new FileReader();
  //   reader.addEventListener('load', (event) => {
  //     img.src = event.target.result;
  //   });
  //   reader.readAsDataURL(file);
  }

  let fileList = [];
  let fileId = 0;

  async function setImageInDropzone(template) {
    const templateClone = template.content.cloneNode(true)
    divs = templateClone.querySelectorAll("div");
    divs.forEach((image, index) => {
      const imageName = image.dataset.name
      const imageSize = image.dataset.size
      const imagePath = image.dataset.path
      const imageColor = image.dataset.color
      const mockFile = { name: imageName, size: imageSize };

      dropzone.emit("addedfile", mockFile);
      dropzone.emit("thumbnail", mockFile, imagePath);
      dropzone.emit("complete", mockFile);

      const fileDetails = {
        id: fileId++,
        name: imageName,
      }
      fileList.push(fileDetails)

      const colorInputs = document.querySelectorAll('.product-color')
      if(imageColor) {
        colorInputs[index].value = imageColor
      }
    })
  }

  $('#upload-product-modal').on('hidden.bs.modal', event => {
    $('.dz-preview').remove();
    table.ajax.reload()
  })

  const dropzone = new Dropzone(".dropzone", { 
    url: "/products/upload",
    addRemoveLinks: true,
    acceptedFiles: ".jpeg,.jpg,.png",
    dictInvalidFileType: "Upload only jpeg, jpg or png file.",
    dictRemoveFileConfirmation: "Are you sure you want to delete image?",
    init: function () {
      this.on("success", async function (file, response) {
        const fileDetails = {
          id: fileId++,
          name: file.name,
        }
        await reloadTable()
        fileList.push(fileDetails)
        manageColorId()
      });
    }
  });
  
  dropzone.on("removedfile", async function(file) {
    for(let i=0;i<fileList.length;++i){
      if(fileList[i]?.name) {
        if(fileList[i].name==file.name) {
          const payload = {
            file_id: fileList[i].id,
            product_id: document.querySelector('#upload-product-id').value,
            _token: "{{csrf_token()}}"
          }
          const param = prepareFetchParam(payload)

          let response = await fetch(`{{url('/products/remove-upload')}}`, param)
          if(response.ok) {
            response = await response.json()
            if(response.status) {
              await reloadTable()

              fileList.splice(fileList[i].id, 1)

              fileList.forEach((file, index) => file.id = index)
              manageColorId()
            }
          }
        }
      }
    }
  });
  
  dropzone.on("sending", function(file, xhr, formData) {
    formData.append("product_id", document.querySelector('#upload-product-id').value);
    formData.append("_token", "{{csrf_token()}}");
  });

  dropzone.on("addedfile", function(file) {
    const uniqueId = Date.now() + Math.floor(Math.random()*100)
    $(".dz-preview:last").append(`
      <lable class="d-block mt-2">Select Color:</label>
      <select class="form-control product-color" onchange="changeColor(event)" data-image-id="">
        <option value="" selected>Select Color</option>
        @foreach($colors as $color)
          <option value="{{$color->id}}">{{$color->name}}</option>
        @endforeach
      </select>
    `);
    manageColorId()
  });

  async function reloadTable() {
    table.ajax.reload()
  }

  async function changeColor(event) {
    const payload = {
      file_id: event.target.dataset.imageId,
      color: event.target.value,
      product_id: document.querySelector('#upload-product-id').value,
      _token: "{{csrf_token()}}"
    }
    const param = prepareFetchParam(payload)

    let response = await fetch(`{{url('/products/change-image-color')}}`, param)
    if(response.ok) {
      response = await response.json()
      if(response.status) {
        console.log(response);
      }
    }
  }

  function manageColorId() {
    const colorInputs = document.querySelectorAll('.product-color')
    colorInputs.forEach((input, index) => {
      input.dataset.imageId = index
    })
  }
</script>
@endpush