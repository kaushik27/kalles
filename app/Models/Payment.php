<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    
    const ACTIVE_PAYMENT = 1;
    const INACTIVE_PAYMENT = 0;
}
