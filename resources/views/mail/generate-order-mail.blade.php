<!DOCTYPE html>
<html>
<head>
    <title>Generate Order Mail</title>
</head>
<body>  

  <img src="{{ asset('images/products/default.png') }}">

    <p>Hello {{$details['name']}},</p>

    <p>Thank you for choosing Amazing Ecom.</p>

    <table border="2" cellpadding="3" cellspacing="0">
      <thead>
        <tr>
          <th>Sr. No.</th>
          <th>Product Name</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Color</th>
          <th>Size</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($details['order_details']['cart_items'] as $key => $item)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$item['product_details']['name']}}</td>
            <td>{{$item['price']}}</td>
            <td>{{$item['quantity']}}</td>
            <td>{{$item['color_details']['name']}}</td>
            <td>{{$item['size_details']['size']}}</td>
            <td>{{$item['total_amount']}}</td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr style="text-align: right">
          <td colspan="6">
            <div style="margin-bottom: 5px">Sub Total</div>
            <div style="margin-bottom: 5px">Shipping Price</div>
            <div style="margin-bottom: 5px">COD Charge</div>
            <div>Final Amount</div>
          </td>
          <td>
            <div style="margin-bottom: 5px">{{$details['order_details']['sub_total_amount']}}</div>
            <div style="margin-bottom: 5px">{{$details['order_details']['shipping_price']}}</div>
            <div style="margin-bottom: 5px">{{$details['order_details']['cod']}}</div>
            <div>{{$details['order_details']['final_amount']}}</div>
          </td>
        </tr>
      </tfoot>
    </table>

    <p>
      Thanks,<br>
      Amazing Ecom Team.
    </p>
    
</body>
</html>