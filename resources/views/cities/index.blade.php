@extends('layouts.app')
@section('title', 'Amazing Ecom - Cities')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cities</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Cities</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of cities.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('cities.export')}}" class="btn btn-info mr-2" title="Export Cities"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create City" onclick="createCity()"><i class="fas fa-plus"></i> Create City</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="city-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>City Name</th>
                  <th>State Name</th>
                  <th>Shipping Price</th>
                  <th>Estimation Time</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create City Modal -->
<div class="modal fade" id="create-city-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-city-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create City</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-city-city">City Name</label>
              <input type="text" class="form-control" name="city" id="create-city-city" placeholder="City Name">
              <span class="error" id="create-city-city-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-city-city">State Name</label>
              <select name="state" id="create-city-state" class="form-control select2">
                <option value="">Select State</option>
                @foreach ($states as $state)
                  <option value="{{$state->id}}">{{$state->state}}</option>
                @endforeach
              </select>
              <span class="error" id="create-city-state-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-city-shipping-price">Shipping Price</label>
              <input type="number" class="form-control" name="shipping_price" id="create-city-shipping-price" min="0" max="100000" placeholder="Shipping Price">
              <span class="error" id="create-city-shipping-price-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-city-estimation-time">Estimation Time</label><br>
              <input type="hidden" class="form-control" name="estimation_time" id="create-city-estimation-time" placeholder="Estimation Time" value="0">
              <div class="mt-2" style="height: 38px">
                <select id="create-city-estimate-day" onchange="changeEstimationTime('create')">
                  @for ($i = 0; $i < 31; $i++)
                    <option value="{{$i}}">{{$i}}</option>                    
                  @endfor
                </select> Days
                <select id="create-city-estimate-hour" class="ml-1" onchange="changeEstimationTime('create')">
                  @for ($i = 0; $i < 24; $i++)
                    <option value="{{$i}}">{{$i}}</option>    
                  @endfor
                </select> Hours
                <select id="create-city-estimate-minute" class="ml-1" onchange="changeEstimationTime('create')">
                  @for ($i = 0; $i < 60; $i++)
                    <option value="{{$i}}">{{$i}}</option>                    
                  @endfor
                </select> Minutes
              </div>
              <span class="error" id="create-city-estimation-time-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="create-city-status">Status</label>
              <select name="status" id="create-city-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-city-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-city-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update City Modal -->
<div class="modal fade" id="update-city-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-city-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update City</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-city-city">City Name</label>
              <input type="hidden" class="form-control" name="id" id="update-city-id" placeholder="City ID">
              <input type="text" class="form-control" name="city" id="update-city-city" placeholder="City Name">
              <span class="error" id="update-city-city-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-city-city">State Name</label>
              <select name="state" id="update-city-state" class="form-control select2">
                <option value="">Select State</option>
                @foreach ($states as $state)
                  <option value="{{$state->id}}">{{$state->state}}</option>
                @endforeach
              </select>
              <span class="error" id="update-city-state-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-city-shipping-price">Shipping Price</label>
              <input type="number" class="form-control" name="shipping_price" id="update-city-shipping-price" min="0" max="100000" placeholder="Shipping Price">
              <span class="error" id="update-city-shipping-price-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-city-estimation-time">Estimation Time</label><br>
              <input type="hidden" class="form-control" name="estimation_time" id="update-city-estimation-time" placeholder="Estimation Time" value="0">
              <div class="mt-2" style="height: 38px">
                <select id="update-city-estimate-day" onchange="changeEstimationTime('update')">
                  @for ($i = 0; $i < 31; $i++)
                    <option value="{{$i}}">{{$i}}</option>                    
                  @endfor
                </select> Days
                <select id="update-city-estimate-hour" class="ml-1" onchange="changeEstimationTime('update')">
                  @for ($i = 0; $i < 24; $i++)
                    <option value="{{$i}}">{{$i}}</option>    
                  @endfor
                </select> Hours
                <select id="update-city-estimate-minute" class="ml-1" onchange="changeEstimationTime('update')">
                  @for ($i = 0; $i < 60; $i++)
                    <option value="{{$i}}">{{$i}}</option>                    
                  @endfor
                </select> Minutes
              </div>
              <span class="error" id="update-city-estimation-time-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="update-city-status">Status</label>
              <select name="status" id="update-city-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-city-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-city-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#city-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("cities.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'city', name: 'city'},
      {data: 'state_name', name: 'state_name'},
      {data: 'shipping_price', name: 'shipping_price'},
      {data: 'estimation_time', name: 'estimation_time'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })

  // create city
  function createCity() {
    $('#create-city-modal').modal('show');
  }

  let createCityFormValidator = $('#create-city-form').validate({
    rules: {
      city: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      state: {
        required: true
      },
      shipping_price: {
        required: true,
        number: true,
        min: 0,
        max: 100000
      },
      estimation_time: {
        required: true
      },
      status: {
        required: true
      }
    },
    messages: {
      city: {
        required: "City name is required.",
      },
      state: {
        required: "State name is required.",
      },
      shipping_price: {
        required: "Shipping price is required."
      },
      estimation_time: {
        required: "Estimation time is required."
      },
      status: {
        required: "Status is required."
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "state") {
        error.appendTo('#create-city-state-error')
      } else if(element.attr('name') == "estimation_time") {
        error.appendTo('#update-city-estimation-time-error')
      } else if(element.attr('name') == "status") {
        error.appendTo('#create-city-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        city: document.querySelector('#create-city-city').value,
        state: document.querySelector('#create-city-state').value,
        shipping_price: document.querySelector('#create-city-shipping-price').value,
        estimation_time: document.querySelector('#create-city-estimation-time').value,
        status: document.querySelector('#create-city-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeCity(param)
    }
  })

  async function storeCity(param) {
    const submitButton = document.querySelector('#create-city-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateCityForm()

      let response = await fetch("{{route('cities.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-city-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.city) document.querySelector('#create-city-city-error').textContent = response.message.city[0]
          if(response?.message?.state) document.querySelector('#create-city-state-error').textContent = response.message.state[0]
          if(response?.message?.shipping_price) document.querySelector('#create-city-shipping-price-error').textContent = response.message.shipping_price[0]
          if(response?.message?.estimation_time) document.querySelector('#create-city-estimation-time-error').textContent = response.message.estimation_time[0]
          if(response?.message?.status) document.querySelector('#create-city-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-city-modal').on('hidden.bs.modal', event => {
    resetCreateCityForm();
  })

  function resetCreateCityForm() {
    const form = document.querySelector('#create-city-form')
    form.reset()
    createCityFormValidator.resetForm()
    document.querySelector('#create-city-city-error').textContent = ""
    document.querySelector('#create-city-state-error').textContent = ""
    document.querySelector('#create-city-shipping-price-error').textContent = ""
    document.querySelector('#create-city-estimation-time-error').textContent = ""
    document.querySelector('#create-city-status-error').textContent = ""
  }
  

  // update city
  async function editCity(city) {
    $('#update-city-modal').modal('show');

    try {
      let response = await fetch(`{{url('/cities/edit')}}/${city}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-city-id').value = response.data.id;
        document.querySelector('#update-city-city').value = response.data.city;

        const state = document.querySelector('#update-city-state')
        state.value = response.data.state_id
        state.dispatchEvent(new Event("change"));

        document.querySelector('#update-city-shipping-price').value = response.data.shipping_price;

        const estimationTime = response.data.estimation_time.split(':');
        document.querySelector('#update-city-estimate-day').value = estimationTime[0];
        document.querySelector('#update-city-estimate-hour').value = estimationTime[1];
        document.querySelector('#update-city-estimate-minute').value = estimationTime[2];
        changeEstimationTime('update')

        const status = document.querySelector('#update-city-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateCityFormValidator = $('#update-city-form').validate({
    rules: {
      city: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      state: {
        required: true
      },
      shipping_price: {
        required: true,
        number: true,
        min: 0,
        max: 100000
      },
      estimation_time: {
        required: true
      },
      status: {
        required: true
      }
    },
    messages: {
      city: {
        required: "City name is required.",
      },
      state: {
        required: "State name is required.",
      },
      shipping_price: {
        required: "Shipping price is required."
      },
      estimation_time: {
        required: "Estimation time is required."
      },
      status: {
        required: "Status is required."
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "state") {
        error.appendTo('#update-city-state-error')
      } else if(element.attr('name') == "estimation_time") {
        error.appendTo('#update-city-estimation-time-error')
      } else if(element.attr('name') == "status") {
        error.appendTo('#update-city-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const cityID = document.querySelector('#update-city-id').value
      const payload = {
        city: document.querySelector('#update-city-city').value,
        state: document.querySelector('#update-city-state').value,
        shipping_price: document.querySelector('#update-city-shipping-price').value,
        estimation_time: document.querySelector('#update-city-estimation-time').value,
        status: document.querySelector('#update-city-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateCity(param, cityID)
    }
  })

  async function updateCity(param, city) {
    const submitButton = document.querySelector('#update-city-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateCityForm()

      let response = await fetch(`{{url('/cities/update')}}/${city}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-city-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.city) document.querySelector('#update-city-city-error').textContent = response.message.city[0]
          if(response?.message?.state) document.querySelector('#update-city-state-error').textContent = response.message.state[0]
          if(response?.message?.shipping_price) document.querySelector('#update-city-shipping-price-error').textContent = response.message.shipping_price[0]
          if(response?.message?.estimation_time) document.querySelector('#update-city-estimation-time-error').textContent = response.message.estimation_time[0]
          if(response?.message?.status) document.querySelector('#update-city-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-city-modal').on('hidden.bs.modal', event => {
    resetUpdateCityForm();
  })

  function resetUpdateCityForm() {
    updateCityFormValidator.resetForm()
    document.querySelector('#update-city-city-error').textContent = ""
    document.querySelector('#update-city-status-error').textContent = ""
    document.querySelector('#update-city-shipping-price-error').textContent = ""
    document.querySelector('#update-city-estimation-time-error').textContent = ""
    document.querySelector('#update-city-status-error').textContent = ""
  }


  // delete city
  function deleteCity(city) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/cities/delete')}}/${city}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of city
  function changeCityStatus(city) {
    swal({
      title: "Are you sure?",
      text: "You want to change city status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/cities/change-status')}}/${city}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // change estimation time
  function changeEstimationTime(type) {
    const days = document.querySelector(`#${type}-city-estimate-day`).value
    const hours = document.querySelector(`#${type}-city-estimate-hour`).value
    const minutes = document.querySelector(`#${type}-city-estimate-minute`).value
    const daysToSec = days * 24 * 60 * 60
    const hoursToSec = hours * 60 * 60
    const minutesToSec = minutes * 60
    const totalSeconds = daysToSec + hoursToSec + minutesToSec;
    document.querySelector(`#${type}-city-estimation-time`).value = totalSeconds
  }
</script>
@endpush