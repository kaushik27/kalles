<?php

namespace App\Exports\Admin\City;

use App\Models\City;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CitiesExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $cities = City::select('cities.*', 'states.state as state_name')->leftJoin('states', 'states.id', 'cities.state_id')->get();
        $all_cities = [];
        foreach ($cities as $key => $city) {
            $single_city = [];
            $single_city['id'] = $city->id;
            $single_city['city'] = $city->city;
            $single_city['state_name'] = $city->state_name;
            $single_city['shipping_price'] = $city->shipping_price;
            $single_city['estimation_time'] = $city->estimation_time ? durationFromSeconds($city->estimation_time) : "";
            $single_city['status'] = $city->status ? "Active" : "Inactive";
            $all_cities[] = $single_city;
        }

        return collect($all_cities);
    }

    public function headings(): array
    {
        return ["ID", "City Name", "State Name", "Shipping Price", 'Estimation Time', "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
