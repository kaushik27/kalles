<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\User\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\ChangeUserAddressRequest;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Mail\EmailVerifyMail;
use App\Models\City;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use Faker\Provider\ar_EG\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index()
    {
        $states = State::where('status', State::ACTIVE_STATE)->get();
        return view('users.index', compact('states'));
    }

    public function getData() {
        $users = User::select('users.*', 'ua.address1', 'ua.address2', 'ua.city', 'ua.state')
                    ->leftJoin('user_addresses as ua', 'ua.user_id', 'users.id')
                    ->where('role', User::USER_ROLE)
                    ->get();
        
        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('fname', function($user){
                return ucfirst($user->fname);
            })
            ->editColumn('lname', function($user){
                return ucfirst($user->lname);
            })
            ->editColumn('phone', function($user){
                return $user->phone ?? "-";
            })
            ->addColumn('address', function($user){
                $city = $user->city ? City::find($user->city)->city : "";
                $state = $user->state ? State::find($user->state)->state : "";
                $address = "";
                $address .= $user->address1 ? $user->address1.", " : "";
                $address .= $user->address2 ? $user->address2.", <br>" : "";
                $address .= $city ? $city.", " : "";
                $address .= $state ? $state.". " : "";
                return $address ? $address : "-";
            })
            ->editColumn('status', function($user){
                return $user->status 
                    ? '<span class="badge badge-success" onclick="changeUserStatus('.$user->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeUserStatus('.$user->id.')">Inactive</span>';
            })
            ->addColumn('action', function($user){
                return '
                    <button class="btn btn-info mr-2" title="Edit User" onclick="editUser('.$user->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-success mr-2" title="Change Address" onclick="changeAddress('.$user->id.')"><i class="fas fa-address-book"></i></button>
                    <button class="btn btn-danger" title="Delete User" onclick="deleteUser('.$user->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['address', 'status', 'action'])
            ->make(true);
    }

    public function create(CreateUserRequest $request) {
        $user = new User();
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone; 
        $user->role = User::USER_ROLE; 
        $user->token = Str::random(32);
        if($user->save()) {

            // send email verification mail
            try {
                $details = [
                    'name' =>  $user->fname.' '.$user->lname,
                    'link'  =>  url('/email-verify/'.$user->token),
                ];
                Mail::to($user->email)->send(new EmailVerifyMail($details));
            } catch (\Throwable $th) {
                return response()->json([
                    'status'    =>  1,
                    'message'   =>  "User created successfully, but failed to sent verification mail to user!"
                ]);
            }

            return response()->json([
                'status'    =>  1,
                'message'   =>  "User created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(User $user) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $user
        ]);
    }

    public function update(UpdateUserRequest $request, User $user) {
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if($user->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "User updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(User $user) {

        // delete user address
        UserAddress::where('user_id', $user->id)->delete();

        if($user->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'User deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function getCities($state) {
        $cities = City::where('state_id', $state)->where('status', City::ACTIVE_CITY)->get();
        return response()->json([
            'status'    =>  1,
            'data'      =>  $cities
        ]);
    }

    public function getUserAddress(User $user) {
        $address = UserAddress::where('user_id', $user->id)->first();
        return response()->json([
            'status'    =>  1,
            'data'      =>  $address
        ]);
    }

    public function udpateAddress(ChangeUserAddressRequest $request, User $user) {
        $user_address = UserAddress::where('user_id', $user->id)->first();
        if(!$user_address) {
            $user_address = new UserAddress();
            $user_address->user_id = $user->id;
        }
        $user_address->address1 = $request->address1;
        $user_address->address2 = $request->address2;
        $user_address->city = $request->city;
        $user_address->state = $request->state;
        $user_address->pincode = $request->pincode;
        if($user_address->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'User address udpated successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(User $user) {
        $user->status = $user->status == User::ACTIVE_USER ? User::INACTIVE_USER : User::ACTIVE_USER;
        if($user->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'User status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
