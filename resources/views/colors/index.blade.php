@extends('layouts.app')
@section('title', 'Amazing Ecom - Colors')

@push('head-script')
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
@endpush

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Colors</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Colors</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of colors.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('colors.export')}}" class="btn btn-info mr-2" title="Export Colors"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Color" onclick="createColor()"><i class="fas fa-plus"></i> Create Color</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="color-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Name</th>
                  <th>Color Code</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Color Modal -->
<div class="modal fade" id="create-color-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-color-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Color</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-color-name">Color Name</label>
              <input type="text" class="form-control" name="name" id="create-color-name" placeholder="Color Name">
              <span class="error" id="create-color-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-color-name">Color Code</label>
              <input type="text" class="form-control color-picker" name="code" id="create-color-code" value="#000000" placeholder="Color Code" autocomplete="off">
              <span class="error" id="create-color-code-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="create-color-status">Status</label>
              <select name="status" id="create-color-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-color-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-color-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Color Modal -->
<div class="modal fade" id="update-color-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-color-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Color</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-color-name">Color Name</label>
              <input type="hidden" class="form-control" name="id" id="update-color-id" placeholder="Color ID">
              <input type="text" class="form-control" name="name" id="update-color-name" placeholder="Color Name">
              <span class="error" id="update-color-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-color-name">Color Code</label>
              <input type="text" class="form-control color-picker" name="code" id="update-color-code" value="#000000" placeholder="Color Code" autocomplete="off">
              <span class="error" id="update-color-code-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="update-color-status">Status</label>
              <select name="status" id="update-color-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-color-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-color-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<!-- bootstrap color picker -->
<script src="{{asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<script>
  $('.color-picker').colorpicker()

  let table = $('#color-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("colors.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'name', name: 'name'},
      {data: 'color', name: 'color'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })

  // create color
  function createColor() {
    $('#create-color-modal').modal('show');
  }

  let createColorFormValidator = $('#create-color-form').validate({
    rules: {
      name: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      code: {
        required: true,
        pattern: "^[#]{1}[a-zA-Z0-9]{6}$"
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Color name is required.",
      },
      code: {
        required: "Color code is required.",
        pattern: "Pleae enter valid color formate. Ex.: #000000"
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#create-color-status-error')
      } else if(element.attr('name') == "code") {
        error.appendTo('#create-color-code-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        name: document.querySelector('#create-color-name').value,
        code: document.querySelector('#create-color-code').value,
        status: document.querySelector('#create-color-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeColor(param)
    }
  })

  async function storeColor(param) {
    const submitButton = document.querySelector('#create-color-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateColorForm()

      let response = await fetch("{{route('colors.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-color-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#create-color-name-error').textContent = response.message.name[0]
          if(response?.message?.code) document.querySelector('#create-color-code-error').textContent = response.message.code[0]
          if(response?.message?.status) document.querySelector('#create-color-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-color-modal').on('hidden.bs.modal', event => {
    resetCreateColorForm();
  })

  function resetCreateColorForm() {
    const form = document.querySelector('#create-color-form')
    form.reset()
    createColorFormValidator.resetForm()
    document.querySelector('#create-color-name-error').textContent = ""
    document.querySelector('#create-color-code-error').textContent = ""
    document.querySelector('#create-color-status-error').textContent = ""
  }

  // update color
  async function editColor(color) {
    $('#update-color-modal').modal('show');

    try {
      let response = await fetch(`{{url('/colors/edit')}}/${color}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-color-id').value = response.data.id;
        document.querySelector('#update-color-name').value = response.data.name;
        document.querySelector('#update-color-code').value = response.data.color;
        const status = document.querySelector('#update-color-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateColorFormValidator = $('#update-color-form').validate({
    rules: {
      name: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      code: {
        required: true,
        pattern: "^[#]{1}[a-zA-Z0-9]{6}$"
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Color name is required.",
      },
      code: {
        required: "Color code is required.",
        pattern: "Pleae enter valid color formate. Ex.: #000000"
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#update-color-status-error')
      } else if(element.attr('name') == "code") {
        error.appendTo('#update-color-code-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const colorID = document.querySelector('#update-color-id').value
      const payload = {
        name: document.querySelector('#update-color-name').value,
        code: document.querySelector('#update-color-code').value,
        status: document.querySelector('#update-color-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateColor(param, colorID)
    }
  })

  async function updateColor(param, color) {
    const submitButton = document.querySelector('#update-color-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateColorForm()

      let response = await fetch(`{{url('/colors/update')}}/${color}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-color-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#update-color-name-error').textContent = response.message.name[0]
          if(response?.message?.code) document.querySelector('#update-color-code-error').textContent = response.message.code[0]
          if(response?.message?.status) document.querySelector('#update-color-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-color-modal').on('hidden.bs.modal', event => {
    resetUpdateColorForm();
  })

  function resetUpdateColorForm() {
    updateColorFormValidator.resetForm()
    document.querySelector('#update-color-name-error').textContent = ""
    document.querySelector('#update-color-code-error').textContent = ""
    document.querySelector('#update-color-status-error').textContent = ""
  }

  // delete color
  function deleteColor(color) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/colors/delete')}}/${color}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of color
  function changeColorStatus(color) {
    swal({
      title: "Are you sure?",
      text: "You want to change color status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/colors/change-status')}}/${color}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

</script>
@endpush