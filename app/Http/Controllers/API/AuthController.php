<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Auth\ForgotPasswordRequest;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Requests\API\Auth\RegisterRequest;
use App\Http\Requests\API\Auth\UpdateAddressRequest;
use App\Http\Requests\API\Auth\UpdateProfileRequest;
use App\Mail\EmailVerifyMail;
use App\Mail\ForgotPasswordMail;
use App\Models\City;
use App\Models\Order;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends BaseController
{
    /**
     * Login api
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function login(LoginRequest $request) {
        $user = User::where('email', $request->email)->first();

        if(!$user)
            return $this->sendError("Email not exist in our database.");

        if(!$user->email_verified) 
            return $this->sendError("Login failed! Email must be verified before login.");

        if($user->status != User::ACTIVE_USER)
            return $this->sendError("You have not access to login, please contact to admin.");
            
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            return $this->sendError("Your password is wrong, Please enter currect password.");

        $data['token'] = $user->createToken('amazing_ecom')->plainTextToken;

        // set user address
        $user->address = $this->prepareAddressResponse($user->id);

        $data['user'] = $user;

        return $this->sendResponse('User login successfully.', $data);
    }

    /**
     * Register api
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function register(RegisterRequest $request) {

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['role'] = User::USER_ROLE;
        $input['token'] = Str::random(32);

        if(User::create($input)) {
            // send email verification mail
            try {
                $details = [
                    'name' =>  $input['fname'].' '.$input['lname'],
                    'link'  =>  url('/email-verify/'.$input['token']),
                ];
                Mail::to($input['email'])->send(new EmailVerifyMail($details));
            } catch (\Throwable $th) {
                return $this->sendError("Failed, Mail has not been sent successfully!", [], 424);
            }

            return $this->sendResponse('We have sent mail successfully, please check your mail and verify it.');
        } else {
            return $this->sendError('Registration failed, Something went wrong!', [], 400);
        }

    }

    /**
     * Forgot password api
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function forgotPassword(ForgotPasswordRequest $request) {

        $user = User::where('email', $request->email)->first();

        if(!$user) 
            return $this->sendError("Email not exist in our database.");

        $token = Str::random(32);

        DB::table('password_resets')->updateOrInsert(
            ['email' => $request->email],
            ['token' => $token, 'created_at' => date('Y-m-d H:i:s')]
        );

        try {
            $details = [
                'name' =>  $user->fname.' '.$user->lname,
                'link'  =>  url('/reset-password/'.$token),
            ];
            Mail::to($user->email)->send(new ForgotPasswordMail($details));
        } catch (\Throwable $th) {
            return $this->sendError("Failed, Mail has not been sent successfully!", [], 424);
        }

        return $this->sendResponse('We have sent mail successfully, please check your mail and click to link for reset your password.');

    }

    /**
     * Logout api
     * 
     * @param  Request  $request
     * 
     * @return  Response
     */
    public function logout(Request $request) {

        $request->user('sanctum')->currentAccessToken()->delete();
    
        return $this->sendResponse("You are logged out successfully!");
    }

    public function profile(Request $request) {
        $user = User::find($request->user('sanctum')->id);

        if($user) {
            $user->address = $this->prepareAddressResponse($user->id);
            return $this->sendResponse("", $user);
        } else {
            return $this->sendError("User not exist, Please enter valid user id.", [], 400);
        }
    }

    public function prepareAddressResponse($user_id) {
        $user_address = UserAddress::select('user_addresses.*', 'c.city as city_name', 'c.status as city_status', 's.state as state_name', 's.status as state_status')
                                ->leftJoin('states as s', 's.id', 'user_addresses.state')
                                ->leftJoin('cities as c', 'c.id', 'user_addresses.city')
                                ->where('user_id', $user_id)
                                ->first();

        if($user_address) {
            $address = [];
            $address['id']  =  $user_address->id;
            $address['user_id']  =  $user_address->user_id;
            $address['address1']  =  $user_address->address1;
            $address['address2']  =  $user_address->address2;
            $address['city']  =  $user_address->city;
            $address['state']  =  $user_address->state;
            $address['city_name']  =  $user_address->city_name;
            $address['state_name']  =  $user_address->state_name;
            $address['pincode']  =  $user_address->pincode;
            $address['is_service_enable']  =  $user_address->city_status && $user_address->state_status ? 1 : 0;
            return $address;
        } else {
            return (object) [];
        }
    }

    public function updateProfile(UpdateProfileRequest $request) {
        $user = User::find($request->user('sanctum')->id);

        if($user) {
            // update user profile
            $user->fname = $request->fname;
            $user->lname = $request->lname;
            $user->phone = $request->phone;
            $user->save();
            
            // set address
            $user->address = $this->prepareAddressResponse($user->id);

            return $this->sendResponse("Profile updated successfully!", $user);
        } else {
            return $this->sendError("User not exist, Please enter valid user id.", [], 400);
        }
    }

    public function updateUserAddress(UpdateAddressRequest $request) {        
        $user = User::find($request->user('sanctum')->id);
        if($user) {

            // update or create user address
            UserAddress::updateOrCreate(['user_id' => $user->id], [
                'address1'  => $request->address1,
                'address2'  => $request->address2,
                'city'      => $request->city,
                'state'     => $request->state,
                'pincode'   => $request->pincode
            ]);

            $user->address = $this->prepareAddressResponse($user->id);

            // add shipping price to order (cart)
            $order = Order::where('user_id', $user->id)
                        ->where('status', Order::ACTIVE_ORDER)
                        ->whereNull('payment_id')
                        ->first();
            if($order) {
                $order->shipping_price = User::shippingPrice();
                $order->final_amount = $order->sub_total_amount + $order->shipping_price;
                $order->save();
            }

            return $this->sendResponse("Address updated successfully!", $user);
        } else {
            return $this->sendError("User not exist, Please enter valid user id in url", [],  400);
        }
    }

    public function getStates() {
        $states = State::where('status', State::ACTIVE_STATE)->get()->toArray();
        if(empty($states)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $states);
    }

    public function getCities($state_id) {
        $cities = City::where('state_id', $state_id)->where('status', City::ACTIVE_CITY)->get();

        if(empty($cities->toArray())) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []
            ]);
        }
        
        foreach ($cities as $key => $city) {
            $city->estimation_time = $city->estimation_time ? durationFromSeconds($city->estimation_time) : "";
        }
        
        return $this->sendResponse("", $cities);
    }
}
