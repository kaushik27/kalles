<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\State;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index() {

        $user_count = User::where('role', User::USER_ROLE)->count();
        $vendor_count = Vendor::count();
        $product_count = Product::count();
        $state_count = State::count();
        $categories_count = Category::count();
        $completed_order_count = Order::where('status', Order::INACTIVE_ORDER)->withTrashed()->count();
        $pending_order_count = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                    ->where('orders.status', Order::ACTIVE_ORDER)
                                    ->whereNotNull('orders.payment_id')
                                    ->withTrashed()
                                    ->count();
        $payment_count = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                    ->where('orders.status', Order::INACTIVE_ORDER)
                                    ->whereNotNull('orders.payment_id')
                                    ->withTrashed()
                                    ->sum('orders.final_amount');

        return view('dashboard', compact([
            'user_count',
            'vendor_count',
            'product_count',
            'state_count',
            'categories_count',
            'completed_order_count',
            'pending_order_count',
            'payment_count'
        ]));
    }

    public function state_wise_user_chart() {
        $stateWiseUsers = User::select('ua.state', DB::raw('COUNT(*) as user_count'))
                    ->leftJoin('user_addresses as ua', 'ua.user_id', 'users.id')
                    ->where('role', User::USER_ROLE)
                    ->groupBy('ua.state')
                    ->pluck('user_count', 'ua.state')
                    ->toArray();

        $response = [];
        $response['states'] = [];
        $response['users'] = [];
        if(!empty($stateWiseUsers)) {
            foreach ($stateWiseUsers as $key => $users) {
                $state = State::find($key);
                if($state) {
                    $response['states'][] = $state->state;
                    $response['users'][] = $users;
                }
            }
        }
        
        return response()->json([
            'status'    =>  1,
            'data'      =>  $response
        ]);
    }

    public function registered_user_chart($duration) {

        $response = [];

        switch ($duration) {
            case 'last3month':
                $labels = [
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereBetween('created_at', [$start, $end])
                                        ->count();
                }
                break;

            case 'last6month':
                $labels = [
                    date('F Y', strtotime("-6 months")),
                    date('F Y', strtotime("-5 months")),
                    date('F Y', strtotime("-4 months")),
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereBetween('created_at', [$start, $end])
                                        ->count();
                }
                break;

            case 'last12month':
                $labels = [
                    date('F Y', strtotime("-12 months")),
                    date('F Y', strtotime("-11 months")),
                    date('F Y', strtotime("-10 months")),
                    date('F Y', strtotime("-9 months")),
                    date('F Y', strtotime("-8 months")),
                    date('F Y', strtotime("-7 months")),
                    date('F Y', strtotime("-6 months")),
                    date('F Y', strtotime("-5 months")),
                    date('F Y', strtotime("-4 months")),
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereBetween('created_at', [$start, $end])
                                        ->count();
                }
                break;
                
            case 'thisYear':
                $labels = [];
                for($i = 0; $i < date('m'); $i++) {
                    if($i == 0) {
                        $labels[] = date('F Y', strtotime(date('Y-01-01')));
                    } else {
                        $labels[] = date('F Y', strtotime("+".$i." months", strtotime(date('Y-01-01'))));
                    }
                }
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereBetween('created_at', [$start, $end])
                                        ->count();
                }
                break;

            case 'lastYear':
                $last_year = date('Y') - 1;
                $labels = ["January ".$last_year, "February ".$last_year, "March ".$last_year, "April ".$last_year, "May ".$last_year, "June ".$last_year, "July ".$last_year, "August ".$last_year, "September ".$last_year, "October ".$last_year, "November ".$last_year, "December".$last_year];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereBetween('created_at', [$start, $end])
                                        ->count();
                }
                break;
            
            default:
                $labels = [
                    date('d M, Y', strtotime("-7 days")),
                    date('d M, Y', strtotime("-6 days")),
                    date('d M, Y', strtotime("-5 days")),
                    date('d M, Y', strtotime("-4 days")),
                    date('d M, Y', strtotime("-3 days")),
                    date('d M, Y', strtotime("-2 days")),
                    date('d M, Y', strtotime("-1 days"))
                ];
                foreach ($labels as $key => $label) {
                    $date = date('Y-m-d', strtotime($label));
                    $response[$label] = User::where('role', User::USER_ROLE)
                                        ->whereDate('created_at', $date)
                                        ->count();
                }
                break;
        }
        
        return response()->json([
            'status'    =>  1,
            'data'      =>  $response
        ]);
    }

    public function order_chart($duration) {

        $response = [];

        switch ($duration) {
            case 'last3month':
                $labels = [
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereBetween('created_at', [$start, $end])->withTrashed()->count();
                }
                break;

            case 'last6month':
                $labels = [
                    date('F Y', strtotime("-6 months")),
                    date('F Y', strtotime("-5 months")),
                    date('F Y', strtotime("-4 months")),
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereBetween('created_at', [$start, $end])->withTrashed()->count();
                }
                break;

            case 'last12month':
                $labels = [
                    date('F Y', strtotime("-12 months")),
                    date('F Y', strtotime("-11 months")),
                    date('F Y', strtotime("-10 months")),
                    date('F Y', strtotime("-9 months")),
                    date('F Y', strtotime("-8 months")),
                    date('F Y', strtotime("-7 months")),
                    date('F Y', strtotime("-6 months")),
                    date('F Y', strtotime("-5 months")),
                    date('F Y', strtotime("-4 months")),
                    date('F Y', strtotime("-3 months")),
                    date('F Y', strtotime("-2 months")),
                    date('F Y', strtotime("-1 months"))
                ];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereBetween('created_at', [$start, $end])->withTrashed()->count();
                }
                break;
                
            case 'thisYear':
                $labels = [];
                for($i = 0; $i < date('m'); $i++) {
                    if($i == 0) {
                        $labels[] = date('F Y', strtotime(date('Y-01-01')));
                    } else {
                        $labels[] = date('F Y', strtotime("+".$i." months", strtotime(date('Y-01-01'))));
                    }
                }
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereBetween('created_at', [$start, $end])->withTrashed()->count();
                }
                break;

            case 'lastYear':
                $last_year = date('Y') - 1;
                $labels = ["January ".$last_year, "February ".$last_year, "March ".$last_year, "April ".$last_year, "May ".$last_year, "June ".$last_year, "July ".$last_year, "August ".$last_year, "September ".$last_year, "October ".$last_year, "November ".$last_year, "December".$last_year];
                foreach ($labels as $key => $label) {
                    $start = date('Y-m-01', strtotime($label));
                    $end = date('Y-m-t', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereBetween('created_at', [$start, $end])->withTrashed()->count();
                }
                break;
            
            default:
                $labels = [
                    date('d M, Y', strtotime("-7 days")),
                    date('d M, Y', strtotime("-6 days")),
                    date('d M, Y', strtotime("-5 days")),
                    date('d M, Y', strtotime("-4 days")),
                    date('d M, Y', strtotime("-3 days")),
                    date('d M, Y', strtotime("-2 days")),
                    date('d M, Y', strtotime("-1 days"))
                ];
                foreach ($labels as $key => $label) {
                    $date = date('Y-m-d', strtotime($label));
                    $response[$label] = Order::whereNotNull('payment_id')->whereDate('created_at', $date)->withTrashed()->count();
                }
                break;
        }
        
        return response()->json([
            'status'    =>  1,
            'data'      =>  $response
        ]);
    }
}
