<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const ACTIVE_PRODUCT = 1;
    const INACTIVE_PRODUCT = 0;

    const SORT_FEATURED = "0";
    const SORT_BEST_SELLING = "1";
    const SORT_ALPHA_A_TO_Z = "2";
    const SORT_ALPHA_Z_TO_A = "3";
    const SORT_PRICE_LOW_TO_HIGH = "4";
    const SORT_PRICE_HIGH_TO_LOW = "5";
    const SORT_DATE_OLD_TO_NEW = "6";
    const SORT_DATE_NEW_TO_OLD = "7";

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
