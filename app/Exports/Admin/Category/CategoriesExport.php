<?php

namespace App\Exports\Admin\Category;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CategoriesExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $categories = Category::all();
        $all_categories = [];
        foreach ($categories as $key => $category) {
            $single_category = [];
            $single_category['id'] = $category->id;
            $single_category['category_name'] = $category->name;
            $single_category['image'] = is_file(public_path('images/category/'.$category->image)) && file_exists(public_path('images/category/'.$category->image))
                    ? asset('images/category/'.$category->image)
                    : asset('images/category/default.png');
            $all_categories[] = $single_category;
        }

        return collect($all_categories);
    }

    public function headings(): array
    {
        return ["ID", "Category Name", "Category Image"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
