<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\SubCategory\SubCategoriesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SubCategory\CreateSubCategoryRequest;
use App\Http\Requests\Admin\SubCategory\UpdateSubCategoryRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class SubCategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('status', Category::ACTIVE_CATEGORY)->get();
        return view('sub-categories.index', compact('categories'));
    }

    public function getData() {
        $sub_categories = SubCategory::select('sub_categories.*', 'categories.name as category_name')->leftJoin('categories', 'categories.id', 'sub_categories.category_id')->get();
        
        return DataTables::of($sub_categories)
            ->addIndexColumn()
            ->editColumn('status', function($sub_category){
                return $sub_category->status 
                    ? '<span class="badge badge-success" onclick="changeSubCategoryStatus('.$sub_category->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeSubCategoryStatus('.$sub_category->id.')">Inactive</span>';
            })
            ->addColumn('action', function($sub_category){
                return '
                    <button class="btn btn-info mr-2" title="Edit sub category" onclick="editSubCategory('.$sub_category->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-danger" title="Delete sub category" onclick="deleteSubCategory('.$sub_category->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create(CreateSubCategoryRequest $request) {
        $sub_category = new SubCategory();
        $sub_category->name = $request->name;
        $sub_category->category_id = $request->category;
        $sub_category->status = $request->status;
        if($sub_category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Sub category created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(SubCategory $sub_category) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $sub_category
        ]);
    }

    public function update(UpdateSubCategoryRequest $request, SubCategory $sub_category) {
        $sub_category->name = $request->name;
        $sub_category->category_id = $request->category;
        $sub_category->status = $request->status;
        if($sub_category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Sub category updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(SubCategory $sub_category) {
        $products = Product::all();
        foreach ($products as $key => $product) {
            $sub_categories = explode(',', $product->sub_categories);
            if(in_array($sub_category->id, $sub_categories)) {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  "Products already found with this sub category, First You need to remove this sub category's products ."
                ]);
            }
        }

        if($sub_category->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Sub category deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(SubCategory $sub_category) {
        $sub_category->status = $sub_category->status == SubCategory::ACTIVE_SUB_CATEGORY ? SubCategory::INACTIVE_SUB_CATEGORY : SubCategory::ACTIVE_SUB_CATEGORY;
        if($sub_category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Sub category status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function getCategories() {
        $categories = Category::where('status', Category::ACTIVE_CATEGORY)->get();
        return response()->json([
            'status'    =>  1,
            'data'      =>  $categories
        ]);
    }

    public function export() {
        return Excel::download(new SubCategoriesExport, 'sub-categories.xlsx');
    }
}
