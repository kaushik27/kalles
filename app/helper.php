<?php

function ago_time($datetime, $full = false) {
  $now = new DateTime;
  $ago = new DateTime($datetime);
  $diff = $now->diff($ago);

  $diff->w = floor($diff->d / 7);
  $diff->d -= $diff->w * 7;

  $string = array(
      'y' => 'year',
      'm' => 'month',
      'w' => 'week',
      'd' => 'day',
      'h' => 'hour',
      'i' => 'minute',
      's' => 'second',
  );
  foreach ($string as $k => &$v) {
      if ($diff->$k) {
          $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
      } else {
          unset($string[$k]);
      }
  }

  if (!$full) $string = array_slice($string, 0, 1);
  return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function durationFromSeconds($time, $type = "") {
    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@".$time);
    $duration = $dt1->diff($dt2);

    if($type == "edit") return $duration->format("%a:%h:%i");

    $formate_type = [];
    ($duration->d > 0) ? $formate_type[] = "%a Days" : "";
    ($duration->h > 0) ? $formate_type[] = "%h Hours" : "";
    ($duration->i > 0) ? $formate_type[] = "%i Minutes" : "";

    return $duration->format(implode(' ', $formate_type));
}