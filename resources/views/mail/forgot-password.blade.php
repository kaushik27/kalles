<!DOCTYPE html>
<html>
<head>
    <title>Forgot Password Mail</title>
</head>
<body>  

    <p>Hello {{$details['name']}},</p>

    <p>You can reset your password using following link:</p>

    <a href="{{$details['link']}}">{{$details['link']}}</a>

    <p>
        Thanks,<br>
        Amazing Ecom Team.
    </p>
    
</body>
</html>