@extends('layouts.app')
@section('title', 'Amazing Ecom - Vendors')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Vendors</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Vendors</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of vendors.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('vendors.export')}}" class="btn btn-info mr-2" title="Export Vendors"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Vendor" onclick="createVendor()"><i class="fas fa-plus"></i> Create Vendor</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vendor-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Vendor Modal -->
<div class="modal fade" id="create-vendor-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-vendor-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Vendor</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-vendor-fname">First Name</label>
              <input type="text" class="form-control" name="fname" id="create-vendor-fname" placeholder="First Name">
              <span class="error" id="create-vendor-fname-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-vendor-lname">Last Name</label>
              <input type="text" class="form-control" name="lname" id="create-vendor-lname" placeholder="Last Name">
              <span class="error" id="create-vendor-lname-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-vendor-email">Email</label>
              <input type="text" class="form-control" name="email" id="create-vendor-email" placeholder="Email">
              <span class="error" id="create-vendor-email-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-vendor-phone">Phone Number</label>
              <input type="text" class="form-control" name="phone" id="create-vendor-phone" placeholder="Phone Number">
              <span class="error" id="create-vendor-phone-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-vendor-address">Address</label>
              <textarea name="address" id="create-vendor-address" class="form-control" id="create-vendor-address" rows="3"></textarea>
              <span class="error" id="create-vendor-address-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-vendor-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Vendor Modal -->
<div class="modal fade" id="update-vendor-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-vendor-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Vendor</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <input type="hidden" class="form-control" name="id" id="update-vendor-id" placeholder="vendor ID">
              <label for="update-vendor-fname">First Name</label>
              <input type="text" class="form-control" name="fname" id="update-vendor-fname" placeholder="First Name">
              <span class="error" id="update-vendor-fname-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-vendor-lname">Last Name</label>
              <input type="text" class="form-control" name="lname" id="update-vendor-lname" placeholder="Last Name">
              <span class="error" id="update-vendor-lname-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-vendor-email">Email</label>
              <input type="text" class="form-control" name="email" id="update-vendor-email" placeholder="Email">
              <span class="error" id="update-vendor-email-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-vendor-phone">Phone Number</label>
              <input type="text" class="form-control" name="phone" id="update-vendor-phone" placeholder="Phone Number">
              <span class="error" id="update-vendor-phone-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-vendor-address">Address</label>
              <textarea name="address" id="update-vendor-address" class="form-control" id="update-vendor-address" rows="3"></textarea>
              <span class="error" id="update-vendor-address-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-vendor-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#vendor-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("vendors.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'fname', name: 'fname'},
      {data: 'lname', name: 'lname'},
      {data: 'email', name: 'email'},
      {data: 'phone', name: 'phone'},
      {data: 'address', name: 'address'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create vendor
  function createVendor() {
    $('#create-vendor-modal').modal('show');
  }

  let createVendorFormValidator = $('#create-vendor-form').validate({
    rules: {
      fname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      lname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      email: {
        required: true,
        pattern: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z]{2,3}/
      },
      phone: {
        required: true,
        pattern: /^[0-9]{10,10}$/
      },
      address: {
        required: true
      }
    },
    messages: {
      fname: {
        required: "First name is required."
      },
      lname: {
        required: "Last name is required."
      },
      email: {
        required: "Email is required.",
        pattern: "Please enter valid formate in this field."
      },
      phone: {
        required: "Phone number is required.",
        pattern: "Please enter valid phone number in this field."
      },
      address: {
        required: "Address is required."
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element))
    },
    submitHandler: function(form) {
      const payload = {
        fname: document.querySelector('#create-vendor-fname').value,
        lname: document.querySelector('#create-vendor-lname').value,
        email: document.querySelector('#create-vendor-email').value,
        phone: document.querySelector('#create-vendor-phone').value,
        address: document.querySelector('#create-vendor-address').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeVendor(param)
    }
  })

  async function storeVendor(param) {
    const submitButton = document.querySelector('#create-vendor-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateVendorForm()

      let response = await fetch("{{route('vendors.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-vendor-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.fname) document.querySelector('#create-vendor-fname-error').textContent = response.message.fname[0]
          if(response?.message?.lname) document.querySelector('#create-vendor-lname-error').textContent = response.message.lname[0]
          if(response?.message?.email) document.querySelector('#create-vendor-email-error').textContent = response.message.email[0]
          if(response?.message?.phone) document.querySelector('#create-vendor-phone-error').textContent = response.message.phone[0]
          if(response?.message?.address) document.querySelector('#create-vendor-address-error').textContent = response.message.address[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-vendor-modal').on('hidden.bs.modal', event => {
    resetCreateVendorForm();
  })

  function resetCreateVendorForm() {
    const form = document.querySelector('#create-vendor-form')
    form.reset()
    createVendorFormValidator.resetForm()
    document.querySelector('#create-vendor-fname-error').textContent = ""
    document.querySelector('#create-vendor-lname-error').textContent = ""
    document.querySelector('#create-vendor-email-error').textContent = ""
    document.querySelector('#create-vendor-phone-error').textContent = ""
    document.querySelector('#create-vendor-address-error').textContent = ""
  }


  // update vendor
  async function editVendor(vendor) {
    $('#update-vendor-modal').modal('show');

    try {
      let response = await fetch(`{{url('/vendors/edit')}}/${vendor}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-vendor-id').value = response.data.id;
        document.querySelector('#update-vendor-fname').value = response.data.fname;
        document.querySelector('#update-vendor-lname').value = response.data.lname;
        document.querySelector('#update-vendor-email').value = response.data.email;
        document.querySelector('#update-vendor-phone').value = response.data.phone;
        document.querySelector('#update-vendor-address').value = response.data.address;
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateVendorFormValidator = $('#update-vendor-form').validate({
    rules: {
      fname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      lname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      email: {
        required: true,
        pattern: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z]{2,3}/
      },
      phone: {
        required: true,
        pattern: /^[0-9]{10,10}$/
      },
      address: {
        required: true
      }
    },
    messages: {
      fname: {
        required: "First name is required."
      },
      lname: {
        required: "Last name is required."
      },
      email: {
        required: "Email is required.",
        pattern: "Please enter valid formate in this field."
      },      
      phone: {
        required: "Phone number is required.",
        pattern: "Please enter valid phone number in this field."
      },
      address: {
        required: "Address is required."
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element))
    },
    submitHandler: function(form) {
      const vendorID = document.querySelector('#update-vendor-id').value
      const payload = {
        fname: document.querySelector('#update-vendor-fname').value,
        lname: document.querySelector('#update-vendor-lname').value,
        email: document.querySelector('#update-vendor-email').value,
        phone: document.querySelector('#update-vendor-phone').value,
        address: document.querySelector('#update-vendor-address').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateVendor(param, vendorID)
    }
  })

  async function updateVendor(param, vendor) {
    const submitButton = document.querySelector('#update-vendor-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateVendorForm()

      let response = await fetch(`{{url('/vendors/update')}}/${vendor}`, param)
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-vendor-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.fname) document.querySelector('#update-vendor-fname-error').textContent = response.message.fname[0]
          if(response?.message?.lname) document.querySelector('#update-vendor-lname-error').textContent = response.message.lname[0]
          if(response?.message?.email) document.querySelector('#update-vendor-email-error').textContent = response.message.email[0]
          if(response?.message?.phone) document.querySelector('#update-vendor-phone-error').textContent = response.message.phone[0]
          if(response?.message?.address) document.querySelector('#update-vendor-address-error').textContent = response.message.address[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-vendor-modal').on('hidden.bs.modal', event => {
    resetUpdateVendorForm();
  })

  function resetUpdateVendorForm() {
    updateVendorFormValidator.resetForm()
    document.querySelector('#update-vendor-fname-error').textContent = ""
    document.querySelector('#update-vendor-lname-error').textContent = ""
    document.querySelector('#update-vendor-email-error').textContent = ""
    document.querySelector('#update-vendor-phone-error').textContent = ""
    document.querySelector('#update-vendor-address-error').textContent = ""
  }


  // delete vendor
  function deleteVendor(vendor) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/vendors/delete')}}/${vendor}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of vendor
  function changeVendorStatus(vendor) {
    swal({
      title: "Are you sure?",
      text: "You want to change vendor status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/vendors/change-status')}}/${vendor}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush