@extends('layouts.app')
@section('title', 'Amazing Ecom - Change password')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Change Password</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Change Password</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <!-- Horizontal Form -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Change Password</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="change-password-form" action="javascript:void(0)" class="form-horizontal">
              <div class="card-body">
                <div class="row mb-3">
                  <label for="password" class="col-sm-2 col-form-label">password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <span class="error" id="password-error"></span>
                  </div>
                </div>
                <div class="row">
                  <label for="password" class="col-sm-2 col-form-label">Confirm Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="cpassword" id="confirm-password" placeholder="Confirm Password">
                    <span class="error" id="cpassword-error"></span>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <input type="reset" class="btn btn-default mr-2" value="Reset">
                    <button id="change-password-submit-btn" type="submit" class="btn btn-info" style="min-width: 75px;">Update</button>
                  </div>
                </div>
              </div>
              <!-- /.card-footer -->
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('footer-script')
<script>

  let changePasswordFormValidator = $('#change-password-form').validate({
    rules: {
      password: {
        required: true,
        minlength: 6,
        maxlength: 20
      },
      cpassword: {
        required: true,
        equalTo: "#password"
      }
    },
    messages: {
      password: {
        required: "Password is required."
      },
      cpassword: {
        required: "Confirm password is required.",
        equalTo: "Passwod and confirm password must be match."
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element))
    },
    submitHandler: function(form) {
      const payload = {
        password: document.querySelector('#password').value,
        cpassword: document.querySelector('#confirm-password').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      changePassword(param)
    }
  })

  async function changePassword(param) {
    const submitButton = document.querySelector('#change-password-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetForm()

      let response = await fetch(`{{url('/change-password/update')}}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.password) document.querySelector('#password-error').textContent = response.message.password[0]
          if(response?.message?.cpassword) document.querySelector('#cpassword-error').textContent = response.message.cpassword[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  function resetForm() {
    const form = document.querySelector('#change-password-form')
    form.reset()
    changePasswordFormValidator.resetForm()
    document.querySelector('#password-error').textContent = ""
    document.querySelector('#cpassword-error').textContent = ""
  }

</script>
@endpush