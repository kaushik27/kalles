<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Product\AddReviewRequest;
use App\Http\Requests\API\Product\GetProductsRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductReview;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Vendor;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends BaseController
{
    public function getColors() {
        $colors = Color::where('status', Color::ACTIVE_COLOR)->get();

        // get colors prouct count
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->pluck('colors')->toArray();
        if(!empty($colors)) {
            foreach ($colors as $color) {
                $match_products = array_filter($products, function ($product) use($color) {
                    if(empty(array_diff([$color->id], explode(',', $product)))) 
                        return $product;
                });
                $color->product_count = count($match_products);
            }
        }

        if(empty($colors)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $colors);
    }

    public function getSizes() {
        $sizes = Size::where('status', Size::ACTIVE_SIZE)->get();

        // get sizes prouct count
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->pluck('sizes')->toArray();
        if(!empty($sizes)) {
            foreach ($sizes as $size) {
                $match_products = array_filter($products, function ($product) use($size) {
                    if(empty(array_diff([$size->id], explode(',', $product)))) 
                        return $product;
                });
                $size->product_count = count($match_products);
            }
        }

        if(empty($sizes)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $sizes);
    }

    public function getBrands() {
        $brands = Brand::where('status', Brand::ACTIVE_BRAND)->get();

        // get brands prouct count
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->pluck('brand')->toArray();
        if(!empty($brands)) {
            foreach ($brands as $brand) {
                $match_products = array_filter($products, function ($product) use($brand) {
                    if($brand->id == $product) 
                        return $product;
                });
                $brand->product_count = count($match_products);
            }
        }

        if(empty($brands)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $brands);
    }

    public function getCategories() {
        $categories = Category::where('status', Category::ACTIVE_CATEGORY)->get();

        // get categories prouct count
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->pluck('categories')->toArray();
        if(!empty($categories)) {
            foreach ($categories as $category) {
                $match_products = array_filter($products, function ($product) use($category) {
                    if(empty(array_diff([$category->id], explode(',', $product)))) 
                        return $product;
                });
                $category->product_count = count($match_products);
            }
        }

        $categories = collect($categories)->map(function($item) {
            $item['image'] = is_file(public_path('images/category/'.$item['image'])) && file_exists(public_path('images/category/'.$item['image']))
                                ? asset('images/category/'.$item['image'])
                                : asset('images/category/default.png');
            return $item;
        })->all();

        if(empty($categories)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $categories);
    }

    public function getSubCategories($category_id) {
        $subCategories = SubCategory::select('sub_categories.*','c.name as category_name')
                                    ->join('categories as c', 'c.id', 'sub_categories.category_id')
                                    ->where('sub_categories.status', SubCategory::ACTIVE_SUB_CATEGORY)
                                    ->where('sub_categories.category_id', $category_id)
                                    ->get();

        // get sub categories prouct count
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->pluck('sub_categories')->toArray();
        if(!empty($subCategories)) {
            foreach ($subCategories as $category) {
                $match_products = array_filter($products, function ($product) use($category) {
                    if(empty(array_diff([$category->id], explode(',', $product)))) 
                        return $product;
                });
                $category->product_count = count($match_products);
            }
        }

        if(empty($subCategories)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $subCategories);
    }

    public function getProducts(GetProductsRequest $request) {
        $limit = $request->limit ?? 8;

        $sort_by = $request->sort_by ?? Product::SORT_FEATURED;

        $search = $request->search ?? "";

        $min_price = $request->min_price ?? "";
        $max_price = $request->max_price ?? "";
        $filter_stock = $request->filter_stock ?? "";

        $filter_categories = $request->filter_categories ?? [];
        $filter_colors = $request->filter_colors ?? [];
        $filter_sizes = $request->filter_sizes ?? [];
        $filter_brands = $request->filter_brands ?? [];
        $is_filter = !empty($filter_categories) || !empty($filter_colors) || !empty($filter_sizes) || !empty($filter_brands) ? true : false;

        $filtered_products_id = [];
        if($is_filter) {
            $products = Product::where('status', Product::ACTIVE_PRODUCT)
                            ->when($min_price != "", function($query) use($min_price) {
                                $query->where('price', '>=', $min_price);
                            })
                            ->when($max_price != "", function($query) use($max_price) {
                                $query->where('price', '<=', $max_price);
                            })
                            ->when($filter_stock != "", function($query) use($filter_stock) {
                                if($filter_stock)
                                    $query->where('quantity', '>', 0);
                                else
                                    $query->where('quantity', 0);
                            })
                            ->get()
                            ->toArray();
            if(!empty($products)) {
                foreach ($products as $key => $product) {

                    // categories filter
                    if(!empty($filter_categories) && !in_array($product['id'], $filtered_products_id)) {
                        $categories = explode(',', $product['categories']);
                        $unmatch_categories = array_diff($filter_categories, $categories);
                        if(empty($unmatch_categories)) $filtered_products_id[] = $product['id'];
                    }

                    // colors filter
                    if(!empty($filter_colors) && !in_array($product['id'], $filtered_products_id)) {
                        $colors = explode(',', $product['colors']);
                        $unmatch_colors = array_diff($filter_colors, $colors);
                        if(empty($unmatch_colors)) $filtered_products_id[] = $product['id'];
                    }

                    // sizes filter
                    if(!empty($filter_sizes) && !in_array($product['id'], $filtered_products_id)) {
                        $sizes = explode(',', $product['sizes']);
                        $unmatch_sizes = array_diff($filter_sizes, $sizes);
                        if(empty($unmatch_sizes)) $filtered_products_id[] = $product['id'];
                    }

                    // brands filter
                    if(!empty($filter_brands) && !in_array($product['id'], $filtered_products_id)) {
                        if(in_array($product['brand'], $filter_brands)) {
                            $filtered_products_id[] = $product['id'];
                        }
                    }
                }
            }
        }

        // get active product's details (vendor, color, size, brand, category, sub category)
        $all_active_products = $this->getActiveProductDetailsId();

        $products = Product::where('status', Product::ACTIVE_PRODUCT)
                        ->whereIn('id', $all_active_products)
                        ->when($sort_by != "", function($query) use($sort_by) {
                            switch ($sort_by) {
                                // case Product::SORT_BEST_SELLING: break;
                                case Product::SORT_ALPHA_A_TO_Z:
                                    $query->orderBy('name', 'asc');
                                    break;
                                case Product::SORT_ALPHA_Z_TO_A:
                                    $query->orderBy('name', 'desc');
                                    break;
                                case Product::SORT_PRICE_LOW_TO_HIGH:
                                    $query->orderBy('price', 'asc');
                                    break;
                                case Product::SORT_PRICE_HIGH_TO_LOW:
                                    $query->orderBy('price', 'desc');
                                    break;
                                case Product::SORT_DATE_OLD_TO_NEW:
                                    $query->orderBy('created_at', 'asc');
                                    break;
                                case Product::SORT_DATE_NEW_TO_OLD:
                                    $query->orderBy('created_at', 'desc');
                                    break;                                    
                                default:
                                    $query->orderBy('id', 'desc');
                                    break;
                            }
                        })
                        ->when($min_price != "", function($query) use($min_price) {
                            $query->where('price', '>=', $min_price);
                        })
                        ->when($max_price != "", function($query) use($max_price) {
                            $query->where('price', '<=', $max_price);
                        })
                        ->when($filter_stock != "", function($query) use($filter_stock) {
                            if($filter_stock)
                                $query->where('quantity', '>', 0);
                            else
                                $query->where('quantity', 0);
                        })
                        ->when($is_filter, function($query) use($filtered_products_id) {
                            $query->whereIn('id', $filtered_products_id);
                        })
                        ->when($search, function($query) use($search) {
                            $query->where('name', 'like', '%'.$search.'%');
                        })
                        ->paginate($limit);

        if(!empty($products)) {
            foreach($products as $product) {
                $this->prepareSingleProductResponse($product);
            }
        }

        if(empty($products)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $products);
    }

    public function getActiveProductDetailsId() {
        
        $products = Product::where('status', Product::ACTIVE_PRODUCT)->get();

        $active_vendors = Vendor::where('status', Vendor::ACTIVE_VENDOR)->pluck('id')->toArray();
        $active_colors = Color::where('status', Color::ACTIVE_COLOR)->pluck('id')->toArray();
        $active_sizes = Size::where('status', Size::ACTIVE_SIZE)->pluck('id')->toArray();
        $active_brands = Brand::where('status', Brand::ACTIVE_BRAND)->pluck('id')->toArray();
        $active_categories = Category::where('status', Category::ACTIVE_CATEGORY)->pluck('id')->toArray();
        $active_sub_categories = SubCategory::where('status', SubCategory::ACTIVE_SUB_CATEGORY)->pluck('id')->toArray();

        $active_vendor_products = [];
        $active_color_products = [];
        $active_size_products = [];
        $active_brand_products = [];
        $active_category_products = [];
        $active_sub_category_products = [];

        foreach ($products as $key => $product) {
            // active vendor
            if($product->vendor_id) {
                if(in_array($product->vendor_id, $active_vendors)) {
                    $active_vendor_products[] = $product->id;
                }
            }

            // active colors
            if($product->colors) {
                $colors = explode(',', $product->colors);
                if(!empty(array_intersect($colors, $active_colors))) {
                    $active_color_products[] = $product->id;
                }
            }

            // active sizes
            if($product->sizes) {
                $sizes = explode(',', $product->sizes);
                if(!empty(array_intersect($sizes, $active_sizes))) {
                    $active_size_products[] = $product->id;
                }
            }

            // active brand
            if($product->brand) {
                if(in_array($product->brand, $active_brands)) {
                    $active_brand_products[] = $product->id;
                }
            }

            // actvie categories
            if($product->categories) {
                $categories = explode(',', $product->categories);
                if(!empty(array_intersect($categories, $active_categories))) {
                    $active_category_products[] = $product->id;
                }
            }

            // actvie sub categories
            if($product->sub_categories) {
                $sub_categories = explode(',', $product->sub_categories);
                if(!empty(array_intersect($sub_categories, $active_sub_categories))) {
                    $active_sub_category_products[] = $product->id;
                }
            }
        }

        return array_intersect($active_vendor_products, $active_color_products, $active_size_products, $active_brand_products, $active_category_products, $active_sub_category_products);
    }

    public function getSingleProduct($product_id) {
        $product = Product::find($product_id);
        if($product) {
            $this->prepareSingleProductResponse($product);
        }
        return $this->sendResponse("", $product);
    }

    public function prepareSingleProductResponse($product) {
        // wishlist
        if(request()->user('sanctum')) {
            $wishlist = Wishlist::where('user_id', request()->user('sanctum')->id)->where('product_id', $product->id)->first();
            $product->wishlist = $wishlist ? Wishlist::IS_WISHLIST : Wishlist::IS_NOT_WISHLIST;
        } else {
            $product->wishlist = Wishlist::IS_NOT_WISHLIST;
        }

        // categories
        $categories = explode(',', $product->categories);
        if(!empty($categories)) {
            $product->categories = Category::whereIn('id', $categories)->get()->toArray();
        }

        // sub categories
        $sub_categories = explode(',', $product->sub_categories);
        if(!empty($sub_categories)) {
            $product->sub_categories = SubCategory::whereIn('id', $sub_categories)->get()->toArray();
        }

        // colors
        $colors = explode(',', $product->colors);
        if(!empty($colors)) {
            $product->colors = Color::whereIn('id', $colors)->get()->toArray();
        }

        // sizes
        $sizes = explode(',', $product->sizes);
        if(!empty($sizes)) {
            $product->sizes = Size::whereIn('id', $sizes)->get()->toArray();
        }

        // brand
        if(!empty($product->brand)) {
            $product->brand = Brand::find($product->brand)->toArray();
        }

        // tags
        $product->tags = $product->tags ? explode(',', $product->tags) : [];

        // product image
        $images = ProductImage::where('product_id', $product->id)->get();
        $all_images = [];
        if(!empty($images)) {
            foreach ($images as $key => $image) {
                $single_image = [];
                $single_image['image'] = is_file(public_path('images/products/'.$image->image)) && file_exists(public_path('images/products/'.$image->image))
                                ? asset('images/products/'.$image->image)
                                : asset('images/products/default.png');
                $single_image['color'] = Color::find($image->color_id);
                if($single_image['color'] != null)
                    $all_images[] = $single_image;
            }
        }
        $product->images = $all_images;

        // reviews
        $product->review_data  = $this->getReviewsWithRateCount($product->id);
    }

    public function addNRemoveWishlist(Request $request, $product_id) {
        $wishlist = Wishlist::where('user_id', $request->user('sanctum')->id)->where('product_id', $product_id)->first();

        if($wishlist) {
            $wishlist->delete();
            return $this->sendResponse("You have removed product from wishlist successfully!");

        } else {
            Wishlist::create([
                'user_id'      =>   $request->user('sanctum')->id,
                'product_id'   =>   $product_id
            ]);
            return $this->sendResponse("You have added product to wishlist successfully!");
        }
    }

    public function reviews($product_id) {
        return $this->sendResponse("", $this->getReviewsWithRateCount($product_id));
    }

    public function addReview(AddReviewRequest $request, $product_id) {
        
        $review = new ProductReview();
        $review->user_id    =   $request->user('sanctum')->id;
        $review->product_id =   $product_id;
        $review->rate       =   $request->rate;
        $review->title      =   $request->title;
        $review->content    =   $request->content;

        $images = [];
        if($request->images) {
            foreach ($request->images as $key => $image) {
                $imageName = time().rand(1,100).'.'.$image->extension();
                $image->move(public_path('images/product_reviews'), $imageName);
                $images[] = $imageName;
            }
        }
        $review->images = !empty($images) ? implode(',', $images) : NULL;

        $review->save();

        return $this->sendResponse("Review added successfully!", $this->getReviewsWithRateCount($product_id));
    }

    public function getReviewsWithRateCount($product_id) {
        $reviews = ProductReview::select('product_reviews.*', 'fname', 'lname', 'email')
                            ->leftJoin('users', 'users.id', 'product_reviews.user_id')
                            ->where('product_id', $product_id)
                            ->orderBy('id', 'desc')
                            ->get();

        $rate_count = [
            1   =>  0,
            2   =>  0,
            3   =>  0,
            4   =>  0,
            5   =>  0
        ];

        if(!empty($reviews)) {
            foreach ($reviews as $key => $review) {
                $review->created_ago = ago_time($review->created_at);

                $rate_count[$review->rate] = ++$rate_count[$review->rate];
                
                $images = $review->images ? explode(',', $review->images) : [];
                $allImages = [];
                if(!empty($images)) {
                    foreach ($images as $key => $image) {
                        $allImages[] = is_file(public_path('images/product_reviews/'.$image)) && file_exists(public_path('images/product_reviews/'.$image))
                                    ? asset('images/product_reviews/'.$image)
                                    : asset('images/product_reviews/default.png');
                    }
                }
                $review->images = $allImages;
            }
        }
        
        $avg_rate = 0;
        array_walk($rate_count, function($val, $key) use(&$avg_rate) {
            $avg_rate += $key * $val;
        });
        $avg_rate = array_sum($rate_count) > 0 ? round($avg_rate / array_sum($rate_count), 2) : 0;

        return [
            'reviews'       =>  $reviews,
            'rate_count'    =>  $rate_count,
            'avg_rate'      =>  $avg_rate
        ];
    }

    public function wishlist(Request $request) {

        $products = Wishlist::select('p.*')
                        ->join('products as p', 'wishlists.product_id', 'p.id')
                        ->where('wishlists.user_id', $request->user('sanctum')->id)
                        ->where('status', Product::ACTIVE_PRODUCT)
                        ->get()
                        ->each(function($item) use($request){
                            // wishlist
                            $item->wishlist = Wishlist::IS_WISHLIST;

                            // categories
                            $categories = explode(',', $item->categories);
                            if(!empty($categories)) {
                                $item->categories = Category::whereIn('id', $categories)->get()->toArray();
                            }

                            // sub categories
                            $sub_categories = explode(',', $item->sub_categories);
                            if(!empty($sub_categories)) {
                                $item->sub_categories = SubCategory::whereIn('id', $sub_categories)->get()->toArray();
                            }

                            // colors
                            $colors = explode(',', $item->colors);
                            if(!empty($colors)) {
                                $item->colors = Color::whereIn('id', $colors)->get()->toArray();
                            }

                            // sizes
                            $sizes = explode(',', $item->sizes);
                            if(!empty($sizes)) {
                                $item->sizes = Size::whereIn('id', $sizes)->get()->toArray();
                            }

                            // brand
                            if(!empty($item->brand)) {
                                $item->brand = Brand::find($item->brand)->toArray();
                            }
                            
                            // product image
                            $images = ProductImage::where('product_id', $item->id)->get();
                            $all_images = [];
                            if(!empty($images)) {
                                foreach ($images as $key => $image) {
                                    $single_image = [];
                                    $single_image['image'] = is_file(public_path('images/products/'.$image->image)) && file_exists(public_path('images/products/'.$image->image))
                                                        ? asset('images/products/'.$image->image)
                                                        : asset('images/products/default.png');
                                    $single_image['color'] = Color::find($image->color_id);
                                    if($single_image['color'] != null)
                                        $all_images[] = $single_image;
                                }
                            }
                            $item->images = $all_images;
                        });

        return $this->sendResponse("", $products);
    }

    public function getStock() {
        $data['in_stock_products'] = Product::where('status', Product::ACTIVE_PRODUCT)->where('quantity', '!=', 0)->count();
        $data['out_of_stock_products'] = Product::where('status', Product::ACTIVE_PRODUCT)->where('quantity', 0)->count();
        return $this->sendResponse("", $data);
    }
}
