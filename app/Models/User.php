<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ADMIN_ROLE = 1;
    const USER_ROLE = 2;

    const EMAIL_NOT_VARIFIED = 0;
    const EMAIL_VARIFIED = 1;

    const ACTIVE_USER = 1;
    const INACTIVE_USER = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fname',
        'lname',
        'email',
        'password',
        'email_verified',
        'role',
        'token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // get user shipping price
    public static function shippingPrice() {
        $address = UserAddress::leftJoin('cities', 'cities.id', 'user_addresses.city')
                            ->where('user_addresses.user_id', auth()->user('sanctum')->id)
                            ->first();
        return $address ? $address->shipping_price : 0;
    }
}
