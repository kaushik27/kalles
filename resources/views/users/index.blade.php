@extends('layouts.app')
@section('title', 'Amazing Ecom - Customers')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Customers</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Customers</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of customers.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('users.export')}}" class="btn btn-info mr-2" title="Export Customers"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create User" onclick="createUser()"><i class="fas fa-plus"></i> Create Customer</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create User Modal -->
<div class="modal fade" id="create-user-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-user-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Customer</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-user-fname">First Name</label>
              <input type="text" class="form-control" name="fname" id="create-user-fname" placeholder="First Name">
              <span class="error" id="create-user-fname-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-user-lname">Last Name</label>
              <input type="text" class="form-control" name="lname" id="create-user-lname" placeholder="Last Name">
              <span class="error" id="create-user-lname-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-user-email">Email</label>
              <input type="text" class="form-control" name="email" id="create-user-email" placeholder="Email">
              <span class="error" id="create-user-email-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-user-password">Password</label>
              <input type="password" class="form-control" name="password" id="create-user-password" placeholder="Password">
              <span class="error" id="create-user-password-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="create-user-phone">Phone Number</label>
              <input type="text" class="form-control" name="phone" id="create-user-phone" placeholder="Phone Number">
              <span class="error" id="create-user-phone-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-user-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update User Modal -->
<div class="modal fade" id="update-user-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-user-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Customer</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <input type="hidden" class="form-control" name="id" id="update-user-id" placeholder="User ID">
              <label for="update-user-fname">First Name</label>
              <input type="text" class="form-control" name="fname" id="update-user-fname" placeholder="First Name">
              <span class="error" id="update-user-fname-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-user-lname">Last Name</label>
              <input type="text" class="form-control" name="lname" id="update-user-lname" placeholder="Last Name">
              <span class="error" id="update-user-lname-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-user-email">Email</label>
              <input type="text" class="form-control" name="email" id="update-user-email" placeholder="Email" readonly>
              <span class="error" id="update-user-email-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-user-phone">Phone Number</label>
              <input type="text" class="form-control" name="phone" id="update-user-phone" placeholder="Phone Number">
              <span class="error" id="update-user-phone-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-user-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Change User Address Modal -->
<div class="modal fade" id="change-user-address-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="change-user-address-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Change Customer Address</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="change-user-address-address1">Address 1</label>
              <input type="hidden" class="form-control" name="id" id="change-user-address-user-id" placeholder="ID">
              <input type="text" class="form-control" name="address1" id="change-user-address-address1" placeholder="Address 1">
              <span class="error" id="change-user-address-address1-error"></span>
            </div>
            <div class="col-md-6">
              <label for="change-user-address-address2">Address 2</label>
              <input type="text" class="form-control" name="address2" id="change-user-address-address2" placeholder="Address 2">
              <span class="error" id="change-user-address-address2-error"></span>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="change-user-address-state">State</label>
              <select name="state" id="change-user-address-state" class="form-control select2" onchange="getCities(event)">
                <option value="">Select State</option>
                @foreach ($states as $state)
                    <option value="{{$state->id}}">{{$state->state}}</option>
                @endforeach
              </select>
              <span class="error" id="change-user-address-state-error"></span>
            </div>
            <div class="col-md-6">
              <label for="change-user-address-city">City</label>
              <select name="city" id="change-user-address-city" class="form-control select2">
                <option value="">Select City</option>
              </select>
              <span class="error" id="change-user-address-city-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="change-user-address-pincode">pincode</label>
              <input type="text" class="form-control" name="pincode" id="change-user-address-pincode" placeholder="Pincode">
              <span class="error" id="change-user-address-pincode-error"></span>
            </div>
            <div class="col-mb-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="change-user-address-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#user-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("users.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'fname', name: 'fname'},
      {data: 'lname', name: 'lname'},
      {data: 'email', name: 'email'},
      {data: 'phone', name: 'phone'},
      {data: 'address', name: 'address'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create user
  function createUser() {
    $('#create-user-modal').modal('show');
  }

  let createUserFormValidator = $('#create-user-form').validate({
    rules: {
      fname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      lname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      email: {
        required: true,
        pattern: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z]{2,3}/
      },
      password: {
        required: true,
        minlength: 6,
        maxlength: 20
      },
      phone: {
        required: true,
        pattern: /^[0-9]{10,10}$/
      }
    },
    messages: {
      fname: {
        required: "First name is required."
      },
      lname: {
        required: "Last name is required."
      },
      email: {
        required: "Email is required.",
        pattern: "Please enter valid formate in this field."
      },
      password: {
        required: "Password is required."
      },
      phone: {
        required: "Phone number is required.",
        pattern: "Please enter valid phone number in this field."
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element))
    },
    submitHandler: function(form) {
      const payload = {
        fname: document.querySelector('#create-user-fname').value,
        lname: document.querySelector('#create-user-lname').value,
        email: document.querySelector('#create-user-email').value,
        password: document.querySelector('#create-user-password').value,
        phone: document.querySelector('#create-user-phone').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeUser(param)
    }
  })

  async function storeUser(param) {
    const submitButton = document.querySelector('#create-user-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateUserForm()

      let response = await fetch("{{route('users.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-user-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.fname) document.querySelector('#create-user-fname-error').textContent = response.message.fname[0]
          if(response?.message?.lname) document.querySelector('#create-user-lname-error').textContent = response.message.lname[0]
          if(response?.message?.email) document.querySelector('#create-user-email-error').textContent = response.message.email[0]
          if(response?.message?.password) document.querySelector('#create-user-password-error').textContent = response.message.password[0]
          if(response?.message?.phone) document.querySelector('#create-user-phone-error').textContent = response.message.phone[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-user-modal').on('hidden.bs.modal', event => {
    resetCreateUserForm();
  })

  function resetCreateUserForm() {
    const form = document.querySelector('#create-user-form')
    form.reset()
    createUserFormValidator.resetForm()
    document.querySelector('#create-user-fname-error').textContent = ""
    document.querySelector('#create-user-lname-error').textContent = ""
    document.querySelector('#create-user-email-error').textContent = ""
    document.querySelector('#create-user-password-error').textContent = ""
    document.querySelector('#create-user-phone-error').textContent = ""
  }
  

  // update user
  async function editUser(user) {
    $('#update-user-modal').modal('show');

    try {
      let response = await fetch(`{{url('/users/edit')}}/${user}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-user-id').value = response.data.id;
        document.querySelector('#update-user-fname').value = response.data.fname;
        document.querySelector('#update-user-lname').value = response.data.lname;
        document.querySelector('#update-user-email').value = response.data.email;
        document.querySelector('#update-user-phone').value = response.data.phone;
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateUserFormValidator = $('#update-user-form').validate({
    rules: {
      fname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      lname: {
        required: true,
        lettersonly: true,
        minlength: 2,
        maxlength: 20
      },
      email: {
        required: true,
        pattern: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z]{2,3}/
      },
      phone: {
        required: true,
        pattern: /^[0-9]{10,10}$/
      }
    },
    messages: {
      fname: {
        required: "First name is required."
      },
      lname: {
        required: "Last name is required."
      },
      email: {
        required: "Email is required.",
        pattern: "Please enter valid formate in this field."
      },      
      phone: {
        required: "Phone number is required.",
        pattern: "Please enter valid phone number in this field."
      }
    },
    errorPlacement: function(error, element) {
      error.insertAfter($(element))
    },
    submitHandler: function(form) {
      const userID = document.querySelector('#update-user-id').value
      const payload = {
        fname: document.querySelector('#update-user-fname').value,
        lname: document.querySelector('#update-user-lname').value,
        email: document.querySelector('#update-user-email').value,
        phone: document.querySelector('#update-user-phone').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateUser(param, userID)
    }
  })

  async function updateUser(param, user) {
    const submitButton = document.querySelector('#update-user-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateUserForm()

      let response = await fetch(`{{url('/users/update')}}/${user}`, param)
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-user-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.fname) document.querySelector('#update-user-fname-error').textContent = response.message.fname[0]
          if(response?.message?.lname) document.querySelector('#update-user-lname-error').textContent = response.message.lname[0]
          if(response?.message?.email) document.querySelector('#update-user-email-error').textContent = response.message.email[0]
          if(response?.message?.phone) document.querySelector('#update-user-phone-error').textContent = response.message.phone[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-user-modal').on('hidden.bs.modal', event => {
    resetUpdateUserForm();
  })

  function resetUpdateUserForm() {
    updateUserFormValidator.resetForm()
    document.querySelector('#update-user-fname-error').textContent = ""
    document.querySelector('#update-user-lname-error').textContent = ""
    document.querySelector('#update-user-email-error').textContent = ""
    document.querySelector('#update-user-phone-error').textContent = ""
  }


  // change address 
  async function changeAddress(user) {
    $('#change-user-address-modal').modal('show');

    try {
      let response = await fetch(`{{url('/users/get-user-address')}}/${user}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#change-user-address-user-id').value = user;
        if(response.data) {
          document.querySelector('#change-user-address-address1').value = response.data.address1;
          document.querySelector('#change-user-address-address2').value = response.data.address2;
          document.querySelector('#change-user-address-pincode').value = response.data.pincode;

          const state = document.querySelector('#change-user-address-state')
          state.value = response.data.state
          state.dispatchEvent(new Event("change"));
          await getCitiesByState(response.data.state)

          const city = document.querySelector('#change-user-address-city')
          city.value = response.data.city
          city.dispatchEvent(new Event("change"));
        }
      }

    } catch (err) {
      console.error(err)
    }
  }

  let changeUserAddressFormValidator = $('#change-user-address-form').validate({
    rules: {
      address1: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      address2: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      city: {
        required: true  
      },
      state: {
        required: true
      },
      pincode: {
        required: true 
      }
    },
    messages: {
      address1: {
        required: "Address 1 is required."
      },
      address2: {
        required: "Address 2 is required.",
      },
      city: {
        required: "City is required."
      },
      state: {
        required: "State is required."
      },
      pincode: {
        required: "Pincode is required."
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "state") {
        error.appendTo('#change-user-address-state-error')
      } else if(element.attr('name') == "city") {
        error.appendTo('#change-user-address-city-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const userId = document.querySelector('#change-user-address-user-id').value
      const payload = {
        address1: document.querySelector('#change-user-address-address1').value,
        address2: document.querySelector('#change-user-address-address2').value,
        city: document.querySelector('#change-user-address-city').value,
        state: document.querySelector('#change-user-address-state').value,
        pincode: document.querySelector('#change-user-address-pincode').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      changeUserAddress(param, userId)
    }
  })

  async function changeUserAddress(param, user) {
    const submitButton = document.querySelector('#change-user-address-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetChangeUserAddressForm()

      let response = await fetch(`{{url('/users/update-address')}}/${user}`, param)
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#change-user-address-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.address1) document.querySelector('#change-user-address-address1-error').textContent = response.message.address1[0]
          if(response?.message?.address2) document.querySelector('#change-user-address-address2-error').textContent = response.message.address2[0]
          if(response?.message?.city) document.querySelector('#change-user-address-city-error').textContent = response.message.city[0]
          if(response?.message?.state) document.querySelector('#change-user-address-state-error').textContent = response.message.state[0]
          if(response?.message?.pincode) document.querySelector('#change-user-address-pincode-error').textContent = response.message.pincode[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#change-user-address-modal').on('hidden.bs.modal', event => {
    resetChangeUserAddressForm();
  })

  function resetChangeUserAddressForm() {
    changeUserAddressFormValidator.resetForm()
    document.querySelector('#change-user-address-address1-error').textContent = ""
    document.querySelector('#change-user-address-address2-error').textContent = ""
    document.querySelector('#change-user-address-state-error').textContent = ""
    document.querySelector('#change-user-address-city-error').textContent = ""
    document.querySelector('#change-user-address-pincode-error').textContent = ""
  }


  // delete user
  function deleteUser(user) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/users/delete')}}/${user}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // get city
  async function getCities(event) {
    let cityInput = document.querySelector(`#change-user-address-city`)
    const state = event.target.value;
    if(!state) return cityInput.innerHTML = `<option value="">Select City</option>`
    await getCitiesByState(state)
  }

  async function getCitiesByState(state) {
    let cityInput = document.querySelector(`#change-user-address-city`)

    try {
      let response = await fetch(`{{url('/users/get-cities')}}/${state}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        const cities = response.data
        let options = `<option value="">Select City</option>`
        cities.forEach(city => options += `<option value="${city.id}">${city.city}</option>`);
        cityInput.innerHTML = options
      }

    } catch (err) {
      console.error(err)
    }
  }

  // update status of user
  function changeUserStatus(user) {
    swal({
      title: "Are you sure?",
      text: "You want to change user status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/users/change-status')}}/${user}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush