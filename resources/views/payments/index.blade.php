@extends('layouts.app')
@section('title', 'Amazing Ecom - Payments')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Payments</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Payments</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of payments.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('payments.export')}}" class="btn btn-info" title="Export Products"><i class="fas fa-download"></i> Export</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="payment-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Order ID</th>
                  <th>Payment ID</th>
                  <th>Customer Name</th>
                  <th>Amount</th>
                  <th>Payment Type</th>
                  <th>Date</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('footer-script')
<script>
  let table = $('#payment-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("payments.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'razorpay_order_id', name: 'razorpay_order_id'},
      {data: 'razorpay_payment_id', name: 'razorpay_payment_id'},
      {data: 'user_name', name: 'user_name'},
      {data: 'final_amount', name: 'final_amount'},
      {data: 'type', name: 'type'},
      {data: 'date', name: 'date'}
    ]
  })
</script>
@endpush