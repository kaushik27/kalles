<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Pyament\RazorpayRequest;
use App\Http\Requests\API\Pyament\RazorpayVerifyRequest;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

class PaymentController extends BaseController
{
    public function razorpay(RazorpayRequest $request) {

        $address = UserAddress::where('user_addresses.user_id', $request->user('sanctum')->id)->first();
        if(!$address) return $this->sendError("User address not exist, please enter first user address.");

        $order = Order::find($request->order_id);
        if($order) {

            try {
                $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));

                // Create Razorpay order
                $orderData  = $api->order->create([
                    'receipt'       => $order->id,
                    'amount'        => $order->final_amount * 100,
                    'currency'      => 'INR'
                ]);

                // update razorpay order in order table
                $order->razorpay_order_id = $orderData['id'];
                $order->save();

                $data = [
                    'key'           => env('RAZORPAY_KEY'),
                    'amount'        => $order->final_amount * 100,
                    'order_id'      => $orderData['id']
                ];
        
                return $this->sendResponse("", $data);

            } catch (\Throwable $th) {
                return $this->sendError("Payment Failed!", [], 400);
            }

        } else {
            return $this->sendError("Order not found, Pleaes enter valid order_id.", [], 400);
        }

    }

    public function verify(RazorpayVerifyRequest $request) {

        try {
            
            $success = true;
            if (empty($request->razorpay_payment_id) === false) {
                $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
                try {
                    $attributes = [
                        'razorpay_order_id'     => $request->razorpay_order_id,
                        'razorpay_payment_id'   => $request->razorpay_payment_id,
                        'razorpay_signature'    => $request->razorpay_signature
                    ];
                    $api->utility->verifyPaymentSignature($attributes);
                } catch (SignatureVerificationError $e) {
                    $success = false;
                }
            }

            if ($success === true) {

                $order = Order::where('razorpay_order_id', $request->razorpay_order_id)->first();
                if($order && $order->status == Order::ACTIVE_ORDER) {
                    $payment = Payment::find($order->payment_id);
                    if($payment) {
                        $payment->razorpay_order_id = $request->razorpay_order_id;
                        $payment->razorpay_payment_id = $request->razorpay_payment_id;
                        $payment->razorpay_signature = $request->razorpay_signature;
                        $payment->status = Payment::ACTIVE_PAYMENT;
                        if($payment->save()) {
                            $order->delete();
                        }
                    }
                }

                return $this->sendResponse("Payment has been successfull");
            } else {
                return $this->sendError("Payment Failed!", [], 400);
            }

        } catch (\Throwable $th) {
            return $this->sendError("Payment Failed!", [], 400);
        }

    }
}
