@extends('layouts.app')
@section('title', 'Amazing Ecom - Sub Categories')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Sub Categories</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Sub Categories</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of sub categories.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('sub_categories.export')}}" class="btn btn-info mr-2" title="Export Sub Categories"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Sub Category" onclick="createSubCategory()"><i class="fas fa-plus"></i> Create Sub Category</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="sub-category-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Sub Category Name</th>
                  <th>Category Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Sub Category Modal -->
<div class="modal fade" id="create-sub-category-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-sub-category-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Cub Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-sub-category-name">Sub Category Name</label>
              <input type="text" class="form-control" name="name" id="create-sub-category-name" placeholder="Sub Category Name">
              <span class="error" id="create-sub-category-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-sub-category-name">Category Name</label>
              <select name="category" id="create-sub-category-category" class="form-control select2">
                <option value="">Select Category</option>
                @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
              <span class="error" id="create-sub-category-category-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="create-sub-category-status">Status</label>
              <select name="status" id="create-sub-category-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-sub-category-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-sub-category-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Sub Category Modal -->
<div class="modal fade" id="update-sub-category-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-sub-category-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Sub Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-sub-category-name">Sub Category Name</label>
              <input type="hidden" class="form-control" name="id" id="update-sub-category-id" placeholder="Sub Category ID">
              <input type="text" class="form-control" name="sub_category" id="update-sub-category-name" placeholder="Sub Category Name">
              <span class="error" id="update-sub-category-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-sub-category-name">Category Name</label>
              <select name="category" id="update-sub-category-category" class="form-control select2">
                <option value="">Select Category</option>
                @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
              <span class="error" id="update-sub-category-category-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="update-sub-category-status">Status</label>
              <select name="status" id="update-sub-category-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-sub-category-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-sub-category-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#sub-category-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("sub_categories.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'name', name: 'name'},
      {data: 'category_name', name: 'category_name'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })

  // create Sub Category
  function createSubCategory() {
    $('#create-sub-category-modal').modal('show');
  }

  let createSubCategoryFormValidator = $('#create-sub-category-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      category: {
        required: true
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Sub category name is required.",
      },
      category: {
        required: "Category name is required.",
      },
      status: {
        required: "Status is required."
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "category") {
        error.appendTo('#create-sub-category-category-error')
      } else if(element.attr('name') == "status") {
        error.appendTo('#create-sub-category-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        name: document.querySelector('#create-sub-category-name').value,
        category: document.querySelector('#create-sub-category-category').value,
        status: document.querySelector('#create-sub-category-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeSubCategory(param)
    }
  })

  async function storeSubCategory(param) {
    const submitButton = document.querySelector('#create-sub-category-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateSubCategoryForm()

      let response = await fetch("{{route('sub_categories.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-sub-category-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#create-sub-category-name-error').textContent = response.message.name[0]
          if(response?.message?.category) document.querySelector('#create-sub-category-category-error').textContent = response.message.category[0]
          if(response?.message?.status) document.querySelector('#create-sub-category-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-sub-category-modal').on('hidden.bs.modal', event => {
    resetCreateSubCategoryForm();
  })

  function resetCreateSubCategoryForm() {
    const form = document.querySelector('#create-sub-category-form')
    form.reset()
    createSubCategoryFormValidator.resetForm()

    const category = document.querySelector('#create-sub-category-category')
    category.value = ""
    category.dispatchEvent(new Event("change"));

    const status = document.querySelector('#create-sub-category-status')
    status.value = 1
    status.dispatchEvent(new Event("change"));

    document.querySelector('#create-sub-category-name-error').textContent = ""
    document.querySelector('#create-sub-category-category-error').textContent = ""
    document.querySelector('#create-sub-category-status-error').textContent = ""
  }
  

  // update sub category
  async function editSubCategory(subCategory) {
    $('#update-sub-category-modal').modal('show');

    try {
      let response = await fetch(`{{url('/sub-categories/edit')}}/${subCategory}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-sub-category-id').value = response.data.id;
        document.querySelector('#update-sub-category-name').value = response.data.name;

        const category = document.querySelector('#update-sub-category-category')
        category.value = response.data.category_id
        category.dispatchEvent(new Event("change"));

        const status = document.querySelector('#update-sub-category-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateSubCategoryFormValidator = $('#update-sub-category-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      category: {
        required: true
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Sub category name is required.",
      },
      category: {
        required: "Category name is required.",
      },
      status: {
        required: "Status is required."
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "category") {
        error.appendTo('#update-sub-category-category-error')
      } else if(element.attr('name') == "status") {
        error.appendTo('#update-sub-category-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const subCategoryID = document.querySelector('#update-sub-category-id').value
      const payload = {
        name: document.querySelector('#update-sub-category-name').value,
        category: document.querySelector('#update-sub-category-category').value,
        status: document.querySelector('#update-sub-category-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateSubCategory(param, subCategoryID)
    }
  })

  async function updateSubCategory(param, subCategory) {
    const submitButton = document.querySelector('#update-sub-category-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateSubCategoryForm()

      let response = await fetch(`{{url('/sub-categories/update')}}/${subCategory}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-sub-category-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#update-sub-category-name-error').textContent = response.message.name[0]
          if(response?.message?.category) document.querySelector('#update-sub-category-category-error').textContent = response.message.category[0]
          if(response?.message?.status) document.querySelector('#update-sub-category-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-sub-category-modal').on('hidden.bs.modal', event => {
    resetUpdateSubCategoryForm();
  })

  function resetUpdateSubCategoryForm() {
    updateSubCategoryFormValidator.resetForm()

    const category = document.querySelector('#update-sub-category-category')
    category.value = ""
    category.dispatchEvent(new Event("change"));

    const status = document.querySelector('#update-sub-category-status')
    status.value = 1
    status.dispatchEvent(new Event("change"));

    document.querySelector('#update-sub-category-name-error').textContent = ""
    document.querySelector('#update-sub-category-category-error').textContent = ""
    document.querySelector('#update-sub-category-status-error').textContent = ""
  }


  // delete sub category
  function deleteSubCategory(subCategory) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/sub-categories/delete')}}/${subCategory}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of sub category
  function changeSubCategoryStatus(subCategory) {
    swal({
      title: "Are you sure?",
      text: "You want to change sub category status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/sub-categories/change-status')}}/${subCategory}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush