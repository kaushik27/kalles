@extends('layouts.app')
@section('title', 'Amazing Ecom - User Report')

@push('head-script')
<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
@endpush

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User Report</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">User Report</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card card-info collapsed-card mb-4">
            <div class="card-header" data-card-widget="collapse" style="cursor: pointer">
              <h3 class="card-title">User Filter</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool">
                  <i class="fas fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <form method="post">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="date">Date</label>
                      <input type="text" class="form-control" name="date" id="date" autocomplete="off" />
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="user">Select User</label>
                      <select name="usre" id="user" class="form-control select2">
                        <option value="">Select User</option>
                        @foreach ($users as $user)
                          <option value="{{$user->id}}">{{ucwords($user->fname . " " . $user->lname)}}</option>  
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <label>&nbsp;</label>
                    <div>
                      <button type="button" class="btn btn-info mr-2" onclick="applyFilter()">Filter</button>
                      <button type="button" class="btn btn-default" onclick="resetFilter()">Reset</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-6">
                  <h3 class="card-title">User wise sale report</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('reports.user_export')}}" class="btn btn-info" title="Export User Report"><i class="fas fa-download"></i> Export</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user-list" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Total Sale</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('footer-script')
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script>

  loadUser()
  
  function loadUser() {
    
    const date = document.querySelector('#date')?.value
    const user = document.querySelector('#user')?.value
    
    let payload = {
      date,
      user,
      _token: "{{csrf_token()}}"
    };

    let table = $('#user-list').DataTable({
      processing: true,
      serverSide: true,
      destroy: true,
      ajax: {
        type: "POST",
        url: "{{route('reports.getUserData')}}",
        data: payload
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'fname', name: 'fname'},
        {data: 'lname', name: 'lname'},
        {data: 'email', name: 'email'},
        {data: 'phone', name: 'phone'},
        {data: 'city', name: 'city'},
        {data: 'state', name: 'state'},
        {data: 'sale', name: 'sale'}
      ]
    })
  }

  function applyFilter() {
    loadUser()
  }

  function resetFilter() {
    date.value = ""
    $('#user').val("").trigger('change')
    loadUser()
  }

  $('#date').daterangepicker({
    locale: {
      cancelLabel: 'Clear',
      format: 'DD-MM-YYYY'
    }
  });

  $('#date').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#date').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
  });
</script>
@endpush