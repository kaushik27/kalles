<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Payments\PaymentsExport;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PaymentController extends Controller
{
    public function index() {
        return view('payments.index');
    }
    
    public function getData() {
        $payments = Order::select('orders.*', 'p.type', 'p.razorpay_payment_id', 'p.created_at as payment_date')
                        ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                        ->whereNotNull('orders.payment_id')
                        ->withTrashed()
                        ->get();
        
        return DataTables::of($payments)
            ->addIndexColumn()
            ->editColumn('date', function($payment){
                return date('d-m-Y H:i A', strtotime($payment->payment_date));
            })
            ->addColumn('user_name', function($payment){
                $user = User::find($payment->user_id);
                if(!$user) return "";
                return ucwords($user->fname. " " .$user->lname);
            })
            ->rawColumns(['user_name'])
            ->make(true);
    }

    public function export() {
        return Excel::download(new PaymentsExport, 'payments.xlsx');
    }
}
