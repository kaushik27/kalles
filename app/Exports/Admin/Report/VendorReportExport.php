<?php

namespace App\Exports\Admin\Report;

use App\Models\AddToCart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Vendor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VendorReportExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $vendors = Vendor::get();

        $all_vendors = [];
        foreach ($vendors as $key => $vendor) {
            $vendor_product_ids = Product::where('vendor_id', $vendor->id)->pluck('id')->toArray();
            $completd_order_ids = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                    ->where('orders.status', Order::INACTIVE_ORDER)
                                    ->whereNotNull('orders.payment_id')
                                    ->withTrashed()
                                    ->pluck('orders.id')
                                    ->toArray();
            $vendor->total_sale = AddToCart::whereIn('order_id', $completd_order_ids)
                                    ->whereIn('product_id', $vendor_product_ids)
                                    ->sum('total_amount');
            
            $single_vendor = [];
            $single_vendor['id'] = $vendor->id;
            $single_vendor['fname'] = $vendor->fname;
            $single_vendor['lname'] = $vendor->lname;
            $single_vendor['email'] = $vendor->email;
            $single_vendor['phone'] = $vendor->phone;
            $single_vendor['total_sale'] = $vendor->total_sale ?? 0;
            $all_vendors[] = $single_vendor;
        }

        return collect($all_vendors);
    }

    public function headings(): array
    {
        return ["ID", "First Name", "Last Name", "Email", "Phone", "Total Sale"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
