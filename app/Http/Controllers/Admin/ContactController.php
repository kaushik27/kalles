<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Contact\ContactsExport;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ContactController extends Controller
{
    public function index() {
        return view('contacts.index');
    }

    public function getData() {
        $contacts = Contact::all();

        return DataTables::of($contacts)
            ->addIndexColumn()
            ->editColumn('name', function($contact){
                return ucfirst($contact->name);
            })
            ->addColumn('action', function($contact){
                return '
                    <button class="btn btn-danger" title="Delete Contact" onclick="deleteContact('.$contact->id.')"><i class="fas fa-trash"></i></button>
                    ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function delete(Contact $contact) {
        if($contact->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Contact deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new ContactsExport, 'contacts.xlsx');
    }
}
