<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->enum('type', ['Online', 'COD'])->after('id');
            $table->string('razorpay_order_id')->nullable()->change();
            $table->string('razorpay_payment_id')->nullable()->change();
            $table->string('razorpay_signature')->nullable()->change();
            $table->integer('status')->default(0)->after('razorpay_signature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
