<div class="row">
  <div class="col-md-12">
    <table id="review-list" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Sr. No.</th>
          <th>User name</th>
          <th>Rate</th>
          <th>Title</th>
          <th>Content</th>
          <th>Reviews Images</th>
          <th>Create At</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($reviews as $key => $review)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{ucwords($review->fname. ' ' .$review->lname)}}</td>
          <td>{{$review->rate}}</td>
          <td>{{$review->title}}</td>
          <td>{{$review->content}}</td>
          <td>
            @php
              $images = [];
              if($review->images != "") {
                $images = explode(',', $review->images);
              }
            @endphp
            @foreach($images as $image)
              <img src="{{asset('images/product_reviews/'.$image)}}" alt="Review Image" height="50" class="mb-1">
            @endforeach
          </td>
          <td>{{date('d-m-Y H:i A', strtotime($review->created_at))}}</td>
          <td>
            <button class="btn btn-danger" title="Delete Review" onclick="deleteReview({{$product->id}}, {{$review->id}})"><i class="fas fa-trash"></i></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>