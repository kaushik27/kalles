@extends('layouts.app')
@section('title', 'Amazing Ecom - Categories')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Categories</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Categories</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of categories.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('categories.export')}}" class="btn btn-info mr-2" title="Export Categories"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Category" onclick="createCategory()"><i class="fas fa-plus"></i> Create Category</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="category-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Category Name</th>
                  <th>Category Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Category Modal -->
<div class="modal fade" id="create-category-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-category-form" action="javascript:void(0)" enctype="multipart/form-data">
        <div class="modal-header">
          <h4 class="modal-title">Create Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="create-category-name">Category Name</label>
              <input type="text" class="form-control" name="name" id="create-category-name" placeholder="category Name">
              <span class="error" id="create-category-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-category-image">Category Image</label>
              <input type="file" class="form-control" name="image" id="create-category-image" style="padding: 3px">
              <span class="error" id="create-category-image-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="create-category-status">Status</label>
              <select name="status" id="create-category-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-category-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-category-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Category Modal -->
<div class="modal fade" id="update-category-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-category-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row mb-3">
            <div class="col-md-6">
              <label for="update-category-name">Category Name</label>
              <input type="hidden" class="form-control" name="id" id="update-category-id" placeholder="category ID">
              <input type="text" class="form-control" name="name" id="update-category-name" placeholder="category Name">
              <span class="error" id="update-category-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-category-image">Category Image</label>
              <input type="file" class="form-control" name="image" id="update-category-image" style="padding: 3px">
              <span class="error" id="update-category-image-error"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="update-category-status">Status</label>
              <select name="status" id="update-category-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-category-status-error"></span>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-category-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#category-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("categories.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'name', name: 'name'},
      {data: 'image', name: 'image'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create category
  function createCategory() {
    $('#create-category-modal').modal('show');
  }

  let createCategoryFormValidator = $('#create-category-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      image: {
        required: true,
        extension: "jpg,jpeg,png"
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Category name is required.",
      },
      image: {
        required: "Category image is required.",
        extension: "Please enter valid file with extension: jpg, jpeg, png"
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#create-category-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const formData = new FormData()
      formData.append('name', document.querySelector('#create-category-name').value)
      formData.append('image', document.querySelector('#create-category-image').files[0] ?? "")
      formData.append('status', document.querySelector('#create-category-status').value)
      formData.append('_token', "{{csrf_token()}}")

      const param = prepareFetchParamWithImage(formData)
      storeCategory(param)
    }
  })

  async function storeCategory(param) {
    const submitButton = document.querySelector('#create-category-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateCategoryForm()

      let response = await fetch("{{route('categories.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-category-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#create-category-name-error').textContent = response.message.name[0]
          if(response?.message?.image) document.querySelector('#create-category-image-error').textContent = response.message.image[0]
          if(response?.message?.status) document.querySelector('#create-category-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-category-modal').on('hidden.bs.modal', event => {
    resetCreateCategoryForm();
  })

  function resetCreateCategoryForm() {
    const form = document.querySelector('#create-category-form')
    form.reset()
    createCategoryFormValidator.resetForm()
    document.querySelector('#create-category-name-error').textContent = ""
    document.querySelector('#create-category-image-error').textContent = ""
    document.querySelector('#create-category-status-error').textContent = ""
  }
  

  // update category
  async function editCategory(category) {
    $('#update-category-modal').modal('show');

    try {
      let response = await fetch(`{{url('/categories/edit')}}/${category}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      console.log(response)
      if(response.status) {
        document.querySelector('#update-category-id').value = response.data.id;
        document.querySelector('#update-category-name').value = response.data.name;
        const status = document.querySelector('#update-category-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateCategoryFormValidator = $('#update-category-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Category name is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#update-category-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const categoryID = document.querySelector('#update-category-id').value
      const formData = new FormData()
      formData.append('name', document.querySelector('#update-category-name').value)
      formData.append('image', document.querySelector('#update-category-image').files[0] ?? "")
      formData.append('status', document.querySelector('#update-category-status').value)
      formData.append('_token', "{{csrf_token()}}")

      const param = prepareFetchParamWithImage(formData)
      updateCategory(param, categoryID)
    }
  })

  async function updateCategory(param, category) {
    const submitButton = document.querySelector('#update-category-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateCategoryForm()

      let response = await fetch(`{{url('/categories/update')}}/${category}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-category-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#update-category-name-error').textContent = response.message.name[0]
          if(response?.message?.image) document.querySelector('#update-category-image-error').textContent = response.message.image[0]
          if(response?.message?.status) document.querySelector('#update-category-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-category-modal').on('hidden.bs.modal', event => {
    resetUpdateCategoryForm();
  })

  function resetUpdateCategoryForm() {
    updateCategoryFormValidator.resetForm()
    document.querySelector('#update-category-name-error').textContent = ""
    document.querySelector('#update-category-image-error').textContent = ""
    document.querySelector('#update-category-status-error').textContent = ""
  }


  // delete category
  function deleteCategory(category) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/categories/delete')}}/${category}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of category
  function changeCategoryStatus(category) {
    swal({
      title: "Are you sure?",
      text: "You want to change category status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/categories/change-status')}}/${category}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush