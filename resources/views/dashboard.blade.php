@extends('layouts.app')
@section('title', 'Amazing Ecom - Dashboard')

@push('head-script')
<style>
  .small-box h3, .small-box p {
    color: white
  }

  .small-box1 h3, .small-box1 p {
    color: black
  }
</style>
@endpush

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{route('dashboard')}}">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #4f4e92">
              <div class="inner">
                <h3>{{$user_count}}</h3>

                <p>Total Customers</p>
              </div>
              <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <a href="{{route('users.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #ec5b47">
              <div class="inner">
                <h3>{{$vendor_count}}</h3>

                <p>Total Vendors</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-friends"></i>
              </div>
              <a href="{{route('vendors.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #328bb4">
              <div class="inner">
                <h3>{{$product_count}}</h3>

                <p>Total Products</p>
              </div>
              <div class="icon">
                <i class="fas fa-luggage-cart"></i>
              </div>
              <a href="{{route('products.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #e6456e">
              <div class="inner">
                <h3>{{$categories_count}}</h3>

                <p>Total Categories</p>
              </div>
              <div class="icon">
                <i class="fas fa-list"></i>
              </div>
              <a href="{{route('categories.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #319397">
              <div class="inner">
                <h3>{{$completed_order_count}}</h3>

                <p>Total Completed Orders</p>
              </div>
              <div class="icon">
                <i class="fas fa-check"></i>
              </div>
              <a href="{{route('orders.completed')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #464646">
              <div class="inner">
                <h3>{{$pending_order_count}}</h3>

                <p>Total Pending Orders</p>
              </div>
              <div class="icon">
                <i class="fas fa-box"></i>
              </div>
              <a href="{{route('orders.pending')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box small-box1" style="background-color: #f9ba28">
              <div class="inner">
                <h3>{{$state_count}}</h3>

                <p>Total States</p>
              </div>
              <div class="icon">
                <i class="fas fa-globe-americas"></i>
              </div>
              <a href="{{route('states.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #2a5383">
              <div class="inner">
                <h3>₹{{$payment_count}}</h3>

                <p>Total Sale</p>
              </div>
              <div class="icon">
                <i class="fas fa-rupee-sign"></i>
              </div>
              <a href="{{route('payments.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-md-8">
            <!-- BAR CHART -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Orders Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="mb-3">
                  <div class="row">
                    <div class="col-sm-4 offset-sm-4">
                      <select name="order_duration" id="order-duration" class="form-control select2" onchange="orderChart()">
                        <option value="week" selected>Last 7 Days</option>
                        <option value="last3month">Last 3 Months</option>
                        <option value="last6month">Last 6 Months</option>
                        <option value="last12month">Last 12 Months</option>
                        <option value="thisYear">This Year</option>
                        <option value="lastYear">Last Year</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="chart">
                  <canvas id="order-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <!-- PIE CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">State Wise User Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="state-wise-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-8">
            <!-- LINE CHART -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Registered User Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="mb-3">
                  <div class="row">
                    <div class="col-sm-4 offset-sm-4">
                      <select name="registered_user_duration" id="registered-user-duration" class="form-control select2" onchange="registeredUserChart()">
                        <option value="week" selected>Last 7 Days</option>
                        <option value="last3month">Last 3 Months</option>
                        <option value="last6month">Last 6 Months</option>
                        <option value="last12month">Last 12 Months</option>
                        <option value="thisYear">This Year</option>
                        <option value="lastYear">Last Year</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="chart">
                  <canvas id="registered-user-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('footer-script')
<!-- ChartJS -->
<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
<script>

  $(function () {
    /* ChartJS
      * -------
      * Here we will create a few charts using ChartJS
      */

    // line chart
    registeredUserChart()

    // pie chart
    stateWiseUserChart()

    // bar chart
    orderChart()

  })

  async function stateWiseUserChart() {
    try {
      let response = await fetch("{{route('dashboard.state_wise_user_chart')}}")

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        const {states: labels, users: usersCount} = response.data

        const data = {
          labels,
          datasets: [
            {
              data: usersCount,
              backgroundColor : await getColorCode(usersCount.length),
            }
          ]
        }

        const pieOption = {
          maintainAspectRatio : false,
          responsive : true,
        }

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        const pieChartCanvas = $('#state-wise-chart').get(0).getContext('2d')
        const pieData        = data
        const pieOptions     = pieOption
        // You can switch between pie and douhnut using the method below.
        new Chart(pieChartCanvas, {
          type: 'pie',
          data: pieData,
          options: pieOptions
        })
      }

    } catch (err) {
      console.error(err)
    }
  }

  async function getColorCode(length) {
    const colors = ["#ec6850", "#33a657", "#ec9b00", "#42c1f1", "#4a8ebe", "#d3d6de", "#3368a6", "#5f33a6", "#a63397", "#a6334e", "#7da633", "#a67833", "#a63333", "#b7dac1", "#dad5b7", "#8c77ae", "#77a0ae", "#a36041", "#151922", "#b600ff", "#ff0092", "#00ff93", "#6ea580", "#6ea5a0", "#458a78", "#247660"]
    return colors.slice(0, length)
  }

  async function registeredUserChart() {
    try {
      const duration = document.querySelector('#registered-user-duration').value
      let response = await fetch(`{{url('/dashboard/registered-user-chart')}}/${duration}`)

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {

        const chartData = response.data

        const data = {
          labels  : Object.keys(chartData),
          datasets: [
            {
              label               : 'Registerd User',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius         : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : Object.values(chartData)
            }
          ]
        }

        const lineOption = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        //-------------
        //- LINE CHART -
        //--------------
        const lineChartCanvas = $('#registered-user-chart').get(0).getContext('2d')
        const lineChartOptions = $.extend(true, {}, lineOption)
        const lineChartData = $.extend(true, {}, data)
        lineChartData.datasets[0].fill = false;
        lineChartOptions.datasetFill = false

        const lineChart = new Chart(lineChartCanvas, {
          type: 'line',
          data: lineChartData,
          options: lineChartOptions
        })

      }

    } catch (err) {
      console.error(err)
    }
  }

  async function orderChart() {
    try {
      const duration = document.querySelector('#order-duration').value
      let response = await fetch(`{{url('/dashboard/order-chart')}}/${duration}`)

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {

        const chartData = response.data

        const data = {
          labels  : Object.keys(chartData),
          datasets: [
            {
              label               : 'Order',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius         : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : Object.values(chartData)
            }
          ]
        }

        const lineOption = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        //-------------
        //- BAR CHART -
        //-------------
        var barChartCanvas = $('#order-chart').get(0).getContext('2d')
        var barChartData = $.extend(true, {}, data)
        var temp0 = data.datasets[0]
        barChartData.datasets[0] = temp0

        var barChartOptions = {
          responsive              : true,
          maintainAspectRatio     : false,
          datasetFill             : false
        }

        new Chart(barChartCanvas, {
          type: 'bar',
          data: barChartData,
          options: barChartOptions
        })

      }

    } catch (err) {
      console.error(err)
    }
  }

</script>
@endpush