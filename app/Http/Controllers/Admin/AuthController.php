<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\ChangePasswordRequest;
use App\Http\Requests\Admin\Auth\LoginRequest;
use App\Models\User;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class AuthController extends Controller
{
    /**
     * Load login form
     *
     * @return  View
     */
    public function login() {
        return view('auth.login');
    }

    /**
     * Handle login attempt
     *
     * @param  Request  $request
     */
    public function attempt(LoginRequest $request) {

        $user = User::where('email', $request->email)->first();

        if(!$user) {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Email not exist in our database."
            ]);
        }

        if(!$user->email_verified) {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Login failed! Email must be verified before login."
            ]);
        }

        if($user->role != User::ADMIN_ROLE) {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "You have no access to login."
            ]);
        }

        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => User::ADMIN_ROLE])) {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Your password is wrong, Please enter currect password."
            ]);
        }

        return response()->json([
            'status'    =>  1,
            'message'   =>  "Logged in."
        ]);
    }

    /**
     * Load forgot password form
     *
     * @return View
     */
    public function forgotPassword() {
        return view('auth.forgot-password');
    }

    /**
     * Load reset password form
     *
     * @return View
     */
    public function resetPassword($token) {

        $user = User::where('token', $token)->first();
        
        if(!$user) return abort(404);

        return view('auth.reset-password');
    }

    /**
     * verify email address
     * 
     * @param  string  $token
     *
     * @return View
     */
    public function emailVerify($token) {
        $user = User::where('token', $token)->first();
        
        if(!$user) return abort(404);

        $user->token = "";
        $user->email_verified = User::EMAIL_VARIFIED;
        $user->email_verified_at = date('Y-m-d H:i:s');

        if(!$user->save()) return abort(404);

        return view('auth.email-verify');
    }

    /**
     * Load change password form
     *
     * @return View
     */
    public function changePassword() {
        return view('auth.change-password');
    }

    /**
     * update password
     *
     * @param  Request  $request
     */
    public function updatePassword(ChangePasswordRequest $request) {
        $user = User::find(Auth::user()->id);
        if(!$user) {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }

        $user->password = Hash::make($request->password);
        if($user->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Password updated successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    /**
     * Logout Admin
     *
     * @param  Request  $request
     */
    public function logout(Request $request) {
        Auth::logout();
 
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect()->route('login');
    }
}
