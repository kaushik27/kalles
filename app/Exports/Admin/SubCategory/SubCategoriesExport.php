<?php

namespace App\Exports\Admin\SubCategory;

use App\Models\SubCategory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SubCategoriesExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $sub_categories = SubCategory::select('sub_categories.*', 'categories.name as category_name')->leftJoin('categories', 'categories.id', 'sub_categories.category_id')->get();
        $all_sub_categories = [];
        foreach ($sub_categories as $key => $sub_category) {
            $single_sub_category = [];
            $single_sub_category['id'] = $sub_category->id;
            $single_sub_category['sub_category_name'] = $sub_category->name;
            $single_sub_category['category_name'] = $sub_category->category_name;
            $single_sub_category['status'] = $sub_category->status;
            $all_sub_categories[] = $single_sub_category;
        }

        return collect($all_sub_categories);
    }

    public function headings(): array
    {
        return ["ID", "Sub Category Name", "Category Name", "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
