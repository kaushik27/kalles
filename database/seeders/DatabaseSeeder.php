<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use App\Models\City;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\State;
use App\Models\SubCategory;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // create admin user
        User::create([
            'fname'             =>  'admin',
            'lname'             =>  'admin',
            'email'             =>  'admin@yopmail.com',
            'password'          =>  Hash::make(123456),
            'email_verified'    =>  1,
            'role'              =>  user::ADMIN_ROLE
        ]);

        // state
        $state = new State();
        $state->state = "Gujarat";
        $state->status = 1;
        $state->Save();

        $state = new State();
        $state->state = "Panjab";
        $state->status = 1;
        $state->Save();

        // city
        $city = new City();
        $city->state_id = 1;
        $city->city = "Surat";
        $city->shipping_price = 100;
        $city->estimation_time = 45000;
        $city->status = 1;
        $city->Save();

        $city = new City();
        $city->state_id = 1;
        $city->city = "Ahmedabad";
        $city->shipping_price = 150;
        $city->estimation_time = 1296000;
        $city->status = 1;
        $city->Save();


        // category 
        $category = new Category();
        $category->name = "Men";
        $category->image = "test";
        $category->save();
        
        $category = new Category();
        $category->name = "Women";
        $category->image = "test";
        $category->save();

        $category = new SubCategory();
        $category->category_id = 1;
        $category->name = "Shoes";
        $category->save();

        $category = new SubCategory();
        $category->category_id = 1;
        $category->name = "Organic";
        $category->save();

        $category = new SubCategory();
        $category->category_id = 2;
        $category->name = "Shoes";
        $category->save();

        $category = new Product();
        $category->name = "Test";
        $category->price = 500;
        $category->quantity = 100;
        $category->summary = "This is the summary";
        $category->categories = "1,2";
        $category->sub_categories = "1,2,3";
        $category->colors = "1";
        $category->sizes = "1";
        $category->brands = "1";
        $category->tags = "tag1,tag2";
        $category->description = "This is the description";
        $category->save();

        $category = new Product();
        $category->name = "shoes";
        $category->price = 600;
        $category->quantity = 200;
        $category->summary = "This is the summary";
        $category->categories = "1,2";
        $category->sub_categories = "1,2,3";
        $category->colors = "1";
        $category->sizes = "1";
        $category->brands = "1";
        $category->tags = "tag1,tag2";
        $category->description = "This is the description";
        $category->save();
        $category = new Product();
        $category->name = "Watch";
        $category->price = 1000;
        $category->quantity = 500;
        $category->summary = "This is the summary";
        $category->categories = "1,2";
        $category->sub_categories = "1,3";
        $category->colors = "1";
        $category->sizes = "1";
        $category->brands = "1";
        $category->tags = "tag1,tag2";
        $category->description = "This is the description";
        $category->save();


        $color = new Color();
        $color->name = "red";
        $color->color = "#ff0000";
        $color->save();
        $color = new Color();
        $color->name = "blue";
        $color->color = "#00ff00";
        $color->save();
        $color = new Brand();
        $color->name = "apple";
        $color->save();
        $color = new Brand();
        $color->name = "Jack & Jeans";
        $color->save();
        $color = new Size();
        $color->size = "M";
        $color->save();
        $color = new Size();
        $color->size = "L";
        $color->save();

    }
}
