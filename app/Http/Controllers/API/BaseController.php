<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BaseController extends Controller
{
    /**
     * success response method
     *
     * @return Response
     */
    public function sendResponse($message = "", $result = [])
    {
    	$response = ['status'   =>  1];

        if(!empty($message)) $response['message'] = $message;

        if(!empty($result)) $response['data'] = $result;

        return response()->json($response, 200);
    }

    /**
     * return error response
     *
     * @return Response
     */
    public function sendError($error = "", $errorMessages = [], $code = 404)
    {
    	$response = ['status'  =>  0];

        if(!empty($error)) $response['message'] = $error;

        if(!empty($errorMessages)) $response['data'] = $errorMessages;

        return response()->json($response, $code);
    }
}
