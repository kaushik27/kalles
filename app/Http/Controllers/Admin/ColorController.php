<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Color\ColorsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Color\CreateColorRequest;
use App\Http\Requests\Admin\Color\UpdateColorRequest;
use App\Models\Color;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ColorController extends Controller
{
    public function index()
    {
        return view('colors.index');
    }

    public function getData() {
        $colors = Color::all();
        
        return DataTables::of($colors)
            ->addIndexColumn()
            ->editColumn('color', function($color){
                return '<span class="badge mr-1" style="background-color: '.$color->color.'">&nbsp;&nbsp;</span>'.$color->color;
            })
            ->editColumn('status', function($color){
                return $color->status 
                    ? '<span class="badge badge-success" onclick="changeColorStatus('.$color->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeColorStatus('.$color->id.')">Inactive</span>';
            })
            ->addColumn('action', function($color){
                return '
                    <button class="btn btn-info mr-2" title="Edit Color" onclick="editColor('.$color->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-danger" title="Delete Color" onclick="deleteColor('.$color->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['color', 'status', 'action'])
            ->make(true);
    }

    public function create(CreateColorRequest $request) {
        $color = new Color();
        $color->name = $request->name;
        $color->color = $request->code;
        $color->status = $request->status;
        if($color->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Color created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Color $color) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $color
        ]);
    }

    public function update(UpdateColorRequest $request, Color $color) {
        $color->name = $request->name;
        $color->color = $request->code;
        $color->status = $request->status;
        if($color->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Color updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Color $color) {

        $products = Product::all();
        foreach ($products as $key => $product) {
            $colors = explode(',', $product->colors);
            if(in_array($color->id, $colors)) {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  "Products already found with this color, First You need to remove this color's products ."
                ]);
            }
        }

        if($color->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Color deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Color $color) {
        $color->status = $color->status == Color::ACTIVE_COLOR ? Color::INACTIVE_COLOR : Color::ACTIVE_COLOR;
        if($color->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Color status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new ColorsExport, 'colors.xlsx');
    }
}
