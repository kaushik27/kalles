<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\State\StatesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\State\CreateStateRequest;
use App\Http\Requests\Admin\State\UpdateStateRequest;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class StateController extends Controller
{
    public function index()
    {
        return view('states.index');
    }

    public function getData() {
        $states = State::all();
        
        return DataTables::of($states)
            ->addIndexColumn()
            ->editColumn('status', function($state){
                return $state->status 
                    ? '<span class="badge badge-success" onclick="changeStateStatus('.$state->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeStateStatus('.$state->id.')">Inactive</span>';
            })
            ->addColumn('action', function($state){
                return '
                    <button class="btn btn-info mr-2" title="Edit state" onclick="editState('.$state->id.')"><i class="fas fa-edit"></i></button>
                ';
                // <button class="btn btn-danger" title="Delete state" onclick="deleteState('.$state->id.')"><i class="fas fa-trash"></i></button>
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create(CreateStateRequest $request) {
        $state = new State();
        $state->state = $request->state;
        $state->status = $request->status;
        if($state->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "State created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(State $state) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $state
        ]);
    }

    public function update(UpdateStateRequest $request, State $state) {
        $state->state = $request->state;
        $state->status = $request->status;
        if($state->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "State updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(State $state) {
        if($state->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'State deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(State $state) {
        $state->status = $state->status == State::ACTIVE_STATE ? State::INACTIVE_STATE : State::ACTIVE_STATE;
        if($state->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'State status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new StatesExport, 'states.xlsx');
    }
}
