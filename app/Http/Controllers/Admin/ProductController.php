<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Product\ProductsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\CreateProductRequest;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductReview;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Vendor;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as FacadesFile;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    public function index() {
        $vendors = Vendor::where('status', Vendor::ACTIVE_VENDOR)->get();
        $categories = Category::where('status', Category::ACTIVE_CATEGORY)->get();
        $sub_categories = SubCategory::where('status', SubCategory::ACTIVE_SUB_CATEGORY)->get();
        $colors = Color::where('status', Color::ACTIVE_COLOR)->get();
        $brands = Brand::where('status', Brand::ACTIVE_BRAND)->get();
        $sizes = Size::where('status', Size::ACTIVE_SIZE)->get();
        return view('products.index', compact('vendors' ,'categories', 'sub_categories', 'colors', 'brands', 'sizes'));
    }

    public function getData() {
        $products = Product::all();

        return DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('vendor', function($product){
                $vendor = Vendor::find($product->vendor_id);
                return $vendor ? ucwords($vendor->fname . ' ' . $vendor->lname) : "";
            })
            ->editColumn('categories', function($product){
                $all_categories = [];
                if($product->categories) {
                    $product_categories = Category::whereIn('id', explode(',', $product->categories))->get();
                    foreach ($product_categories as $key => $category) {
                        $all_categories[] = $category->name;
                    }
                }
                return implode(', ', $all_categories) ?? "-";
            })
            ->editColumn('sub_categories', function($product){
                $all_sub_categories = [];
                if($product->sub_categories) {
                    $product_sub_categories = SubCategory::whereIn('id', explode(',', $product->sub_categories))->get();
                    foreach ($product_sub_categories as $key => $sub_category) {
                        $all_sub_categories[] = $sub_category->name;
                    }
                }
                return implode(', ', $all_sub_categories) ?? "-";
            })
            ->editColumn('colors', function($product){
                $all_colors = [];
                if($product->colors) {
                    $product_colors = Color::whereIn('id', explode(',', $product->colors))->get();
                    foreach ($product_colors as $key => $color) {
                        $all_colors[] = $color->name;
                    }
                }
                return implode(', ', $all_colors) ?? "-";
            })
            ->editColumn('brand', function($product){
                $product_brand = Brand::find($product->brand);
                return $product_brand ? $product_brand->name : "-";
            })
            ->editColumn('sizes', function($product){
                $all_sizes = [];
                if($product->sizes) {
                    $product_sizes = Size::whereIn('id', explode(',', $product->sizes))->get();
                    foreach ($product_sizes as $key => $size) {
                        $all_sizes[] = $size->size;
                    }
                }
                return implode(', ', $all_sizes) ?? "-";
            })
            ->editColumn('tags', function($product){
                $all_tags = [];
                if($product->tags) 
                    $all_tags = explode(',', $product->tags);

                return implode(', ', $all_tags) ?? "-";
            })
            ->editColumn('status', function($product){
                return $product->status 
                    ? '<span class="badge badge-success" onclick="changeProductStatus('.$product->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeProductStatus('.$product->id.')">Inactive</span>';
            })
            ->addColumn('action', function($product){

                $product_images = ProductImage::where('product_id', $product->id)->get();
                $all_images = "";
                $all_image_colors = "";
                foreach ($product_images as $key => $image) {
                    $file_path = is_file(public_path('images/products/'.$image->image)) && file_exists(public_path('images/products/'.$image->image))
                                        ? asset('images/products/'.$image->image)
                                        : "";
                                        
                    if($file_path) {
                        $file_size = FacadesFile::size(public_path('images/products/'.$image->image));
                        $all_images .= '<div data-name="'.$image->image.'" data-size="'.$file_size.'" data-path="'.$file_path.'" data-color="'.$image->color_id.'"></div>';
                    }
                }
                return '
                    <button class="btn btn-info mr-2" title="Edit product" onclick="editProduct('.$product->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-success mr-2" title="Upload Images" onclick="uploadProducts('.$product->id.')"><i class="fas fa-upload"></i></button>
                    <template id="old-images-'.$product->id.'">
                        '.$all_images.'
                    </template>
                    <button class="btn btn-warning mr-2" title="Rates & Reviews" onclick="viewRateReview('.$product->id.')"><i class="fas fa-star"></i></button>
                    <button class="btn btn-danger" title="Delete product" onclick="deleteProduct('.$product->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['categories', 'sub_categories', 'colors', 'brand', 'sizes', 'tags', 'status', 'action'])
            ->make(true);
    }

    public function create(CreateProductRequest $request) {
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->vendor_id = $request->vendor;
        $product->categories = $request->categories;
        $product->sub_categories = $request->sub_categories;
        $product->colors = $request->colors;
        $product->brand = $request->brand;
        $product->sizes = $request->sizes;
        $product->tags = $request->tags;
        $product->summary = $request->summary;
        $product->description = $request->description;
        $product->status = Product::INACTIVE_PRODUCT;
        if($product->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Product created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Product $product) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $product
        ]);
    }

    public function update(UpdateProductRequest $request, Product $product) {
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->categories = $request->categories;
        $product->sub_categories = $request->sub_categories;
        $product->colors = $request->colors;
        $product->brand = $request->brand;
        $product->sizes = $request->sizes;
        $product->tags = $request->tags;
        $product->summary = $request->summary;
        $product->description = $request->description;
        if($product->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Product updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Product $product) {        
        if($product->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Product deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Product $product) {
        $product->status = $product->status == Product::ACTIVE_PRODUCT ? Product::INACTIVE_PRODUCT : Product::ACTIVE_PRODUCT;
        if($product->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Product status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function getSubCategories(Request $request) {
        $categories = explode(',', $request->categories);
        $sub_categories = SubCategory::whereIn('category_id', $categories)->get();

        return response()->json([
            'status'    =>  1,
            'data'      =>  $sub_categories
        ]);
    }

    public function upload(Request $request) {
        if($request->product_id) {

            $product = Product::find($request->product_id);
            if($product) {
                $product_image = new ProductImage();
                $product_image->product_id = $product->id;
                // update product image
                if($request->file) {
                    $image_name = time().rand(1,100).'.'.$request->file->extension();
                    $request->file->move(public_path('images/products/'), $image_name);
                    $product_image->image = $image_name;
                }
                if($product_image->save()) {
                    return response()->json([
                        'status'    =>  1
                    ]);
                }
            }

        }
    }

    public function removeUplaod(Request $request) {
        if($request->product_id) {

            $product = Product::find($request->product_id);
            if($product) {
                $product_images = ProductImage::where('product_id', $product->id)->get();

                if(!empty($product_images)) {
                    foreach ($product_images as $key => $image) {
                        if($key == $request->file_id) {
                            @unlink(public_path('images/products/'.$image->image));
                            $image->delete();

                            return response()->json([
                                'status'    =>  1
                            ]);
                        }
                    }
                }
            }

        }
    }
    
    public function changeImageColor(Request $request) {
        if($request->product_id) {

            $product = Product::find($request->product_id);
            if($product) {
                $product_images = ProductImage::where('product_id', $product->id)->get();

                if(!empty($product_images)) {
                    foreach ($product_images as $key => $image) {
                        if($key == $request->file_id) {
                            $image->color_id = $request->color;
                            $image->save();

                            return response()->json([
                                'status'    =>  1
                            ]);
                        }
                    }
                }
            }

        }
    }

    public function getRateReview($product) {
        $product = Product::find($product);

        $reviews = [];
        if($product) {
            $reviews = ProductReview::select('product_reviews.*', 'u.fname', 'u.lname')
                            ->leftJoin('users as u', 'u.id', 'product_reviews.user_id')
                            ->where('product_reviews.product_id', $product->id)
                            ->get();
        }

        return view('products.review-modal', compact('reviews', 'product'));
    }

    public function deleteRateReview(ProductReview $review) {
        if($review->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Product review deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }
}
