$('.select2').select2()

function prepareFetchParam(payload) {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  };
}

function prepareFetchParamWithImage(formData) {
  return {
    method: 'POST',
    body: formData
  };
}