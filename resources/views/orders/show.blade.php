<div class="row">
  <div class="col-md-12 mb-3">
    <h5 class="mb-3">Order Details</h5>
    <div class="row mb-3">
      <div class="col-lg-3">
          <strong>Customer Name</strong>
      </div>
      <div class="col-lg-9">
          {{ucwords($order->fname.' '.$order->lname)}}
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-3">
          <strong>Razorpay Order ID</strong>
      </div>
      <div class="col-lg-9">
          {{$order->razorpay_order_id ?? "-"}}
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-3">
          <strong>Amount</strong>
      </div>
      <div class="col-lg-9">
          {{$order->final_amount}}
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-3">
          <strong>Razorpay Payment ID</strong>
      </div>
      <div class="col-lg-9">
          {{$order->razorpay_payment_id ?? "-"}}
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-3">
          <strong>Payment Date</strong>
      </div>
      <div class="col-lg-9">
        {{$order->payment_date ? date('d-m-Y', strtotime($order->payment_date)) : "-"}}
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3">
          <strong>Customer Note</strong>
      </div>
      <div class="col-lg-9">
        {{$order->note ?? "-"}}
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <hr>
    <h5>Cart Items</h5>
    <div class="mb-3 text-right">
      @if($type == "pending")
        <button class="btn btn-success" onclick="changeStatus({{$order->id}})">Complete Order</button>
      @endif
    </div>
    <table id="product-list" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Sr. No.</th>
          <th>Product Image</th>
          <th>Product Name</th>
          <th>Price</th>
          <th>Quanity</th>
          <th>Colors</th>
          <th>Sizes</th>
          <th>Amount</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        @foreach($items as $key => $item)
        <tr>
          <td>{{$key+1}}</td>
          <td>
            @foreach ($item['product_details']['images'] as $key => $image)
              @if($key == 0)
                <img src="{{$image}}" width="50px">
              @endif
            @endforeach
          </td>
          <td>{{$item['product_details']['name']}}</td>
          <td>{{$item['price']}}</td>
          <td>{{$item['quantity']}}</td>
          <td>{{$item['color_details']['name']}}</td>
          <td>{{$item['size_details']['size']}}</td>
          <td>{{$item['total_amount']}}</td>
          <td>{{date('d-m-Y H:i A', strtotime($item['created_at']))}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>