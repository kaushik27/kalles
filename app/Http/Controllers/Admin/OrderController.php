<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Orders\CompletedOrdersExport;
use App\Exports\Admin\Orders\PendingOrdersExport;
use App\Http\Controllers\Controller;
use App\Models\AddToCart;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function index() {
        return view('orders.pending');
    }

    public function completed() {
        return view('orders.completed');
    }

    public function getPendingData() {
        $orders = Order::select('orders.*', 'u.fname', 'u.lname', 'p.type', 'p.created_at as payment_date')
                    ->leftJoin('users as u', 'u.id', 'orders.user_id')
                    ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                    ->where('orders.status', Order::ACTIVE_ORDER)
                    ->whereNotNull('orders.payment_id')
                    ->withTrashed()
                    ->get();
        
        return DataTables::of($orders)
            ->addIndexColumn()
            ->addColumn('user_name', function($order){
                return ucwords($order->fname. " " .$order->lname);
            })
            ->editColumn('type', function($order){
                return $order->payment_date ? date('d-m-Y H:i A', strtotime($order->payment_date)) : "-";
            })
            ->editColumn('payment_date', function($order){
                return $order->payment_date ? date('d-m-Y H:i A', strtotime($order->payment_date)) : "-";
            })
            ->editColumn('note', function($order){
                return $order->note ?? "-";
            })
            ->addColumn('action', function($order){
                return '<button class="btn btn-success mr-2" title="Complete Order" onclick="viewOrder('.$order->id.', \'pending\')"><i class="fas fa-eye"></i></button>';
            })
            ->rawColumns(['user_name', 'payment_status', 'payment_date', 'action'])
            ->make(true);
    }

    public function getCompletedData() {
        $orders = Order::select('orders.*', 'u.fname', 'u.lname', 'p.type', 'p.created_at as payment_date')
                    ->leftJoin('users as u', 'u.id', 'orders.user_id')
                    ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                    ->where('orders.status', Order::INACTIVE_ORDER)
                    ->withTrashed()
                    ->get();
        
        return DataTables::of($orders)
            ->addIndexColumn()
            ->addColumn('user_name', function($order){
                return ucwords($order->fname. " " .$order->lname);
            })
            ->editColumn('payment_date', function($order){
                return date('d-m-Y H:i A', strtotime($order->payment_date));
            })
            ->editColumn('note', function($order){
                return $order->note ?? "-";
            })
            ->addColumn('action', function($order){
                return '<button class="btn btn-success mr-2" title="Complete Order" onclick="viewOrder('.$order->id.', \'completed\')"><i class="fas fa-eye"></i></button>';
            })
            ->rawColumns(['user_name', 'payment_date', 'payment_status', 'action'])
            ->make(true);
    }

    public function changeStatus($order) {
        $order = Order::where('id', $order)->withTrashed()->first();
        $order->status = $order->status == Order::ACTIVE_ORDER ? Order::INACTIVE_ORDER : Order::ACTIVE_ORDER;
        if($order->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Order status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function show($order, $type) {
        $order = Order::select('orders.*', 'u.fname', 'u.lname', 'p.razorpay_payment_id', 'p.created_at as payment_date')
                    ->leftJoin('users as u', 'u.id', 'orders.user_id')
                    ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                    ->where('orders.id', $order)
                    ->withTrashed()
                    ->first();

        $cart_items = [];
        if($order) {
            $columns = [
                'add_to_cart.*',
                'p.name as product_name', 'p.status as product_status',
                'c.name as color', 'c.color as color_code', 'c.status as color_status',
                's.size as size', 's.status as size_status',
            ];
            $cart = AddToCart::select(...$columns)
                            ->leftJoin('products as p', 'p.id', 'add_to_cart.product_id')
                            ->leftJoin('colors as c', 'c.id', 'add_to_cart.color_id')
                            ->leftJoin('sizes as s', 's.id', 'add_to_cart.size_id')
                            ->where('order_id', $order->id)
                            ->get();
    
            if(!empty($cart)) {
                foreach ($cart as $key => $item) {

                    // product image
                    $images = ProductImage::where('product_id', $item->product_id)->get();
                    $all_images = [];
                    if(!empty($images)) {
                        foreach ($images as $key => $image) {
                            $all_images[] = is_file(public_path('images/products/'.$image->image)) && file_exists(public_path('images/products/'.$image->image))
                                            ? asset('images/products/'.$image->image)
                                            : asset('images/products/default.png');
                        }
                    }

                    $cart_items[] = [
                        'id'            =>  $item->id,
                        'order_id'      =>  $item->order_id,
                        'price'         =>  $item->price,
                        'quantity'      =>  $item->quantity,
                        'total_amount'  =>  $item->total_amount,
                        'created_at'    =>  date('d-m-Y', strtotime($item->created_at)),
                        'updated_at'    =>  date('d-m-Y', strtotime($item->updated_at)),
                        'product_details'   =>  [
                            'id'        =>  $item->product_id,
                            'name'      =>  $item->product_name,
                            'images'    =>  $all_images,
                            'status'    =>  $item->product_status
                        ],
                        'color_details'     =>  [
                            'name'      =>  $item->color,
                            'color'     =>  $item->color_code,
                            'status'    =>  $item->color_status
                        ],
                        'size_details'      =>  [
                            'size'      =>  $item->size,
                            'status'    =>  $item->size_status
                        ]
                    ];
                }
            }
        }
        $items = $cart_items;
        return view('orders.show', compact('order', 'items', 'type'));
    }

    public function pendingExport() {
        return Excel::download(new PendingOrdersExport, 'pending-orders-export.xlsx');
    }

    public function completedExport() {
        return Excel::download(new CompletedOrdersExport, 'completed-orders-export.xlsx');
    }
}
