<footer class="main-footer">
  <strong>Copyright &copy; {{date('Y')}} <a href="{{route('dashboard')}}">Amazing Ecom</a>.</strong>
  All rights reserved.
</footer>