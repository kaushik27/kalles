<?php

namespace App\Exports\Admin\User;

use App\Models\City;
use App\Models\State;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::select('users.*', 'ua.address1', 'ua.address2', 'ua.city', 'ua.state')
                    ->leftJoin('user_addresses as ua', 'ua.user_id', 'users.id')
                    ->where('role', User::USER_ROLE)
                    ->get();
                    
        $all_users = [];
        foreach ($users as $key => $user) {
            $single_user = [];
            $single_user['id'] = $user->id;
            $single_user['firstname'] = ucfirst($user->fname);
            $single_user['lastname'] = ucfirst($user->lname);
            $single_user['email'] = $user->email;
            $single_user['phone'] = $user->phone;

            // address
            $city = $user->city ? City::find($user->city)->city : "";
            $state = $user->state ? State::find($user->state)->state : "";
            $address = "";
            $address .= $user->address1 ? $user->address1.", " : "";
            $address .= $user->address2 ? $user->address2.", <br>" : "";
            $address .= $city ? $city.", " : "";
            $address .= $state ? $state.". " : "";
            $single_user['address'] = $address ? $address : "-";

            $single_user['status'] = $user->status;
            $all_users[] = $single_user;
        }

        return collect($all_users);
    }

    public function headings(): array
    {
        return ["ID", "Frist Name", "Last Name", "Email", "Phone", "Address", "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
