<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\ColorController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\SizeController;
use App\Http\Controllers\Admin\StateController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VendorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/attempt', [AuthController::class, 'attempt'])->name('attempt');
Route::get('/forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot_password');
Route::get('/email-verify/{token}', [AuthController::class, 'emailVerify'])->name('email_verify');
Route::get('/reset-password/{token}', [AuthController::class, 'resetPassword'])->name('reset_password');

Route::middleware(['auth'])->group(function () {

  // logout
  Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

  // change password
  Route::get('/change-password', [AuthController::class, 'changePassword'])->name('change_password');
  Route::post('/change-password/update', [AuthController::class, 'updatePassword'])->name('update_password');

  // Dashboard
  Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
  Route::get('/dashboard/state-wise-user-chart', [DashboardController::class, 'state_wise_user_chart'])->name('dashboard.state_wise_user_chart');
  Route::get('/dashboard/registered-user-chart/{duration}', [DashboardController::class, 'registered_user_chart'])->name('dashboard.registered_user_chart');
  Route::get('/dashboard/order-chart/{duration}', [DashboardController::class, 'order_chart'])->name('dashboard.order_chart');

  // Colors
  Route::get('/colors', [ColorController::class, 'index'])->name('colors.index');
  Route::get('/colors/data', [ColorController::class, 'getData'])->name('colors.data');
  Route::post('/colors/create', [ColorController::class, 'create'])->name('colors.create');
  Route::get('/colors/edit/{color}', [ColorController::class, 'edit'])->name('colors.edit');
  Route::post('/colors/update/{color}', [ColorController::class, 'update'])->name('colors.update');
  Route::get('/colors/delete/{color}', [ColorController::class, 'delete'])->name('colors.delete');
  Route::get('/colors/change-status/{color}', [ColorController::class, 'changeStatus'])->name('colors.changeStatus');
  Route::get('/colors/export', [ColorController::class, 'export'])->name('colors.export');

  // Sizes
  Route::get('/sizes', [SizeController::class, 'index'])->name('sizes.index');
  Route::get('/sizes/data', [SizeController::class, 'getData'])->name('sizes.data');
  Route::post('/sizes/create', [SizeController::class, 'create'])->name('sizes.create');
  Route::get('/sizes/edit/{size}', [SizeController::class, 'edit'])->name('sizes.edit');
  Route::post('/sizes/update/{size}', [SizeController::class, 'update'])->name('sizes.update');
  Route::get('/sizes/delete/{size}', [SizeController::class, 'delete'])->name('sizes.delete');
  Route::get('/sizes/change-status/{size}', [SizeController::class, 'changeStatus'])->name('sizes.changeStatus');
  Route::get('/sizes/export', [SizeController::class, 'export'])->name('sizes.export');

  // Brands
  Route::get('/brands', [BrandController::class, 'index'])->name('brands.index');
  Route::get('/brands/data', [BrandController::class, 'getData'])->name('brands.data');
  Route::post('/brands/create', [BrandController::class, 'create'])->name('brands.create');
  Route::get('/brands/edit/{brand}', [BrandController::class, 'edit'])->name('brands.edit');
  Route::post('/brands/update/{brand}', [BrandController::class, 'update'])->name('brands.update');
  Route::get('/brands/delete/{brand}', [BrandController::class, 'delete'])->name('brands.delete');
  Route::get('/brands/change-status/{brand}', [BrandController::class, 'changeStatus'])->name('brands.changeStatus');
  Route::get('/brands/export', [BrandController::class, 'export'])->name('brands.export');

  // States
  Route::get('/states', [StateController::class, 'index'])->name('states.index');
  Route::get('/states/data', [StateController::class, 'getData'])->name('states.data');
  Route::post('/states/create', [StateController::class, 'create'])->name('states.create');
  Route::get('/states/edit/{state}', [StateController::class, 'edit'])->name('states.edit');
  Route::post('/states/update/{state}', [StateController::class, 'update'])->name('states.update');
  Route::get('/states/delete/{state}', [StateController::class, 'delete'])->name('states.delete');
  Route::get('/states/change-status/{state}', [StateController::class, 'changeStatus'])->name('states.changeStatus');
  Route::get('/states/export', [StateController::class, 'export'])->name('states.export');

  // Cities
  Route::get('/cities', [CityController::class, 'index'])->name('cities.index');
  Route::get('/cities/data', [CityController::class, 'getData'])->name('cities.data');
  Route::post('/cities/create', [CityController::class, 'create'])->name('cities.create');
  Route::get('/cities/edit/{city}', [CityController::class, 'edit'])->name('cities.edit');
  Route::post('/cities/update/{city}', [CityController::class, 'update'])->name('cities.update');
  Route::get('/cities/delete/{city}', [CityController::class, 'delete'])->name('cities.delete');
  Route::get('/cities/change-status/{city}', [CityController::class, 'changeStatus'])->name('cities.changeStatus');
  Route::get('/cities/export', [CityController::class, 'export'])->name('cities.export');

  // Categories
  Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
  Route::get('/categories/data', [CategoryController::class, 'getData'])->name('categories.data');
  Route::post('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
  Route::get('/categories/edit/{category}', [CategoryController::class, 'edit'])->name('categories.edit');
  Route::post('/categories/update/{category}', [CategoryController::class, 'update'])->name('categories.update');
  Route::get('/categories/delete/{category}', [CategoryController::class, 'delete'])->name('categories.delete');
  Route::get('/categories/change-status/{category}', [CategoryController::class, 'changeStatus'])->name('categories.changeStatus');
  Route::get('/categories/export', [CategoryController::class, 'export'])->name('categories.export');

  // Sub Categories
  Route::get('/sub-categories', [SubCategoryController::class, 'index'])->name('sub_categories.index');
  Route::get('/sub-categories/data', [SubCategoryController::class, 'getData'])->name('sub_categories.data');
  Route::post('/sub-categories/create', [SubCategoryController::class, 'create'])->name('sub_categories.create');
  Route::get('/sub-categories/edit/{sub_category}', [SubCategoryController::class, 'edit'])->name('sub_categories.edit');
  Route::post('/sub-categories/update/{sub_category}', [SubCategoryController::class, 'update'])->name('sub_categories.update');
  Route::get('/sub-categories/delete/{sub_category}', [SubCategoryController::class, 'delete'])->name('sub_categories.delete');
  Route::get('/sub-categories/change-status/{sub_category}', [SubCategoryController::class, 'changeStatus'])->name('sub_categories.changeStatus');
  Route::get('/sub-categories/export', [SubCategoryController::class, 'export'])->name('sub_categories.export');

  // Users
  Route::get('/users', [UserController::class, 'index'])->name('users.index');
  Route::get('/users/data', [UserController::class, 'getData'])->name('users.data');
  Route::post('/users/create', [UserController::class, 'create'])->name('users.create');
  Route::get('/users/edit/{user}', [UserController::class, 'edit'])->name('users.edit');
  Route::post('/users/update/{user}', [UserController::class, 'update'])->name('users.update');
  Route::get('/users/delete/{user}', [UserController::class, 'delete'])->name('users.delete');
  Route::get('/users/change-status/{user}', [UserController::class, 'changeStatus'])->name('users.changeStatus');
  Route::get('/users/get-user-address/{user}', [UserController::class, 'getUserAddress'])->name('users.getUserAddress');
  Route::get('/users/get-cities/{state}', [UserController::class, 'getCities'])->name('users.getCities');
  Route::post('/users/update-address/{user}', [UserController::class, 'udpateAddress'])->name('users.udpateAddress');
  Route::get('/users/export', [UserController::class, 'export'])->name('users.export');

  // Vendors
  Route::get('/vendors', [VendorController::class, 'index'])->name('vendors.index');
  Route::get('/vendors/data', [VendorController::class, 'getData'])->name('vendors.data');
  Route::post('/vendors/create', [VendorController::class, 'create'])->name('vendors.create');
  Route::get('/vendors/edit/{vendor}', [VendorController::class, 'edit'])->name('vendors.edit');
  Route::post('/vendors/update/{vendor}', [VendorController::class, 'update'])->name('vendors.update');
  Route::get('/vendors/delete/{vendor}', [VendorController::class, 'delete'])->name('vendors.delete');
  Route::get('/vendors/change-status/{vendor}', [VendorController::class, 'changeStatus'])->name('vendors.changeStatus');
  Route::post('/vendors/update-address/{vendor}', [VendorController::class, 'udpateAddress'])->name('vendors.udpateAddress');
  Route::get('/vendors/export', [VendorController::class, 'export'])->name('vendors.export');

  // Payment
  Route::get('/payments', [PaymentController::class, 'index'])->name('payments.index');
  Route::get('/payments/data', [PaymentController::class, 'getData'])->name('payments.data');
  Route::get('/payments/export', [PaymentController::class, 'export'])->name('payments.export');

  // Orders
  Route::get('/orders/pending', [OrderController::class, 'index'])->name('orders.pending');
  Route::get('/orders/completed', [OrderController::class, 'completed'])->name('orders.completed');
  Route::get('/orders/pending-data', [OrderController::class, 'getPendingData'])->name('orders.pedingData');
  Route::get('/orders/completed-data', [OrderController::class, 'getCompletedData'])->name('orders.completedData');
  Route::get('/orders/change-status/{order}', [OrderController::class, 'changeStatus'])->name('orders.changeStatus');
  Route::get('/orders/show/{order}/{type}', [OrderController::class, 'show'])->name('orders.show');
  Route::get('/orders/pending/export', [OrderController::class, 'pendingExport'])->name('orders.pending_export');
  Route::get('/orders/completed/export', [OrderController::class, 'completedExport'])->name('orders.completed_export');

  // Products
  Route::get('/products', [ProductController::class, 'index'])->name('products.index');
  Route::get('/products/data', [ProductController::class, 'getData'])->name('products.data');
  Route::post('/products/create', [ProductController::class, 'create'])->name('products.create');
  Route::get('/products/edit/{product}', [ProductController::class, 'edit'])->name('products.edit');
  Route::post('/products/update/{product}', [ProductController::class, 'update'])->name('products.update');
  Route::get('/products/delete/{product}', [ProductController::class, 'delete'])->name('products.delete');
  Route::get('/products/change-status/{product}', [ProductController::class, 'changeStatus'])->name('products.changeStatus');
  Route::get('/products/get-sub-categories', [ProductController::class, 'getSubCategories'])->name('products.getSubCategories');
  Route::post('/products/upload', [ProductController::class, 'upload'])->name('products.upload');
  Route::post('/products/remove-upload', [ProductController::class, 'removeUplaod'])->name('products.removeUplaod');
  Route::post('/products/change-image-color', [ProductController::class, 'changeImageColor'])->name('products.changeImageColor');
  Route::get('/products/{product}/rate-review', [ProductController::class, 'getRateReview'])->name('products.getRateReview');
  Route::get('/products/rate-review/delete/{review}', [ProductController::class, 'deleteRateReview'])->name('products.deleteRateReview');
  Route::get('/products/export', [ProductController::class, 'export'])->name('products.export');

  // Reports
  Route::get('/reports/vendors', [ReportController::class, 'vendor'])->name('reports.vendor');
  Route::post('/reports/vendors/data', [ReportController::class, 'getVendorData'])->name('reports.getVendorData');
  Route::get('/reports/vendors/export', [ReportController::class, 'vendorExport'])->name('reports.vendor_export');
  Route::get('/reports/users', [ReportController::class, 'user'])->name('reports.user');
  Route::post('/reports/users/data', [ReportController::class, 'getUserData'])->name('reports.getUserData');
  Route::get('/reports/users/export', [ReportController::class, 'userExport'])->name('reports.user_export');
  Route::get('/reports/products', [ReportController::class, 'product'])->name('reports.product');
  Route::post('/reports/products/data', [ReportController::class, 'getProductData'])->name('reports.getProductData');
  Route::get('/reports/products/export', [ReportController::class, 'productExport'])->name('reports.product_export');

  // Contacts
  Route::get('/contacts', [ContactController::class, 'index'])->name('contacts.index');
  Route::get('/contacts/data', [ContactController::class, 'getData'])->name('contacts.data');
  Route::get('/contacts/delete/{contact}', [ContactController::class, 'delete'])->name('contacts.delete');
  Route::get('/contacts/export', [ContactController::class, 'export'])->name('contacts.export');

});