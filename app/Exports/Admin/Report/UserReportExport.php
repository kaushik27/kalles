<?php

namespace App\Exports\Admin\Report;

use App\Models\AddToCart;
use App\Models\City;
use App\Models\Order;
use App\Models\State;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UserReportExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::select('users.*', 'ua.address1', 'ua.address2', 'ua.city', 'ua.state')
                    ->leftJoin('user_addresses as ua', 'ua.user_id', 'users.id')
                    ->where('role', User::USER_ROLE)
                    ->get();

        $all_users = [];
        foreach ($users as $key => $user) {
            $completd_order_ids = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                        ->where('orders.status', Order::INACTIVE_ORDER)
                                        ->where('orders.user_id', $user->id)
                                        ->whereNotNull('orders.payment_id')
                                        ->withTrashed()
                                        ->pluck('orders.id')
                                        ->toArray();
            $user->total_sale = AddToCart::whereIn('order_id', $completd_order_ids)->sum('total_amount');
            
            $single_user = [];
            $single_user['id'] = $user->id;
            $single_user['fname'] = $user->fname;
            $single_user['lname'] = $user->lname;
            $single_user['email'] = $user->email;
            $single_user['phone'] = $user->phone;
            $single_user['city'] = $user->city ? City::find($user->city)->city : "";
            $single_user['state'] = $user->state ? State::find($user->state)->state : "";
            $single_user['total_sale'] = $user->total_sale ?? 0;
            $all_users[] = $single_user;
        }

        return collect($all_users);
    }

    public function headings(): array
    {
        return ["ID", "First Name", "Last Name", "Email", "Phone", "City", "State", "Total Sale"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
