@extends('layouts.app')
@section('title', 'Amazing Ecom - States')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>States</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">States</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of states.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('states.export')}}" class="btn btn-info mr-2" title="Export States"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create State" onclick="createState()"><i class="fas fa-plus"></i> Create State</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="state-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>State Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create State Modal -->
<div class="modal fade" id="create-state-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-state-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create State</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="create-state-state">State Name</label>
              <input type="text" class="form-control" name="state" id="create-state-state" placeholder="State Name">
              <span class="error" id="create-state-state-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-state-status">Status</label>
              <select name="status" id="create-state-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-state-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-state-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update State Modal -->
<div class="modal fade" id="update-state-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-state-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update State</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="update-state-state">State Name</label>
              <input type="hidden" class="form-control" name="id" id="update-state-id" placeholder="State ID">
              <input type="text" class="form-control" name="state" id="update-state-state" placeholder="State Name">
              <span class="error" id="update-state-state-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-state-status">Status</label>
              <select name="status" id="update-state-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-state-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-state-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#state-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("states.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'state', name: 'state'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create state
  function createState() {
    $('#create-state-modal').modal('show');
  }

  let createStateFormValidator = $('#create-state-form').validate({
    rules: {
      state: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      state: {
        required: "State name is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#create-state-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        state: document.querySelector('#create-state-state').value,
        status: document.querySelector('#create-state-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeState(param)
    }
  })

  async function storeState(param) {
    const submitButton = document.querySelector('#create-state-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateStateForm()

      let response = await fetch("{{route('states.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-state-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.state) document.querySelector('#create-state-state-error').textContent = response.message.state[0]
          if(response?.message?.status) document.querySelector('#create-state-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-state-modal').on('hidden.bs.modal', event => {
    resetCreateStateForm();
  })

  function resetCreateStateForm() {
    const form = document.querySelector('#create-state-form')
    form.reset()
    createStateFormValidator.resetForm()
    document.querySelector('#create-state-state-error').textContent = ""
    document.querySelector('#create-state-status-error').textContent = ""
  }
  

  // update state
  async function editState(state) {
    $('#update-state-modal').modal('show');

    try {
      let response = await fetch(`{{url('/states/edit')}}/${state}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-state-id').value = response.data.id;
        document.querySelector('#update-state-state').value = response.data.state;
        const status = document.querySelector('#update-state-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateStateFormValidator = $('#update-state-form').validate({
    rules: {
      state: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      state: {
        required: "State name is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#update-state-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const stateID = document.querySelector('#update-state-id').value
      const payload = {
        state: document.querySelector('#update-state-state').value,
        status: document.querySelector('#update-state-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateState(param, stateID)
    }
  })

  async function updateState(param, state) {
    const submitButton = document.querySelector('#update-state-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateStateForm()

      let response = await fetch(`{{url('/states/update')}}/${state}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-state-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.state) document.querySelector('#update-state-state-error').textContent = response.message.state[0]
          if(response?.message?.status) document.querySelector('#update-state-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-state-modal').on('hidden.bs.modal', event => {
    resetUpdateStateForm();
  })

  function resetUpdateStateForm() {
    updateStateFormValidator.resetForm()
    document.querySelector('#update-state-state-error').textContent = ""
    document.querySelector('#update-state-status-error').textContent = ""
  }


  // delete state
  function deleteState(state) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/states/delete')}}/${state}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of state
  function changeStateStatus(state) {
    swal({
      title: "Are you sure?",
      text: "You want to change state status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/states/change-status')}}/${state}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush