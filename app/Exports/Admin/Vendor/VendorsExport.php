<?php

namespace App\Exports\Admin\Vendor;

use App\Models\Vendor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VendorsExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $vendors = Vendor::all();
                    
        $all_vendors = [];
        foreach ($vendors as $key => $vendor) {
            $single_vendor = [];
            $single_vendor['id'] = $vendor->id;
            $single_vendor['firstname'] = ucfirst($vendor->fname);
            $single_vendor['lastname'] = ucfirst($vendor->lname);
            $single_vendor['email'] = $vendor->email;
            $single_vendor['phone'] = $vendor->phone;
            $single_vendor['Address'] = $vendor->address;
            $single_vendor['status'] = $vendor->status;
            $all_vendors[] = $single_vendor;
        }

        return collect($all_vendors);
    }

    public function headings(): array
    {
        return ["ID", "Frist Name", "Last Name", "Email", "Phone", "Address", "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
