<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Report\ProductReportExport;
use App\Exports\Admin\Report\UserReportExport;
use App\Exports\Admin\Report\VendorReportExport;
use App\Http\Controllers\Controller;
use App\Models\AddToCart;
use App\Models\Brand;
use App\Models\Category;
use App\Models\City;
use App\Models\Color;
use App\Models\Order;
use App\Models\Product;
use App\Models\Size;
use App\Models\State;
use App\Models\SubCategory;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    public function vendor() {
        $vendors = Vendor::all();
        return view('reports.vendor', compact('vendors'));
    }

    public function getVendorData(Request $request) {

        $date = $request->date ?? "";
        $selected_vendor = $request->vendor ?? "";

        $start = "";
        $end = "";
        if($date) {
            $date = explode(' - ', $date);
            $start = date('Y-m-d', strtotime($date[0]));
            $end = date('Y-m-d', strtotime($date[1]));
        }

        $vendors = Vendor::when($selected_vendor, function ($query) use($selected_vendor) {
                        $query->where('id', $selected_vendor);
                    })
                    ->get();

        foreach ($vendors as $key => $vendor) {
            $vendor_product_ids = Product::where('vendor_id', $vendor->id)->pluck('id')->toArray();
            $completd_order_ids = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                    ->when($start && $end, function ($query) use($start, $end) {
                                        $query->whereBetween('p.created_at', [$start, $end]);
                                    })
                                    ->where('orders.status', Order::INACTIVE_ORDER)
                                    ->whereNotNull('orders.payment_id')
                                    ->withTrashed()
                                    ->pluck('orders.id')
                                    ->toArray();
            $vendor->total_sale = AddToCart::whereIn('order_id', $completd_order_ids)
                                    ->whereIn('product_id', $vendor_product_ids)
                                    ->sum('total_amount');
        }

        $vendors = collect($vendors->toArray())->sortBy([
            ['total_sale', 'desc'],
        ])->all();
        
        return DataTables::of($vendors)
            ->addIndexColumn()
            ->editColumn('fname', function($vendor){
                return ucfirst($vendor['fname']);
            })
            ->editColumn('lname', function($vendor){
                return ucfirst($vendor['lname']);
            })
            ->editColumn('phone', function($vendor){
                return $vendor['phone'] ?? "-";
            })
            ->addColumn('sale', function($vendor){
                return $vendor['total_sale'] ?? 0;
            })
            ->rawColumns(['sale'])
            ->make(true);
    }

    public function user() {
        $users = User::where('role', User::USER_ROLE)->get();
        return view('reports.user', compact('users'));
    }

    public function getUserData(Request $request) {

        $date = $request->date ?? "";
        $selected_user = $request->user ?? "";

        $start = "";
        $end = "";
        if($date) {
            $date = explode(' - ', $date);
            $start = date('Y-m-d', strtotime($date[0]));
            $end = date('Y-m-d', strtotime($date[1]));
        }

        $users = User::select('users.*', 'ua.address1', 'ua.address2', 'ua.city', 'ua.state')
                    ->leftJoin('user_addresses as ua', 'ua.user_id', 'users.id')
                    ->when($selected_user, function ($query) use($selected_user) {
                        $query->where('id', $selected_user);
                    })
                    ->where('role', User::USER_ROLE)
                    ->get();

        foreach ($users as $key => $user) {
            $completd_order_ids = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                        ->when($start && $end, function ($query) use($start, $end) {
                                            $query->whereBetween('p.created_at', [$start, $end]);
                                        })
                                        ->where('orders.status', Order::INACTIVE_ORDER)
                                        ->where('orders.user_id', $user->id)
                                        ->whereNotNull('orders.payment_id')
                                        ->withTrashed()
                                        ->pluck('orders.id')
                                        ->toArray();
            $user->total_sale = AddToCart::whereIn('order_id', $completd_order_ids)->sum('total_amount');
        }

        $users = collect($users->toArray())->sortBy([
            ['total_sale', 'desc'],
        ])->all();
        
        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('fname', function($user){
                return ucfirst($user['fname']);
            })
            ->editColumn('lname', function($user){
                return ucfirst($user['lname']);
            })
            ->editColumn('phone', function($user){
                return $user['phone'] ?? "-";
            })
            ->addColumn('city', function($user){
                return $user['city'] ? City::find($user['city'])->city : "";
            })
            ->addColumn('state', function($user){
                return $user['state'] ? State::find($user['state'])->state : "";
            })
            ->addColumn('sale', function($user){
                return $user['total_sale'] ?? 0;
            })
            ->rawColumns([])
            ->make(true);
    }

    public function product() {
        $products = Product::all();
        return view('reports.product', compact('products'));
    }

    public function getProductData(Request $request) {

        $date = $request->date ?? "";
        $selected_product = $request->product ?? "";

        $start = "";
        $end = "";
        if($date) {
            $date = explode(' - ', $date);
            $start = date('Y-m-d', strtotime($date[0]));
            $end = date('Y-m-d', strtotime($date[1]));
        }

        $products = Product::when($selected_product, function ($query) use($selected_product) {
                        $query->where('id', $selected_product);
                    })->get();

        foreach ($products as $key => $product) {
            $completd_order_ids = Order::leftJoin('payments as p', 'p.id', 'orders.payment_id')
                                        ->when($start && $end, function ($query) use($start, $end) {
                                            $query->whereBetween('p.created_at', [$start, $end]);
                                        })
                                        ->where('orders.status', Order::INACTIVE_ORDER)
                                        ->where('orders.user_id', $product->id)
                                        ->whereNotNull('orders.payment_id')
                                        ->withTrashed()
                                        ->pluck('orders.id')
                                        ->toArray();
            $product->total_sale = AddToCart::whereIn('order_id', $completd_order_ids)
                                        ->where('product_id', $product->id)
                                        ->sum('total_amount');
        }

        $products = collect($products->toArray())->sortBy([
            ['total_sale', 'desc'],
        ])->all();

        return DataTables::of($products)
            ->addIndexColumn()
            ->addColumn('vendor', function($product){
                $vendor = Vendor::find($product['vendor_id']);
                return $vendor ? ucwords($vendor->fname . ' ' . $vendor->lname) : "";
            })
            ->editColumn('categories', function($product){
                $all_categories = [];
                if($product['categories']) {
                    $product_categories = Category::whereIn('id', explode(',', $product['categories']))->get();
                    foreach ($product_categories as $key => $category) {
                        $all_categories[] = $category->name;
                    }
                }
                return implode(', ', $all_categories) ?? "-";
            })
            ->editColumn('sub_categories', function($product){
                $all_sub_categories = [];
                if($product['sub_categories']) {
                    $product_sub_categories = SubCategory::whereIn('id', explode(',', $product['sub_categories']))->get();
                    foreach ($product_sub_categories as $key => $sub_category) {
                        $all_sub_categories[] = $sub_category->name;
                    }
                }
                return implode(', ', $all_sub_categories) ?? "-";
            })
            ->editColumn('colors', function($product){
                $all_colors = [];
                if($product['colors']) {
                    $product_colors = Color::whereIn('id', explode(',', $product['colors']))->get();
                    foreach ($product_colors as $key => $color) {
                        $all_colors[] = $color->name;
                    }
                }
                return implode(', ', $all_colors) ?? "-";
            })
            ->editColumn('brand', function($product){
                $product_brand = Brand::find($product['brand']);
                return $product_brand ? $product_brand->name : "-";
            })
            ->editColumn('sizes', function($product){
                $all_sizes = [];
                if($product['sizes']) {
                    $product_sizes = Size::whereIn('id', explode(',', $product['sizes']))->get();
                    foreach ($product_sizes as $key => $size) {
                        $all_sizes[] = $size->size;
                    }
                }
                return implode(', ', $all_sizes) ?? "-";
            })
            ->editColumn('tags', function($product){
                $all_tags = [];
                if($product['tags']) 
                    $all_tags = explode(',', $product['tags']);

                return implode(', ', $all_tags) ?? "-";
            })
            ->addColumn('sale', function($product){
                return $product['total_sale'] ?? 0;
            })
            ->rawColumns(['categories', 'sub_categories', 'colors', 'brand', 'sizes', 'tags'])
            ->make(true);
    }

    public function vendorExport() {
        return Excel::download(new VendorReportExport, 'vendor_report.xlsx');
    }

    public function userExport() {
        return Excel::download(new UserReportExport, 'user_report.xlsx');
    }

    public function productExport() {
        return Excel::download(new ProductReportExport, 'product_report.xlsx');
    }

}
