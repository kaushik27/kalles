<?php

namespace App\Http\Requests\Admin\City;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "city"      =>  "required|min:2|max:20",
            "state"     =>  "required",
            "shipping_price"    =>  "required|integer|min:0|max:100000",
            "estimation_time"   =>  "required|integer|min:0",
            "status"    =>  "required"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "city.required"             =>  "City name is required.",
            "state.required"            =>  "State name is required.",
            "shipping_price.required"   =>  "Shipping price is required.",
            "estimation_time.required"  =>  "Estimation time is required.",
            "status.required"           =>  "Status is required."
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
