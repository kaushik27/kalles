<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Brand\BrandsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Brand\CreateBrandRequest;
use App\Http\Requests\Admin\Brand\UpdateBrandRequest;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class BrandController extends Controller
{
    public function index()
    {
        return view('brands.index');
    }

    public function getData() {
        $brands = Brand::all();
        
        return DataTables::of($brands)
            ->addIndexColumn()
            ->editColumn('status', function($brand){
                return $brand->status 
                    ? '<span class="badge badge-success" onclick="changeBrandStatus('.$brand->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeBrandStatus('.$brand->id.')">Inactive</span>';
            })
            ->addColumn('action', function($brand){
                return '
                    <button class="btn btn-info mr-2" title="Edit Brand" onclick="editBrand('.$brand->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-danger" title="Delete Brand" onclick="deleteBrand('.$brand->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create(CreateBrandRequest $request) {
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->status = $request->status;
        if($brand->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Brand created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Brand $brand) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $brand
        ]);
    }

    public function update(UpdateBrandRequest $request, Brand $brand) {
        $brand->name = $request->name;
        $brand->status = $request->status;
        if($brand->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Brand updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Brand $brand) {
        
        $products = Product::all();
        foreach ($products as $key => $product) {
            $brands = explode(',', $product->brands);
            if(in_array($brand->id, $brands)) {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  "Products already found with this brand, First You need to remove this brand's products ."
                ]);
            }
        }
        
        if($brand->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Brand deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Brand $brand) {
        $brand->status = $brand->status == Brand::ACTIVE_BRAND ? Brand::INACTIVE_BRAND : Brand::ACTIVE_BRAND;
        if($brand->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Brand status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new BrandsExport, 'brands.xlsx');
    }
}
