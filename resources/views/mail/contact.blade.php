<!DOCTYPE html>
<html>
<head>
    <title>Contact Mail</title>
</head>
<body>  

    <p>Hello {{$details['name']}},</p>

    <p>Thank you for reaching out.</p>

    <p>We will contact you soon.</p>

    <p>
      Thanks,<br>
      Amazing Ecom Team.
    </p>
    
</body>
</html>