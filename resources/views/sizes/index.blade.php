@extends('layouts.app')
@section('title', 'Amazing Ecom - Sizes')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Sizes</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Sizes</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of sizes.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('sizes.export')}}" class="btn btn-info mr-2" title="Export Sizes"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Size" onclick="createSize()"><i class="fas fa-plus"></i> Create Size</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="size-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Size</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Size Modal -->
<div class="modal fade" id="create-size-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-size-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Size</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="create-size-size">Size</label>
              <input type="text" class="form-control" name="size" id="create-size-size" placeholder="Size">
              <span class="error" id="create-size-size-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-size-status">Status</label>
              <select name="status" id="create-size-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-size-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-size-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Size Modal -->
<div class="modal fade" id="update-size-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-size-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Size</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="update-size-size">Size</label>
              <input type="hidden" class="form-control" name="id" id="update-size-id" placeholder="Size ID">
              <input type="text" class="form-control" name="size" id="update-size-size" placeholder="Size">
              <span class="error" id="update-size-size-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-size-status">Status</label>
              <select name="status" id="update-size-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-size-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-size-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#size-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("sizes.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'size', name: 'size'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create size
  function createSize() {
    $('#create-size-modal').modal('show');
  }

  let createSizeFormValidator = $('#create-size-form').validate({
    rules: {
      size: {
        required: true,
        lettersonly: true,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      size: {
        required: "Size is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#create-size-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        size: document.querySelector('#create-size-size').value,
        status: document.querySelector('#create-size-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeSize(param)
    }
  })

  async function storeSize(param) {
    const submitButton = document.querySelector('#create-size-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateSizeForm()

      let response = await fetch("{{route('sizes.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-size-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.size) document.querySelector('#create-size-size-error').textContent = response.message.size[0]
          if(response?.message?.status) document.querySelector('#create-size-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-size-modal').on('hidden.bs.modal', event => {
    resetCreateSizeForm();
  })

  function resetCreateSizeForm() {
    const form = document.querySelector('#create-size-form')
    form.reset()
    createSizeFormValidator.resetForm()
    document.querySelector('#create-size-size-error').textContent = ""
    document.querySelector('#create-size-status-error').textContent = ""
  }
  

  // update size
  async function editSize(size) {
    $('#update-size-modal').modal('show');

    try {
      let response = await fetch(`{{url('/sizes/edit')}}/${size}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-size-id').value = response.data.id;
        document.querySelector('#update-size-size').value = response.data.size;
        const status = document.querySelector('#update-size-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateSizeFormValidator = $('#update-size-form').validate({
    rules: {
      size: {
        required: true,
        lettersonly: true,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      size: {
        required: "Size is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#update-size-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const sizeID = document.querySelector('#update-size-id').value
      const payload = {
        size: document.querySelector('#update-size-size').value,
        status: document.querySelector('#update-size-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateSize(param, sizeID)
    }
  })

  async function updateSize(param, size) {
    const submitButton = document.querySelector('#update-size-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateSizeForm()

      let response = await fetch(`{{url('/sizes/update')}}/${size}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-size-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.size) document.querySelector('#update-size-size-error').textContent = response.message.size[0]
          if(response?.message?.status) document.querySelector('#update-size-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-size-modal').on('hidden.bs.modal', event => {
    resetUpdateSizeForm();
  })

  function resetUpdateSizeForm() {
    updateSizeFormValidator.resetForm()
    document.querySelector('#update-size-size-error').textContent = ""
    document.querySelector('#update-size-status-error').textContent = ""
  }


  // delete size
  function deleteSize(size) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/sizes/delete')}}/${size}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of size
  function changeSizeStatus(size) {
    swal({
      title: "Are you sure?",
      text: "You want to change size status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/sizes/change-status')}}/${size}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush