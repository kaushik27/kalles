<?php

namespace App\Http\Requests\API\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class GetProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort_by'               =>  'numeric',
            'filter_categories'     =>  'array',
            'filter_brands'         =>  'array',
            'filter_colors'         =>  'array',
            'filter_sizes'          =>  'array',
            'min_price'             =>  'numeric',
            'max_price'             =>  'numeric',
            'stock'                 =>  'numeric',
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
