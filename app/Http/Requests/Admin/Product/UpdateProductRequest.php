<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"              =>  "required|min:2|max:20",
            "price"             =>  "required",
            "quantity"          =>  "required",
            "categories"        =>  "required",
            "sub_categories"    =>  "required",
            "colors"            =>  "required",
            "brand"             =>  "required",
            "sizes"             =>  "required",
            "tags"              =>  "required",
            "summary"           =>  "nullable|max:250"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.required"             =>  "Product name is required.",
            "price.required"            =>  "Price is required.",
            "quantity.required"         =>  "Quantity is required.",
            "categories.required"       =>  "Category is required.",
            "sub_categories.required"   =>  "Sub category is required.",
            "colors.required"           =>  "Color is required.",
            "brand.required"            =>  "Brand is required.",
            "sizes.required"            =>  "Size is required.",
            "tags.required"             =>  "Tags is required.",
            "summary.required"          =>  "Summary is required."
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
