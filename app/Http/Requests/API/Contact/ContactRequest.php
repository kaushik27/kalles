<?php

namespace App\Http\Requests\API\Contact;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  "required|min:3",
            'email'     =>  "required|regex:/^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z0-9_]+/",
            'phone'     =>  "required|digits:10",
            'message'   =>  "required|min:3"
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
