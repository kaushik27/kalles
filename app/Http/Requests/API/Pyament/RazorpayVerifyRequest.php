<?php

namespace App\Http\Requests\API\Pyament;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RazorpayVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razorpay_order_id'     =>  'required',
            'razorpay_payment_id'   =>  'required',
            'razorpay_signature'    =>  'required'
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
