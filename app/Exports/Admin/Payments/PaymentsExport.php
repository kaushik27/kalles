<?php

namespace App\Exports\Admin\Payments;

use App\Models\Order;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PaymentsExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $payments = Order::select('orders.*', 'p.type', 'p.razorpay_payment_id', 'p.created_at as payment_date')
                        ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                        ->whereNotNull('orders.payment_id')
                        ->withTrashed()
                        ->get();
                    
        $all_payments = [];
        foreach ($payments as $key => $payment) {
            $single_payment = [];
            $single_payment['id'] = $payment->id;
            $single_payment['type'] = $payment->type;
            $single_payment['order_id'] = $payment->razorpay_order_id;
            $single_payment['payment_id'] = $payment->razorpay_payment_id;

            $user = User::find($payment->user_id);
            $single_payment['customer_name'] = $user ? $user->fname. " " .$user->lname : "";

            $single_payment['amount'] = $payment->final_amount;
            $single_payment['date'] = date('d-m-Y H:i A', strtotime($payment->payment_date));
            $all_payments[] = $single_payment;
        }

        return collect($all_payments);
    }

    public function headings(): array
    {
        return ["ID", "Payment Type", "Order ID", "Payment ID", "Customer Name", "Amount", "Date"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
