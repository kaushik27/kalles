<?php

namespace App\Http\Requests\Admin\Vendor;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateVendorRequest extends FormRequest
{
    /**
     * Determine if the vendor is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fname"     =>  "required|min:2|max:20",
            "lname"     =>  "required|min:2|max:20",
            "email"     =>  "required|email|unique:vendors,email",
            "phone"     =>  "required|digits:10"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "fname.required"        =>  "First name is required.",
            "lname.required"        =>  "Last name is required.",
            "email.required"        =>  "Email is required.",
            "Phone.required"        =>  "Phone number is required."
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
