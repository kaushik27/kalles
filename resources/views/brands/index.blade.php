@extends('layouts.app')
@section('title', 'Amazing Ecom - Brands')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Brands</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Brands</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-lg-6">
                  <h3 class="card-title">Here is the list of brands.</h3>
                </div>
                <div class="col-lg-6 text-right">
                  <a href="{{route('brands.export')}}" class="btn btn-info mr-2" title="Export Brands"><i class="fas fa-download"></i> Export</a>
                  <a href="javascript:void(0)" class="btn btn-success" title="Create Brand" onclick="createBrand()"><i class="fas fa-plus"></i> Create Brand</a>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="brand-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Brand Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->

<!-- Create Brand Modal -->
<div class="modal fade" id="create-brand-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="create-brand-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Create Brand</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="create-brand-name">Brand Name</label>
              <input type="text" class="form-control" name="name" id="create-brand-name" placeholder="Brand Name">
              <span class="error" id="create-brand-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="create-brand-status">Status</label>
              <select name="status" id="create-brand-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="create-brand-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="create-brand-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Update Brand Modal -->
<div class="modal fade" id="update-brand-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="update-brand-form" action="javascript:void(0)">
        <div class="modal-header">
          <h4 class="modal-title">Update Brand</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <label for="update-brand-name">Brand Name</label>
              <input type="hidden" class="form-control" name="id" id="update-brand-id" placeholder="Brand ID">
              <input type="text" class="form-control" name="name" id="update-brand-name" placeholder="Brand Name">
              <span class="error" id="update-brand-name-error"></span>
            </div>
            <div class="col-md-6">
              <label for="update-brand-status">Status</label>
              <select name="status" id="update-brand-status" class="form-control select2">
                <option value="1" selected>Active</option>
                <option value="0">Inactive</option>
              </select>
              <span class="error" id="update-brand-status-error"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="update-brand-submit-btn" type="submit" class="btn btn-primary" style="min-width: 75px;">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@push('footer-script')
<script>
  let table = $('#brand-list').DataTable({
    autoWidth: true,
    responsive: true,
    processing: true,
    serverSide: true,
    ajax: '{{route("brands.data")}}',
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'name', name: 'name'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true}
    ]
  })


  // create brand
  function createBrand() {
    $('#create-brand-modal').modal('show');
  }

  let createBrandFormValidator = $('#create-brand-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Brand name is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#create-brand-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const payload = {
        name: document.querySelector('#create-brand-name').value,
        status: document.querySelector('#create-brand-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      storeBrand(param)
    }
  })

  async function storeBrand(param) {
    const submitButton = document.querySelector('#create-brand-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetCreateBrandForm()

      let response = await fetch("{{route('brands.create')}}", param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#create-brand-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#create-brand-name-error').textContent = response.message.name[0]
          if(response?.message?.status) document.querySelector('#create-brand-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Submit'
    }
  }

  $('#create-brand-modal').on('hidden.bs.modal', event => {
    resetCreateBrandForm();
  })

  function resetCreateBrandForm() {
    const form = document.querySelector('#create-brand-form')
    form.reset()
    createBrandFormValidator.resetForm()
    document.querySelector('#create-brand-name-error').textContent = ""
    document.querySelector('#create-brand-status-error').textContent = ""
  }
  

  // update brand
  async function editBrand(brand) {
    $('#update-brand-modal').modal('show');

    try {
      let response = await fetch(`{{url('/brands/edit')}}/${brand}`);

      if(!response.ok) {
        return swal("Something went wrong, Please try again!", {
          icon: "error",
        })
      }

      response = await response.json();
      if(response.status) {
        document.querySelector('#update-brand-id').value = response.data.id;
        document.querySelector('#update-brand-name').value = response.data.name;
        const status = document.querySelector('#update-brand-status')
        status.value = response.data.status
        status.dispatchEvent(new Event("change"));
      }

    } catch (err) {
      console.error(err)
    }
  }

  let updateBrandFormValidator = $('#update-brand-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 20
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Brand name is required.",
      },
      status: {
        required: "Status is required.",
      }
    },
    errorPlacement: function(error, element) {
      if(element.attr('name') == "status") {
        error.appendTo('#update-brand-status-error')
      } else {
        error.insertAfter($(element))
      }
    },
    submitHandler: function(form) {
      const brandID = document.querySelector('#update-brand-id').value
      const payload = {
        name: document.querySelector('#update-brand-name').value,
        status: document.querySelector('#update-brand-status').value,
        _token: "{{csrf_token()}}"
      }
      const param = prepareFetchParam(payload)
      updateBrand(param, brandID)
    }
  })

  async function updateBrand(param, brand) {
    const submitButton = document.querySelector('#update-brand-submit-btn')
    submitButton.innerHTML = '<i class="fas fa-spinner loader"></i>'
    try {
      resetUpdateBrandForm()

      let response = await fetch(`{{url('/brands/update')}}/${brand}`, param)
      
      if(response.ok) {
        response = await response.json()
        if(response.status) {
          swal(response.message, {
            icon: "success",
          })
          $('#update-brand-modal').modal('hide');
          table.ajax.reload()
        } else {
          swal(response.message, {
            icon: "error",
          })
        }
        
      } else {
        if(response.status !== 422) {
          return swal("Something went wrong, Please try again!", {
            icon: "error",
          })
        } 
        response = await response.json()
        if(response.status == 0) {
          if(response?.message?.name) document.querySelector('#update-brand-name-error').textContent = response.message.name[0]
          if(response?.message?.status) document.querySelector('#update-brand-status-error').textContent = response.message.status[0]
        }
      }

    } catch (err) {
      console.error(err)
    } finally {
      submitButton.innerHTML = 'Update'
    }
  }

  $('#update-brand-modal').on('hidden.bs.modal', event => {
    resetUpdateBrandForm();
  })

  function resetUpdateBrandForm() {
    updateBrandFormValidator.resetForm()
    document.querySelector('#update-brand-name-error').textContent = ""
    document.querySelector('#update-brand-status-error').textContent = ""
  }


  // delete brand
  function deleteBrand(brand) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/brands/delete')}}/${brand}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }

  // update status of brand
  function changeBrandStatus(brand) {
    swal({
      title: "Are you sure?",
      text: "You want to change brand status !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then(async (confirm) => {
      if (confirm) {
        try {
          let response = await fetch(`{{url('/brands/change-status')}}/${brand}`);

          if(!response.ok) {
            return swal("Something went wrong, Please try again!", {
              icon: "error",
            })
          }

          response = await response.json();
          if(response.status) {
            swal(response.message, {
              icon: "success",
            })
            table.ajax.reload()
          } else {
            swal(response.message, {
              icon: "error",
            })
          }

        } catch (err) {
          console.error(err)
        }
      }
    })
  }
</script>
@endpush