<?php

namespace App\Http\Requests\Admin\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'  =>  'required|min:6|max:20',
            'cpassword' =>  'required|same:password'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required'     =>  'Password is required.',
            'cpassword.required'    =>  'Confirm password is required.',
            'cpassword.same'        =>  'Passwod and confirm password must be match.'
        ];
    }

    /**
     * return validation errors
     *
     * @return array
     */
    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $response = response()->json([
            'status' => 0,
            'message' => $errors->messages(),
        ], 422);
        throw new HttpResponseException($response);
    }
}
