<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Vendor\VendorsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vendor\CreateVendorRequest;
use App\Http\Requests\Admin\Vendor\UpdateVendorRequest;
use App\Models\City;
use App\Models\State;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class VendorController extends Controller
{
    public function index()
    {
        $states = State::where('status', State::ACTIVE_STATE)->get();
        return view('vendors.index', compact('states'));
    }

    public function getData() {
        $vendors = Vendor::all();
        
        return DataTables::of($vendors)
            ->addIndexColumn()
            ->editColumn('fname', function($vendor){
                return ucfirst($vendor->fname);
            })
            ->editColumn('lname', function($vendor){
                return ucfirst($vendor->lname);
            })
            ->editColumn('phone', function($vendor){
                return $vendor->phone ?? "-";
            })
            ->addColumn('address', function($vendor){
                return $vendor->address ?? "-";
            })
            ->editColumn('status', function($vendor){
                return $vendor->status 
                    ? '<span class="badge badge-success" onclick="changeVendorStatus('.$vendor->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeVendorStatus('.$vendor->id.')">Inactive</span>';
            })
            ->addColumn('action', function($vendor){
                return '
                    <button class="btn btn-info mr-2" title="Edit Vendor" onclick="editVendor('.$vendor->id.')"><i class="fas fa-edit"></i></button>
                    ';
                    // <button class="btn btn-danger" title="Delete Vendor" onclick="deleteVendor('.$vendor->id.')"><i class="fas fa-trash"></i></button>
            })
            ->rawColumns(['address', 'status', 'action'])
            ->make(true);
    }

    public function create(CreateVendorRequest $request) {
        $vendor = new Vendor();
        $vendor->fname = $request->fname;
        $vendor->lname = $request->lname;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        $vendor->address = $request->address;
        if($vendor->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Vendor created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Vendor $vendor) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $vendor
        ]);
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor) {
        $vendor->fname = $request->fname;
        $vendor->lname = $request->lname;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        $vendor->address = $request->address;
        if($vendor->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Vendor updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Vendor $vendor) {
        if($vendor->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Vendor deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Vendor $vendor) {
        $vendor->status = $vendor->status == Vendor::ACTIVE_VENDOR ? Vendor::INACTIVE_VENDOR : Vendor::ACTIVE_VENDOR;
        if($vendor->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Vendor status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new VendorsExport, 'vendors.xlsx');
    }
}
