<!DOCTYPE html>
<html>
<head>
    <title>Email Verification Mail</title>
</head>
<body>  

    <p>Hello {{$details['name']}},</p>

    <p>Welcome to Amazing Ecom</p>

    <p>Thanks for registering an account with Amazing Ecom! Before we get started, we will need to verify your email.</p>

    <p>You can verify your email using following link:</p>

    <a href="{{$details['link']}}">{{$details['link']}}</a>

    <p>
        Thanks,<br>
        Amazing Ecom Team.
    </p>
    
</body>
</html>