<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CartController;
use App\Http\Controllers\API\HomeController;
use App\Http\Controllers\API\PaymentController;
use App\Http\Controllers\API\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Auth
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);

// get state
Route::get('/states', [AuthController::class, 'getStates']);

// get city by state_id
Route::get('/state/{state_id}/cities', [AuthController::class, 'getCities']);

// get colors
Route::get('/colors', [ProductController::class, 'getColors']);

// get sizes
Route::get('/sizes', [ProductController::class, 'getSizes']);

// get brand
Route::get('/brands', [ProductController::class, 'getBrands']);

// get category
Route::get('/categories', [ProductController::class, 'getCategories']);

// get sub category by category_id
Route::get('/category/{category_id}/sub-categories', [ProductController::class, 'getSubCategories']);

// get stock
Route::get('/stocks', [ProductController::class, 'getStock']);

// get services
Route::get('/services', [HomeController::class, 'getServices']);

// products
Route::get('/products', [ProductController::class, 'getProducts']);
Route::get('/product/{product_id}', [ProductController::class, 'getSingleProduct']);
Route::get('/product/{product_id}/review', [ProductController::class, 'reviews']);

// Contact
Route::post('/contact', [HomeController::class, 'contact']);

Route::middleware(['auth:sanctum'])->group(function () {
  // logout
  Route::get('/logout', [AuthController::class, 'logout']);
  
  // profile
  Route::get('/profile', [AuthController::class, 'profile']);
  Route::put('/profile/update', [AuthController::class, 'updateProfile']);
  Route::put('/profile/address/update', [AuthController::class, 'updateUserAddress']);

  // product wishlist
  Route::get('/wishlist', [ProductController::class, 'wishlist']);
  Route::get('/product/{product_id}/wishlist', [ProductController::class, 'addNRemoveWishlist']);
  
  // product review
  Route::post('/product/{product_id}/review/add', [ProductController::class, 'addReview']);  
  
  // add to part
  Route::post('/add-to-cart', [CartController::class, 'addToCart']);  
  Route::get('/get-cart', [CartController::class, 'getCart']);
  Route::get('/add-to-cart/{cart_id}/add-quantity', [CartController::class, 'addQuantity']);
  Route::get('/add-to-cart/{cart_id}/remove-quantity', [CartController::class, 'removeQuantity']);
  Route::get('/remove-from-cart/{cart_id}', [CartController::class, 'removeFromCart']);
  Route::post('/note', [CartController::class, 'saveNote']);

  // add & remove COD charge on order
  Route::get('order/{order}/checkout/{type}', [CartController::class, 'checkout']);
  Route::get('/order/{order}/cod', [CartController::class, 'addRemoveCOD']);

  // order history
  Route::get('/get-order-history', [CartController::class, 'getOrderHistory']);

  // razorpay payment
  Route::post('payment/razorpay', [PaymentController::class, 'razorpay']);
  Route::post('payment/razorpay/verify', [PaymentController::class, 'verify']);
});
