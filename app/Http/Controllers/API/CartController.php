<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Cart\AddToCartRequest;
use App\Http\Requests\API\Cart\SaveNoteRequest;
use App\Mail\GenerateOrderMail;
use App\Models\AddToCart;
use App\Models\Color;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Stmt\TryCatch;

class CartController extends BaseController
{
    public function addToCart(AddToCartRequest $request) {
        try {
            $product = Product::find($request->product_id);

            // if product not exist
            if(!$product) return $this->sendError("Please enter valid product id.", [], 400);

            // if product quantity is less than request quantity
            if($request->quantity > $product->quantity) return $this->sendError("You can not enter quantity more than stock.", [], 400);

            // if color not exist in product
            if($request->color_id) {
                $colors = explode(',', $product->colors);
                $unmatch_colors = array_diff([$request->color_id], $colors);
                if(!empty($unmatch_colors)) return $this->sendError("Product not found with this color, Please enter valid color id.", [], 400);
            }

            // if size not exist in product
            if($request->size_id) {
                $sizes = explode(',', $product->sizes);
                $unmatch_sizes = array_diff([$request->size_id], $sizes);
                if(!empty($unmatch_sizes)) return $this->sendError("Product not found with this size, Please enter valid size id.", [], 400);
            }

            // total amount of single product [ quantity * price ]
            $total_amount = round($request->quantity * $product->price, 2);

            $order = Order::where('user_id', $request->user('sanctum')->id)
                        ->where('status', Order::ACTIVE_ORDER)
                        ->whereNULL('payment_id')
                        ->first();

            if($order) {

                $color_id   =   $request->color_id ?? "";
                $size_id    =   $request->size_id ?? "";

                $exist_product_in_cart = AddToCart::where('order_id', $order->id)
                            ->where('product_id', $request->product_id)
                            ->where('price', $product->price)
                            ->when($color_id, function($query) use($color_id) {
                                $query->where('color_id', $color_id);
                            })
                            ->when($color_id == "", function($query) {
                                $query->whereNull('color_id');
                            })
                            ->when($size_id, function($query) use($size_id) {
                                $query->where('size_id', $size_id);
                            })
                            ->when($size_id == "", function($query) {
                                $query->whereNull('size_id');
                            })
                            ->first();

                if($exist_product_in_cart) {

                    // if user has already added this product into cart
                    // And someone has perchase those products (that means now product quantity not enough)
                    // then return with error message (Even in this scenario doesn't add product into the cart)
                    if(($request->quantity + $exist_product_in_cart->quantity) > $product->quantity) {
                        return $this->sendError("You can not enter quantity more than stock.", [], 400);
                    }

                    // only udpate quantity into the cart
                    $exist_product_in_cart->quantity        +=  $request->quantity;
                    $exist_product_in_cart->total_amount    +=  $total_amount;
                    $exist_product_in_cart->save();

                } else {
                    // first add product into the existing order
                    $cart = new AddToCart();
                    $cart->user_id          =   $request->user('sanctum')->id;
                    $cart->order_id         =   $order->id;
                    $cart->product_id       =   $product->id;
                    $cart->color_id         =   $request->color_id;
                    $cart->size_id          =   $request->size_id;
                    $cart->price            =   $product->price;
                    $cart->quantity         =   $request->quantity;
                    $cart->total_amount     =   $total_amount;
                    $cart->save();
                }

                // update order
                $order->sub_total_amount    +=  $total_amount;
                $order->shipping_price      =   User::shippingPrice();
                $order->final_amount        =   $order->sub_total_amount + $order->shipping_price;
                $order->save();

            } else {
                // first create order (blank cart)
                $order = new Order();
                $order->user_id             =   $request->user('sanctum')->id;
                $order->sub_total_amount    =   $total_amount;
                $order->shipping_price      =   User::shippingPrice();
                $order->final_amount        =   $order->sub_total_amount + $order->shipping_price;
                $order->save();
    
                // after creating order add product into the order
                $cart = new AddToCart();
                $cart->user_id              =   $request->user('sanctum')->id;
                $cart->order_id             =   $order->id;
                $cart->product_id           =   $product->id;
                $cart->color_id             =   $request->color_id;
                $cart->size_id              =   $request->size_id;
                $cart->price                =   $product->price;
                $cart->quantity             =   $request->quantity;
                $cart->total_amount         =   $total_amount;
                $cart->save();
            }

            return $this->sendResponse("Product added into the cart successfully!");

        } catch (\Throwable $th) {
            return $this->sendError("Something went wrong, Please try again!", [], 400);
        }
    }

    public function getCart(Request $request) {
        $order = Order::where('user_id', $request->user('sanctum')->id)
                        ->where('status', Order::ACTIVE_ORDER)
                        ->whereNULL('payment_id')
                        ->first();

        if($order) {
            $cartResponse = $this->prepareCartResponse($order);
            return (!empty($cartResponse))
                    ? $this->sendResponse("", $cartResponse) 
                    : $this->sendResponse("Cart is empty.", $cartResponse);

        } else {
            return $this->sendResponse("Cart is empty.", []);
        }
    }

    public function prepareCartResponse($order) {
        $columns = [
            'add_to_cart.*',
            'p.name as product_name', 'p.status as product_status', 'p.quantity as product_quantity',
            'c.name as color', 'c.color as color_code', 'c.status as color_status',
            's.size as size', 's.status as size_status',
        ];
        $cart = AddToCart::select(...$columns)
                        ->leftJoin('products as p', 'p.id', 'add_to_cart.product_id')
                        ->leftJoin('colors as c', 'c.id', 'add_to_cart.color_id')
                        ->leftJoin('sizes as s', 's.id', 'add_to_cart.size_id')
                        ->where('order_id', $order->id)
                        ->get();

        if(empty($cart)) return [];

        $cart_items = [];
        foreach ($cart as $key => $item) {

            // product image
            $images = ProductImage::where('product_id', $item->product_id)->get();
            $all_images = [];
            if(!empty($images)) {
                foreach ($images as $key => $image) {
                    $single_image = [];
                    $single_image['image'] = is_file(public_path('images/products/'.$image->image)) && file_exists(public_path('images/products/'.$image->image))
                                        ? asset('images/products/'.$image->image)
                                        : asset('images/products/default.png');
                    $single_image['color'] = Color::find($image->color_id);
                    if($single_image['color'] != null)
                        $all_images[] = $single_image;
                }
            }

            $cart_items[] = [
                'id'            =>  $item->id,
                'order_id'      =>  $item->order_id,
                'price'         =>  $item->price,
                'quantity'      =>  $item->quantity,
                'total_amount'  =>  $item->total_amount,
                'created_at'    =>  date('d-m-Y', strtotime($item->created_at)),
                'updated_at'    =>  date('d-m-Y', strtotime($item->updated_at)),
                'product_details'   =>  [
                    'id'        =>  $item->product_id,
                    'name'      =>  $item->product_name,
                    'quantity'  =>  $item->product_quantity,
                    'images'    =>  $all_images,
                    'status'    =>  $item->product_status
                ],
                'color_details'     =>  [
                    'id'        =>  $item->color_id,
                    'name'      =>  $item->color,
                    'color'     =>  $item->color_code,
                    'status'    =>  $item->color_status
                ],
                'size_details'      =>  [
                    'size'      =>  $item->size,
                    'status'    =>  $item->size_status
                ]
            ];
        }

        $paymentDetails = [];
        if($order->payment_id) {
            $payment = Payment::find($order->payment_id);
            if($payment) {
                $paymentDetails['id'] = $payment->id;
                $paymentDetails['type'] = $payment->type;
                $paymentDetails['status'] = $payment->status ? "Paid" : "Created";
            }
        }

        $data = [
            'cart_items'        =>  $cart_items,
            'order_id'          =>  $order->id,
            'sub_total_amount'  =>  $order->sub_total_amount,
            'shipping_price'    =>  $order->shipping_price ?? 0,
            'cod'               =>  $order->cod ?? 0,
            'final_amount'      =>  $order->final_amount,
            'note'              =>  $order->note,
            'status'            =>  $order->status,
            'payment_details'   =>  $paymentDetails
        ];
        return $data;
    }

    public function addQuantity($cart_id) {
        $cart = AddToCart::find($cart_id);

        if(!$cart) return $this->sendError("Cart not found, Please enter valid cart id in url.", [], 400);
        
        $product = Product::find($cart->product_id);

        // if product is not available in stock
        if($product->quantity < 1) return $this->sendError("Product is the out of stock, So you can not increase quantity now.", [], 400);

        // increase quantity & total amount in cart item
        $cart->quantity += 1;
        $cart->total_amount += $cart->price;
        $cart->save();

        // increase sub total amount in order
        $order = Order::find($cart->order_id);
        $order->sub_total_amount    += $cart->price;
        $order->shipping_price      = User::shippingPrice();
        $order->final_amount        = $order->sub_total_amount + $order->shipping_price;
        $order->save();

        // decrease quanity in products
        $product->quantity -= 1;
        $product->save();

        return $this->sendResponse("Quantity added successfully!");
    }

    public function removeQuantity($cart_id) {
        $cart = AddToCart::find($cart_id);

        if(!$cart) return $this->sendError("Cart not found, Please enter valid cart id in url.", [], 400);

        if($cart->quantity == 1) {
            // decrease sub total amount in order
            $order = Order::find($cart->order_id);
            $order->sub_total_amount -= $cart->price;
            $order->shipping_price = User::shippingPrice();
            $order->final_amount = $order->sub_total_amount + $order->shipping_price;
            $order->save();

            // delete cart item
            $cart->delete();

        } else {
            // decrease quantity & total amount in cart item
            $cart->quantity -= 1;
            $cart->total_amount -= $cart->price;
            $cart->save();

            // decrease sub total amount in order
            $order = Order::find($cart->order_id);
            $order->sub_total_amount -= $cart->price;
            $order->shipping_price = User::shippingPrice();
            $order->final_amount = $order->sub_total_amount + $order->shipping_price;
            $order->save();
        }

        // increase quantity in products
        $product = Product::find($cart->product_id);
        $product->quantity += 1;
        $product->save();

        return $this->sendResponse("Quantity removed successfully!");
    }

    public function removeFromCart($cart_id) {
        $cart = AddToCart::find($cart_id);

        if(!$cart) return $this->sendError("Cart not found, Please enter valid cart id in url.", [], 400);

        // decrease sub total amount in order
        $order = Order::find($cart->order_id);
        $order->sub_total_amount -= $cart->total_amount;
        $order->shipping_price = User::shippingPrice();
        $order->final_amount = $order->sub_total_amount + $order->shipping_price;
        $order->save();

        // increase quantity in products
        $product = Product::find($cart->product_id);
        $product->quantity += $cart->quantity;
        $product->save();

        $cart->delete();

        return $this->sendResponse("Item removed from cart successfully!");
    }

    public function saveNote(SaveNoteRequest $request) {
        $order = Order::find($request->order_id);
        if(!$order) return $this->sendError("Order not found, Please enter valid order_id.");

        $order->note = $request->note;
        $order->save();

        return $this->sendResponse("Note save successfully!");
    }

    public function getOrderHistory(Request $request) {
        $orders = Order::where('user_id', $request->user('sanctum')->id)
                    ->whereNotNull('payment_id')
                    ->withTrashed()
                    ->orderByDesc('id')
                    ->get();

        $all_orders = [];
        if(!empty($orders)) {
            foreach ($orders as $key => $order) {
                $all_orders[] = $this->prepareCartResponse($order);
            }
        }

        return $this->sendResponse("", $all_orders);
    }

    public function addRemoveCOD($order) {
        $order = Order::find($order);

        if($order) {
            if($order->payment_id) {
                return $this->sendError("You can not add COD charge after payment done.");
            }
    
            if($order->status == Order::INACTIVE_ORDER) {
                return $this->sendError("You can not add COD charge for completed order.");
            }
            
            if($order->cod) {
                $order->final_amount -= $order->cod;
                $order->cod = 0;
                $order->save();

                $cartResponse = $this->prepareCartResponse($order);
                return $this->sendResponse("COD charge removed successfully!", $cartResponse);
                
            } else {
                $cod_charge = round(($order->final_amount * 20) / 100);
                $order->cod = $cod_charge;
                $order->final_amount += $cod_charge;
                $order->save();

                $cartResponse = $this->prepareCartResponse($order);
                return $this->sendResponse("COD charge added successfully!", $cartResponse);
            }

        } else {
            return $this->sendError("Order not exist, please enter valid order id in url.");
        }
    }

    public function checkout(Request $request, $order, $type) {

        if(!in_array($type, ["online", "cod"])) {
            return $this->sendError("Please enter valid checkout type in url");
        }

        $order = Order::find($order);
        
        if($order) {

            // check user has already checkout or not this order
            if($order->payment_id || $order->status == Order::INACTIVE_ORDER) {
                return $this->sendError("Your order has already completed, You can not checkout again.");
            }

            // check product quantity enough or not
            // if not then return error resopnse
            $is_product_available = $this->checkProductAvailablity($order);
            if($is_product_available['status']) {
                if($is_product_available['available'] == 0) {
                    return $this->sendError($is_product_available['message'], $is_product_available['data']);
                }
            } else {
                return $this->sendError($is_product_available['message']);
            }

            // check enable service or not on user address
            // if not then return error response
            $user_address = UserAddress::select('user_addresses.*', 'c.city as city_name', 'c.status as city_status', 's.state as state_name', 's.status as state_status')
                                ->leftJoin('states as s', 's.id', 'user_addresses.state')
                                ->leftJoin('cities as c', 'c.id', 'user_addresses.city')
                                ->where('user_id', $request->user('sanctum')->id)
                                ->first();

            if(!$user_address) return $this->sendError("User address not exist, please enter first user address.");

            $is_service_enable  =  $user_address->city_status && $user_address->state_status ? 1 : 0;
            if(!$is_service_enable) return $this->sendError("Curretly we are not giving service in your city.");

            // make payment
            $payment = new Payment();
            $payment->type = $type;
            if($payment->save()) {

                // add payment id in order
                $order->payment_id = $payment->id;
                if($order->save()) {

                    // decrease quantity from product
                    $this->decreaseProductsQuantity($order);

                    // send generate order mail to user
                    try {
                        $details = [
                            'name'          =>  $request->user('sanctum')->fname . ' ' . $request->user('sunctum')->lname,
                            'order_details' =>  $this->prepareCartResponse($order)
                        ];
                        Mail::to($request->user('sanctum')->email)->send(new GenerateOrderMail($details));

                    } catch (\Throwable $th) {
                        $this->sendError("Checkout successfully, but mail has not been sent successfully!");
                    }
    
                    return $this->sendResponse("Checkout successfully.");

                } else {
                    return $this->sendError("Something went wrong, Please try again!");
                }

            } else {
                return $this->sendError("Something went wrong, Please try again!");
            }
    
        } else {
            return $this->sendError("Order not exist, Please enter valid order id in url");
        }
    }

    public function checkProductAvailablity($order) {
        $cartResponse = $this->prepareCartResponse($order);

        if(empty($cartResponse)) {
            return [
                'status'    =>  0,
                'message'   =>  "Cart item not found, You can not checkout without add any product to cart."
            ];
        }

        $cart_items = $cartResponse['cart_items'];

        $not_enough_quantity_cart_item_ids = [];
        if(!empty($cart_items)) {
            foreach ($cart_items as $key => $item) {
                if($item->quantity > $item->product_details->quantity) {
                    $not_enough_quantity_cart_item_ids[] = [
                        "id"            =>  $item->id,
                        "max_quantity"  =>  $item->product_details->quantity
                    ];
                }
            }
        }

        if(!empty($not_enough_quantity_cart_item_ids)) {
            return [
                'status'    =>  1,
                'available' =>  0,
                'message'   =>  "Product quantity is not enough",
                'data'      =>  [
                    "not_enough_items"  =>  $not_enough_quantity_cart_item_ids
                ]
            ];
        } else {
            return [
                'status'    =>  1,
                'available' =>  1
            ];
        }
    }

    public function decreaseProductsQuantity($order) {
        $cartResponse = $this->prepareCartResponse($order);

        $cart_items = $cartResponse['cart_items'];

        if(!empty($cart_items)) {
            foreach ($cart_items as $key => $item) {
                $product = Product::find($item->product_details->id);
                if($product) {
                    $product->quantity -= $item->quantity;
                    $product->save();
                }
            }
        }
    }
}
