<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Contact\ContactRequest;
use App\Mail\ContactMail;
use App\Models\Contact;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends BaseController
{
    public function getServices() {
        $services = Service::where('status', Service::ACTIVE_SERVICE)->get()->toArray();

        $services = collect($services)->map(function($item) {
            $item['icon'] = is_file(public_path('images/service/'.$item['icon'])) && file_exists(public_path('images/service/'.$item['icon']))
                                ? asset('images/service/'.$item['icon'])
                                : asset('images/service/default.png');
            return $item;
        })->all();

        if(empty($services)) {
            return response()->json([
                'status'    =>  1,
                'data'      =>  []    
            ]);
        }
        return $this->sendResponse("", $services);
    }

    public function contact(ContactRequest $request) {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        if($contact->save()) {

            // send mail
            try {
                $details = [
                    'name' =>  $request->name
                ];
                Mail::to($request->email)->send(new ContactMail($details));
            } catch (\Throwable $th) {
                return $this->sendError("Failed, Mail has not been sent successfully!", [], 424);
            }

            return $this->sendResponse("You have submitted successfully, admin will be contact you soon.");
        } else {
            return $this->sendError("Something went wrong, Please try again!");
        }
    }
}
