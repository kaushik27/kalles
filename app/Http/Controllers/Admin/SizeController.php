<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Size\SizesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Size\CreateSizeRequest;
use App\Http\Requests\Admin\Size\UpdateSizeRequest;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class SizeController extends Controller
{
    public function index()
    {
        return view('sizes.index');
    }

    public function getData() {
        $sizes = Size::all();
        
        return DataTables::of($sizes)
            ->addIndexColumn()
            ->editColumn('status', function($size){
                return $size->status 
                    ? '<span class="badge badge-success" onclick="changeSizeStatus('.$size->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeSizeStatus('.$size->id.')">Inactive</span>';
            })
            ->addColumn('action', function($size){
                return '
                    <button class="btn btn-info mr-2" title="Edit Size" onclick="editSize('.$size->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-danger" title="Delete Size" onclick="deleteSize('.$size->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create(CreateSizeRequest $request) {
        $size = new Size();
        $size->size = $request->size;
        $size->status = $request->status;
        if($size->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Size created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Size $size) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $size
        ]);
    }

    public function update(UpdateSizeRequest $request, Size $size) {
        $size->size = $request->size;
        $size->status = $request->status;
        if($size->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Size updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Size $size) {

        $products = Product::all();
        foreach ($products as $key => $product) {
            $sizes = explode(',', $product->sizes);
            if(in_array($size->id, $sizes)) {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  "Products already found with this size, First You need to remove this size's products ."
                ]);
            }
        }

        if($size->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Size deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Size $size) {
        $size->status = $size->status == Size::ACTIVE_SIZE ? Size::INACTIVE_SIZE : Size::ACTIVE_SIZE;
        if($size->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Size status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new SizesExport, 'sizes.xlsx');
    }
}
