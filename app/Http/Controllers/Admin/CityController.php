<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\City\CitiesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\City\CreateCityRequest;
use App\Http\Requests\Admin\City\UpdateCityRequest;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class CityController extends Controller
{
    public function index()
    {
        $states = State::where('status', State::ACTIVE_STATE)->get();
        return view('cities.index', compact('states'));
    }

    public function getData() {
        $cities = City::select('cities.*', 'states.state as state_name')->leftJoin('states', 'states.id', 'cities.state_id')->get();
        
        return DataTables::of($cities)
            ->addIndexColumn()
            ->editColumn('estimation_time', function($city){
                return $city->estimation_time ? durationFromSeconds($city->estimation_time) : "";
            })
            ->editColumn('status', function($city){
                return $city->status 
                    ? '<span class="badge badge-success" onclick="changeCityStatus('.$city->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeCityStatus('.$city->id.')">Inactive</span>';
            })
            ->addColumn('action', function($city){
                return '
                    <button class="btn btn-info mr-2" title="Edit city" onclick="editCity('.$city->id.')"><i class="fas fa-edit"></i></button>
                ';
                // <button class="btn btn-danger" title="Delete city" onclick="deleteCity('.$city->id.')"><i class="fas fa-trash"></i></button>
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create(CreateCityRequest $request) {
        $city = new City();
        $city->city = $request->city;
        $city->state_id = $request->state;
        $city->shipping_price = $request->shipping_price;
        $city->estimation_time = $request->estimation_time;
        $city->status = $request->status;
        if($city->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "City created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(City $city) {
        $city->estimation_time = durationFromSeconds($city->estimation_time, 'edit');
        return response()->json([
            'status'    =>  1,
            'data'      =>  $city
        ]);
    }

    public function update(UpdateCityRequest $request, City $city) {
        $city->city = $request->city;
        $city->state_id = $request->state;
        $city->shipping_price = $request->shipping_price;
        $city->estimation_time = $request->estimation_time;
        $city->status = $request->status;
        if($city->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "City updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(City $city) {
        if($city->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'City deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(City $city) {
        $city->status = $city->status == City::ACTIVE_CITY ? City::INACTIVE_CITY : City::ACTIVE_CITY;
        if($city->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'City status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function getStates() {
        $states = State::where('status', State::ACTIVE_STATE)->get();
        return response()->json([
            'status'    =>  1,
            'data'      =>  $states
        ]);
    }

    public function export() {
        return Excel::download(new CitiesExport, 'cities.xlsx');
    }
}
