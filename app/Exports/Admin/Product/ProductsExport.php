<?php

namespace App\Exports\Admin\Product;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Vendor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductsExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $products = Product::all();

        $all_products = [];
        foreach ($products as $key => $product) {
            
            $single_product = [];
            $single_product['id'] = $product->id;
            $single_product['name'] = $product->name;
            $single_product['price'] = $product->name;
            $single_product['quantity'] = $product->name;

            // vendor
            $vendor = Vendor::find($product->vendor_id);
            $single_product['vendor'] = $vendor ? ucwords($vendor->fname . ' ' . $vendor->lname) : "";

            // categories
            $all_categories = [];
            if($product->categories) {
                $product_categories = Category::whereIn('id', explode(',', $product->categories))->get();
                foreach ($product_categories as $key => $category) {
                    $all_categories[] = $category->name;
                }
            }
            $single_product['categories'] = implode(', ', $all_categories) ?? "-";

            // sub categories
            $all_sub_categories = [];
            if($product->sub_categories) {
                $product_sub_categories = SubCategory::whereIn('id', explode(',', $product->sub_categories))->get();
                foreach ($product_sub_categories as $key => $sub_category) {
                    $all_sub_categories[] = $sub_category->name;
                }
            }
            $single_product['sub_categories'] = implode(', ', $all_sub_categories) ?? "-";

            // colors
            $all_colors = [];
            if($product->colors) {
                $product_colors = Color::whereIn('id', explode(',', $product->colors))->get();
                foreach ($product_colors as $key => $color) {
                    $all_colors[] = $color->name;
                }
            }
            $single_product['colors'] = implode(', ', $all_colors) ?? "-";

            // brand
            $product_brand = Brand::find($product->brand);
            $single_product['brand'] = $product_brand ? $product_brand->name : "-";

            // size
            $all_sizes = [];
            if($product->sizes) {
                $product_sizes = Size::whereIn('id', explode(',', $product->sizes))->get();
                foreach ($product_sizes as $key => $size) {
                    $all_sizes[] = $size->size;
                }
            }
            $single_product['sizes'] = implode(', ', $all_sizes) ?? "-";

            // tags
            $all_tags = [];
            if($product->tags) 
                $all_tags = explode(',', $product->tags);

            $single_product['tags'] = implode(', ', $all_tags) ?? "-";

            // status
            $single_product['status'] = $product->status;

            $all_prodcuts[] = $single_product;
        }

        return collect($all_prodcuts);
    }

    public function headings(): array
    {
        return ["ID", "Product Name", "Price", "Quantity", "Vendor Name", "Categories", "Sub Categories", "Colors", "Brand", "Sizes", "Tags", "Status"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
