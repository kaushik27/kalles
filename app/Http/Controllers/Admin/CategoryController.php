<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Admin\Category\CategoriesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CreateCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function index()
    {
        return view('categories.index');
    }

    public function getData() {
        $categories = Category::all();
        
        return DataTables::of($categories)
            ->addIndexColumn()
            ->editColumn('image', function($category){
                return is_file(public_path('images/category/'.$category->image)) && file_exists(public_path('images/category/'.$category->image))
                    ? '<img src="'.asset('images/category/'.$category->image).'" height=70"">'
                    : '<img src="'.asset('images/category/default.png').'" height="70">';
            })
            ->editColumn('status', function($category){
                return $category->status 
                    ? '<span class="badge badge-success" onclick="changeCategoryStatus('.$category->id.')">Active</span>'
                    : '<span class="badge badge-danger" onclick="changeCategoryStatus('.$category->id.')">Inactive</span>';
            })
            ->addColumn('action', function($category){
                return '
                    <button class="btn btn-info mr-2" title="Edit category" onclick="editCategory('.$category->id.')"><i class="fas fa-edit"></i></button>
                    <button class="btn btn-danger" title="Delete category" onclick="deleteCategory('.$category->id.')"><i class="fas fa-trash"></i></button>
                ';
            })
            ->rawColumns(['image', 'status', 'action'])
            ->make(true);
    }

    public function create(CreateCategoryRequest $request) {
        $category = new Category();
        $category->name = $request->name;

        // update category image
        if($request->image) {
            $image_name = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/category/'), $image_name);
            $category->image = $image_name;
        }

        $category->status = $request->status;
        if($category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Category created successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function edit(Category $category) {
        return response()->json([
            'status'    =>  1,
            'data'      =>  $category
        ]);
    }

    public function update(UpdateCategoryRequest $request, Category $category) {
        $category->name = $request->name;

        // update category image
        if($request->image) {
            // delete old category image
            @unlink(public_path('images/category/'.$category->image));

            $image_name = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/category/'), $image_name);
            $category->image = $image_name;
        }

        $category->status = $request->status;
        if($category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  "Category updated successfully!"
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function delete(Category $category) {
        $products = Product::all();
        foreach ($products as $key => $product) {
            $categories = explode(',', $product->categories);
            if(in_array($category->id, $categories)) {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  "Products already found with this category, First You need to remove this category's products ."
                ]);
            }
        }

        // delete category image
        @unlink(public_path('images/category/'.$category->image));
        
        if($category->delete()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Category deleted successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function changeStatus(Category $category) {
        $category->status = $category->status == Category::ACTIVE_CATEGORY ? Category::INACTIVE_CATEGORY : Category::ACTIVE_CATEGORY;
        if($category->save()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Category status has been changed successfully!'
            ]);
        } else {
            return response()->json([
                'status'    =>  0,
                'message'   =>  "Something went wrong, please try again!"
            ]);
        }
    }

    public function export() {
        return Excel::download(new CategoriesExport, 'categories.xlsx');
    }
}
