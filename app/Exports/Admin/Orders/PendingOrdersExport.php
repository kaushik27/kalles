<?php

namespace App\Exports\Admin\Orders;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PendingOrdersExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orders = Order::select('orders.*', 'u.fname', 'u.lname', 'p.type', 'p.created_at as payment_date')
                    ->leftJoin('users as u', 'u.id', 'orders.user_id')
                    ->leftJoin('payments as p', 'p.id', 'orders.payment_id')
                    ->where('orders.status', Order::ACTIVE_ORDER)
                    ->whereNotNull('orders.payment_id')
                    ->withTrashed()
                    ->get();
                    
        $all_orders = [];
        foreach ($orders as $key => $order) {
            $single_order = [];
            $single_order['id'] = $order->id;
            $single_order['customer_name'] = ucwords($order->fname. " " .$order->lname);
            $single_order['amount'] = $order->final_amount;
            $single_order['type'] = $order->type;
            $single_order['date'] = date('d-m-Y H:i A', strtotime($order->payment_date));
            $single_order['note'] = $order->note;
            $all_orders[] = $single_order;
        }

        return collect($all_orders);
    }

    public function headings(): array
    {
        return ["ID", "Customer Name", "Amount", "Payment Type", "Payment Date", "Note"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => ['bold' => true, 'size' => 12],
            ],
        ];
    }
}
